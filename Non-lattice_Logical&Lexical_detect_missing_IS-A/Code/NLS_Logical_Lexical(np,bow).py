#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 17 14:20:18 2020

@author: fengbozheng
"""
###############################################################################
#Step1: Pre-processing
###############################################################################
#activeConcept20200301.txt
#activeFSN20200301.txt
#activeInferredRelation(GroupNum)20200301.txt
#hierarchicalRelation(childParent)20200301.txt
#hierarchicalRelation(parentChild)20200301.txt
file1 = open("sct2_Concept_Full_US1000124_20200301.txt","r")
file2 = open("sct2_Description_Full-en_US1000124_20200301.txt","r")
activeConcept = {}
for lines1 in file1:
    line1 = lines1.split("\n")[0].split("\t")
    if line1[0] != "id":
        conceptID = line1[0]
        effectiveTime = line1[1]
        if effectiveTime <= "20200301":
            content = (effectiveTime,line1[2],line1[4])
            if activeConcept.get(conceptID,"default") == "default":
                activeConcept[conceptID] = content
            else:
                if effectiveTime > activeConcept.get(conceptID)[0]:
                    activeConcept[conceptID] = content  
output1 = open("activeConcept20200301.txt","w")
for key in activeConcept.keys():
    if activeConcept.get(key)[1] == "1":
        output1.write(key+"\t"+activeConcept.get(key)[2]+"\n")
output1.close()
file1.close()
activeFSN = {}
for lines2 in file2:
    line2 = lines2.split("\n")[0].split("\t")    
    if line2[0] != "id":
        if line2[1] <= "20200301":
            content = (line2[1],line2[2],line2[4],line2[6],line2[7])
            if activeFSN.get(line2[0],"default") == "default":
                activeFSN[line2[0]] = content
            else:
                if line2[1] > activeFSN.get(line2[0])[0]:
                    activeFSN[line2[0]] = content
output2 = open("activeFSN20200301.txt","w")
for key in activeFSN.keys():
    if activeFSN.get(key)[1] == "1":
        if (activeFSN.get(key)[3]=="900000000000003001"):
            output2.write(activeFSN.get(key)[2]+"\t"+activeFSN.get(key)[4]+"\n")
output2.close()
file2.close()
#SOME concepts have their corresponding FSNs (FSNs are active) but are inactive 
file3 = open("sct2_Relationship_Full_US1000124_20200301.txt","r")
activeRelation = {}
for lines3 in file3:
    line3 = lines3.split("\n")[0].split("\t")
    if line3[0] != "id":
        if line3[1] <= "20200301":
            content = (line3[1],line3[2],line3[4],line3[5],line3[6],line3[7]) 
            #effectiveTime, active, sourceID, destiniationID, Group, typeID
            if activeRelation.get(line3[0],"default") == "default":
                activeRelation[line3[0]] = content
            else:
                if line3[1] > activeRelation.get(line3[0])[0]:
                    activeRelation[line3[0]] = content
#
output3 = open("activeInferredRelation(GroupNum)20200301.txt","w")
for key in activeRelation.keys():
    if activeRelation.get(key)[1] == "1":
        output3.write(activeRelation.get(key)[2]+"\t"+activeRelation.get(key)[5]+"\t"+activeRelation.get(key)[3]+"\t"+activeRelation.get(key)[4]+"\n")
output3.close()
file3.close()
#
file4 = open("activeInferredRelation(GroupNum)20200301.txt","r")
output4 = open("hierarchicalRelation(childParent)20200301.txt","w")
output5 = open("hierarchicalRelation(parentChild)20200301.txt","w")
for lines4 in file4:
    line4 = lines4.split()
    if line4[1] == "116680003":
        output4.write(line4[0]+"\t"+line4[2]+"\n")
        output5.write(line4[2]+"\t"+line4[0]+"\n")
output4.close()
output5.close()
file4.close()
#
file4 = open("activeInferredRelation(GroupNum)20200301.txt","r")
output4 = open("hierarchicalRelation_WithIsModificationOf(childParent)20200301.txt","w")
for lines4 in file4:
    line4 = lines4.split()
    if line4[1] in ["738774007","116680003"]:
        output4.write(line4[0]+"\t"+line4[2]+"\n")
output4.close()
file4.close()
'''
#
import networkx as nx
file2 = open("IsModificationOf20200301.txt","rb")
Dirgraph1 = nx.read_edgelist(file2, create_using = nx.DiGraph(), nodetype = str)
file2.close()
#
print(nx.dag_longest_path(Dirgraph1))
'''
###############################################################################
#Rashmmie extracted stated definition for SNOMED CT
'''
file1 = open("SNOMED_20200301_isaRels.txt","r")
file2 = open("SNOMED_20200301_attributeRels.txt","r")
file3 = open("activeStatedRelation(GroupNum)20200301.txt","w")
for lines1 in file1:
    line1 = lines1.split()
    file3.write(line1[0]+"\t"+"116680003"+"\t"+line1[1]+"\t"+"0"+"\n")
file1.close()
for lines2 in file2:
    line2 = lines2.split()
    file3.write(line2[0]+"\t"+line2[1]+"\t"+line2[2]+"\t"+line2[3]+"\n")
file2.close()
file3.close()
'''
###############################################################################
#Step2: Find each concept's attributes (inferred ones)
###############################################################################
inferredAttributes = {}  
#key(sourceID): value [(groupNum, typeID, destinationID),....(groupNum, typeID, destinationID)]
#key(sourceID): value (a list of all its attributes (attribute + groupNum)
#
file3 = open("activeInferredRelation(GroupNum)20200301.txt","r")
for lines3 in file3:
    line3 = lines3.split()
    sourceID = line3[0]
    attributeWithGroup = line3[3]+" "+line3[1]+" "+line3[2]  # attributeWithGroup = GroupNum + typeID + destinationID
    if inferredAttributes.get(sourceID,"default") == "default":
        inferredAttributes[sourceID] = [attributeWithGroup]
    else:
        inferredAttributes[sourceID].append(attributeWithGroup)   
file3.close()
#
###############################################################################
#Step3: For a specific concept A, compute its grouped inferred attributes  
###############################################################################    
def computeGroupedInferredAttributes(conceptA):
    groupedAttributesDic = {}
    allungroupedAttributes = inferredAttributes.get(conceptA)
    for attributeWithGroupNum in allungroupedAttributes:
        attributesPlusGroupNum = attributeWithGroupNum.split()
        groupNum = attributesPlusGroupNum[0]
        attribute = (attributesPlusGroupNum[1],attributesPlusGroupNum[2])
        if groupedAttributesDic.get(groupNum,"default") =="default":
            groupedAttributesDic[groupNum] = [attribute]
        else:
            groupedAttributesDic[groupNum].append(attribute)
    #return groupedAttributesDic
    attributeList = []
    for keys in groupedAttributesDic.keys():
        if keys == "0":
            for eachAttribute in groupedAttributesDic.get(keys):
                attributeList.append([eachAttribute])                
        else:
            attributeList.append(sorted(groupedAttributesDic.get(keys)))
    return sorted(attributeList)
#
###############################################################################
#Step4: For a specific node(concept) in the hierarchy, find all its supertype ancestors
###############################################################################
import networkx as nx
file2 = open("hierarchicalRelation(childParent)20200301.txt","rb")
Dirgraph1 = nx.read_edgelist(file2, create_using = nx.DiGraph(), nodetype = str)
file2.close()
#
def findAncestors(node):
    Dic = dict(nx.bfs_successors(Dirgraph1,node))
    d = Dic.values()
    c = []
    for e in d:
        c.extend(e)
    return c  
#
file3 = open("hierarchicalRelation_WithIsModificationOf(childParent)20200301.txt","rb")
Dirgraph2 = nx.read_edgelist(file3, create_using = nx.DiGraph(), nodetype = str)
file3.close()
def findAncestors2(node):
    Dic = dict(nx.bfs_successors(Dirgraph2,node))
    d = Dic.values()
    c = []
    for e in d:
        c.extend(e)
    return c  
#Pre-compute ancestors for each concepts to improve execution time
ancestorFile1 = open("conceptAndAncestor.txt","w")
AllConceptInSNOMED = list(Dirgraph1.nodes)
for concept1 in AllConceptInSNOMED:
    ancestor1 = findAncestors(concept1)
    ancestorFile1.write(concept1+"\t"+"\t".join(ancestor1)+"\n")
ancestorFile1.close()
ancestorFile2 = open("conceptAndAncestor_propertyChain.txt","w")
for concept2 in AllConceptInSNOMED:
    ancestor2 = findAncestors2(concept2)
    ancestorFile2.write(concept2+"\t"+"\t".join(ancestor2)+"\n")
ancestorFile2.close()
###############################################################################
#Step5: Logical definition subsumption checking
###############################################################################
file1 = open("conceptAndAncestor.txt","r")
file2 = open("conceptAndAncestor_propertyChain.txt","r")
ancestorDic1 = {}
for lines1 in file1:
    line1 = lines1.split("\n")[0].split("\t")
    ancestorDic1[line1[0]] = line1[1:]
ancestorDic2 = {}#Property Chain
for lines2 in file2:
    line2 = lines2.split("\n")[0].split("\t")
    ancestorDic2[line2[0]] = line2[1:]
file1.close()
file2.close()
#
propertyChainList = set(["127489000","424361007","363701004","246093002","246075003","762949000"])
#Given two attribute (single) attributeA and attributeB, check if attributeB is more general than attributeA
def checkSingleGeneral(attributeB, attributeA):
    if (attributeB[0] == attributeA[0]) or (attributeB[0] in ancestorDic1.get(attributeA[0])):
        if attributeA[0] in propertyChainList and attributeB[0] in propertyChainList:
            if (attributeB[1] in ancestorDic2.get(attributeA[1])) or (attributeB[1] == attributeA[1]):
                return True
            else:
                return False
        elif (attributeB[1] in ancestorDic1.get(attributeA[1])) or (attributeB[1] == attributeA[1]):
            return True
        else:
            return False
    else:
        return False
#Given two attribute: attributeB (one single), attributeA (one group), check if attributeB is more general than attributeA
def checkSingleGroupGeneral(attributeB, attributeAList):
    if any(checkSingleGeneral(attributeB, singleAttribute) for singleAttribute in attributeAList):
        return True
    else:
        return False   
#
def checkGroupGroupGeneral(attributeBList, attributeAList):
    if all(checkSingleGroupGeneral(singleAttribute, attributeAList) for singleAttribute in attributeBList):
        return True
    else:
        return False        
#
def checkLogicalDefGeneral(conceptADef,conceptBDef):
    generalFlag = 1
    for x in conceptADef:
        if any(checkGroupGroupGeneral(x,y) for y in conceptBDef) == False:
            generalFlag = 0
    if generalFlag == 1:
        return True
    else:
        return False
###############################################################################
#Step6: Identify candidate concept pairs in NLS that satisfy logical-based subsumption checking 
###############################################################################
activeConcept = {}
file2 = open("activeConcept20200301.txt","r")
for lines2 in file2:
    line2 = lines2.split()
    if line2[1] == "900000000000073002":
        activeConcept[line2[0]] = "defined"
    else:
        activeConcept[line2[0]] = "primitive"
file2.close()
activeID = set(activeConcept.keys())
#
IDToFSN = {}
file1= open("activeFSN20200301.txt","r")
for lines in file1:
    line = lines.split("\n")[0]
    linee = line.split("\t")
    conceptID = linee[0]

    conceptFSN = linee[1]
    if conceptID in activeID:
        if IDToFSN.get(conceptID,"default") == "default":
            IDToFSN[conceptID] = conceptFSN
        else:#validation
            if conceptFSN !=IDToFSN.get(conceptID):
                print("error"+"multiple FSN for one ID")
                print(conceptID)
                print(conceptFSN)
                print(IDToFSN.get(conceptID))
                continue        
file1.close()
###############################################################################
#Version1: Generate new DiGraph for NLS and compare with the orignial one 
###############################################################################
def compareWithOld(node):
    Dic = dict(nx.bfs_successors(originalGraph,node))
    d = Dic.values()
    c = []
    for e in d:
        c.extend(e)
    return c      
#       
def compareWithNew(node):
    Dic = dict(nx.bfs_successors(reductedNewGraph,node))
    d = Dic.values()
    c = []
    for e in d:
        c.extend(e)
    return c  
#
logicDefDic = {}
AllConceptInSNOMED = list(Dirgraph1.nodes)
AllConceptInSNOMED.remove("138875005")
for eachConcept in AllConceptInSNOMED:
    logicDefDic[eachConcept] = computeGroupedInferredAttributes(eachConcept)
#
import networkx as nx
import csv
file1 = open("SNOMED_20200301_nlss.txt","r")
file2 = open("OriginalIS-A_notCovered_All(<>10).csv","w")
file3 = open("PotentiallyMissingIS-A_All(<>10).csv","a")
writer2 = csv.writer(file2)
writer3 = csv.writer(file3)
nlsCount = 0
for lines1 in file1:
    nlsCount = nlsCount+1
    if nlsCount>5695:
        line1 = lines1.split("\n")[0].split(";")
        print(nlsCount,line1[4])
        if int(line1[4]) >0:
            #Build old graph
            originalGraph = nx.DiGraph()
            originalEdges = [tuple(x.split("|")) for x in line1[3].split(",")]
            originalGraph.add_edges_from(originalEdges)
            #Compare logical definitions and get new graph
            allNodes = list(originalGraph.nodes)
            #logicDefDic = {}
            #for eachConcept in allNodes:
            #    logicDefDic[eachConcept] = computeGroupedInferredAttributes(eachConcept)
            #newGraph = nx.DiGraph()
            for i in range(0,len(allNodes)-1):
                concept1LogicDef = logicDefDic.get(allNodes[i])
                for j in range(i+1,len(allNodes)):
                    if allNodes[i] not in ancestorDic1.get(allNodes[j]) and allNodes[j] not in ancestorDic1.get(allNodes[i]):
                        concept2LogicDef = logicDefDic.get(allNodes[j])
                        if concept1LogicDef != concept2LogicDef:
                            if checkLogicalDefGeneral(concept1LogicDef,concept2LogicDef):
                                originalGraph.add_edge(allNodes[j],allNodes[i])
                            elif checkLogicalDefGeneral(concept2LogicDef,concept1LogicDef):
                                originalGraph.add_edge(allNodes[i],allNodes[j])
            if len(list(nx.simple_cycles(originalGraph))) == 0:
                reductedNewGraph = nx.transitive_reduction(originalGraph)
                #allNodesNew = list(reductedNewGraph.nodes)
                newEdges = list(reductedNewGraph.edges)   
                #Compare old and new hierarchy
                '''
                for oldEdge in originalEdges: #(0 is-a 1)
                    if oldEdge[1] not in allNodesNew or oldEdge[0] not in allNodesNew:
                        writer2.writerow((nlsCount,line1[4],oldEdge[0],oldEdge[1],IDToFSN.get(oldEdge[0]),IDToFSN.get(oldEdge[1]),activeConcept.get(oldEdge[0]),activeConcept.get(oldEdge[1])))
                    elif oldEdge[1] not in compareWithNew(oldEdge[0]):
                        writer2.writerow((nlsCount,line1[4],oldEdge[0],oldEdge[1],IDToFSN.get(oldEdge[0]),IDToFSN.get(oldEdge[1]),activeConcept.get(oldEdge[0]),activeConcept.get(oldEdge[1])))
                '''
                for newEdge in newEdges:
                    if newEdge[1] not in ancestorDic1.get(newEdge[0]):
                        writer3.writerow((nlsCount,line1[4],newEdge[0],newEdge[1],IDToFSN.get(newEdge[0]),IDToFSN.get(newEdge[1]),activeConcept.get(newEdge[0]),activeConcept.get(newEdge[1])))
            else:
                print("error",nlsCount)
file1.close()
file2.close()
file3.close()
###############################################################################
#Version2: Generate all candidate pairs and for each pair, perform subsumption checking
###############################################################################
import networkx as nx
import csv
file1 = open("SNOMED_20200301_nlss.txt","r")
file2 = open("PotentiallyMissingIS-A_(<>10)_new.csv","w")
file3 = open("CandidatePair.csv","w")
writer2 = csv.writer(file2)
writer3 = csv.writer(file3)
candidatePair = set()
nlsCount = 0
for lines1 in file1:
    nlsCount = nlsCount+1
    print(nlsCount)
    line1 = lines1.split("\n")[0].split(";")
    #print(nlsCount,line1[4])
    #if int(line1[4]) >0:
    allNodes = line1[2].split("|")
    #logicDefDic = {}
    #for eachConcept in allNodes:
    #    logicDefDic[eachConcept] = computeGroupedInferredAttributes(eachConcept)
    #newGraph = nx.DiGraph()
    for i in range(0,len(allNodes)-1):
        for j in range(i+1,len(allNodes)):
            if allNodes[i] not in ancestorDic1.get(allNodes[j]) and allNodes[j] not in ancestorDic1.get(allNodes[i]):
                candidatePair.add(tuple(sorted((allNodes[i],allNodes[j]))))
file1.close()
#
k=0
for eachPair in candidatePair:
    writer3.writerow(eachPair)
    k = k+1
    print(k)
    concept1LogicDef = logicDefDic.get(eachPair[0])
    concept2LogicDef = logicDefDic.get(eachPair[1])
    if concept1LogicDef != concept2LogicDef:
        if checkLogicalDefGeneral(concept1LogicDef,concept2LogicDef):
            writer2.writerow((eachPair[1],eachPair[0],IDToFSN.get(eachPair[1]),IDToFSN.get(eachPair[0]),activeConcept.get(eachPair[1]),activeConcept.get(eachPair[0])))
        elif checkLogicalDefGeneral(concept2LogicDef,concept1LogicDef):
            writer2.writerow((eachPair[0],eachPair[1],IDToFSN.get(eachPair[0]),IDToFSN.get(eachPair[1]),activeConcept.get(eachPair[0]),activeConcept.get(eachPair[1]))) 
file2.close()
file3.close()
###############################################################################
#Step7: Further Filter  
############################################################################### 
file1 = open("activeConcept20200301.txt","r")
file2 = open("activeFSN20200301.txt","r")
activeConcept = set()
for lines1 in file1:
    line1 = lines1.split()
    activeConcept.add(line1[0])
file1.close()
#
import string
allowed = string.ascii_letters + string.digits
#
def checkNotSymbol(inputToken):
    if any(x in allowed for x in inputToken) == True:
        return True
    else:
        return False
#    
BagOfWord = {}
for lines2 in file2:
    line2 = lines2.split("\n")[0].split("\t")
    if line2[0] in activeConcept:
        BOW = " (".join(line2[1].lower().replace(",", " ,").replace(":"," :").replace("."," .").split(" (")[:-1]).split(" ")
        BOW_noSingleSymbol =[x for x in BOW if checkNotSymbol(x)] 
        BagOfWord[line2[0]] = list(set(BOW_noSingleSymbol)) 
file2.close()
#
output3 = open("enrichedBagOfWords.txt","w")
activeConceptL = list(activeConcept)
for eachConcept in activeConceptL:
    eachConceptAncestors = findAncestors(eachConcept)
    if BagOfWord.get(eachConcept,"DEFAULT") == "DEFAULT":
        print("error")
    else:
        enrichedBOW = BagOfWord.get(eachConcept)
    for eachAncestor in eachConceptAncestors:
        ancestorBOW = BagOfWord.get(eachAncestor) 
        enrichedBOW.extend(ancestorBOW)
    enrichedBOW = list(set(enrichedBOW))
    output3.write(eachConcept+"\t"+"\t".join(enrichedBOW)+"\n")
output3.close()
#
enrichedBagOfWord = {}
file3 = open("enrichedBagOfWords.txt","r")
for lines3 in file3:
    line3 = lines3.split("\n")[0].split("\t")
    enrichedBagOfWord[line3[0]] = line3[1:]
file3.close()
############################################################################### 
import csv
import spacy
#import re
from spacy.tokenizer import Tokenizer #split by space #default for Tokenizer
nlp = spacy.load("en_core_web_lg")
#nlp = spacy.load("en_core_sci_lg")
nlp.tokenizer = Tokenizer(nlp.vocab)
import string
allowed = string.ascii_letters + string.digits
#
def checkNotSymbol(inputToken):
    if any(x in allowed for x in inputToken) == True:
        return True
    else:
        return False
#
file4 = open("activeFSN20200301.txt","r")
output5 = open("Concept_nounChunks.csv","w")
writer5 = csv.writer(output5)
for lines4 in file4:
    line4 = lines4.split("\n")[0].split("\t")
    conceptID = line4[0]
    conceptNameWithSemanticTag = line4[1].replace(","," ,").replace(":"," :").replace("."," .").lower()
    midList = conceptNameWithSemanticTag.split(" (")
    conceptName = " (".join(midList[0:-1])
    #print(conceptName)
    doc = nlp(conceptName)
    LinguisticFeature = []
    LinguisticFeature.append(conceptID)
    for nounChunks in doc.noun_chunks:
        if checkNotSymbol(nounChunks.text) == True and len(nounChunks)>1:
            LinguisticFeature.append(nounChunks.text)
    writer5.writerow(tuple(LinguisticFeature))
file4.close()
output5.close()
#
import csv
nounChunks = {}
file1 = open("activeConcept20200301.txt","r")
file2 = open("Concept_nounChunks.csv","r")
activeConcept = set()
for lines1 in file1:
    line1 = lines1.split()
    activeConcept.add(line1[0])
file1.close()
reader2 = csv.reader(file2)
for row2 in reader2:
    if row2[0] in activeConcept:
        nounChunks[row2[0]] = row2[1:]
#
output = open("enrichedConcept_nounChunks.csv","w")
outputWriter = csv.writer(output)
activeConceptL = list(activeConcept)
for eachConcept in activeConceptL:
    eachConceptAncestors = findAncestors(eachConcept)
    if nounChunks.get(eachConcept,"DEFAULT") == "DEFAULT":
        print("error")
    else:
        enrichedNP = nounChunks.get(eachConcept)
    for eachAncestor in eachConceptAncestors:
        ancestorNP = nounChunks.get(eachAncestor) 
        enrichedNP.extend(ancestorNP)
    enrichedNP = list(set(enrichedNP))
    outputWriter.writerow((eachConcept,)+tuple(enrichedNP))
file2.close()
output.close()
#
enrichedNounChunks = {}
file1 = open("enrichedConcept_nounChunks.csv","r") 
reader1 = csv.reader(file1)
for row1 in reader1:
    enrichedNounChunks[row1[0]] = row1[1:]
file1.close()
###############################################################################
import csv
file1 = open("PotentiallyMissingIS-A_(<>10)_new.csv","r") 
reader1 = csv.reader(file1)
output1 = open("PotentiallyMissingIS-A_Filtered(<>10)_npBOW.csv","w")
writer1 = csv.writer(output1)
for row1 in reader1:
    parentDef = logicDefDic.get(row1[1])
    childDef = logicDefDic.get(row1[0])
    parentAttributeType = []
    for group in parentDef:
        for relation in group:
            if relation[0]!= "116680003":
                parentAttributeType.append(relation[0])
    if len(parentAttributeType)>0:
        parentDefISARemoved = sorted([y for y in parentDef if y[0][0]!="116680003"])
        childDefISARemoved = sorted([z for z in childDef if z[0][0]!="116680003"])
        if row1[4] == "defined" and parentDefISARemoved!=childDefISARemoved:
            childNP = enrichedNounChunks.get(row1[0])
            childBOW = enrichedBagOfWord.get(row1[0])
            parentNP = enrichedNounChunks.get(row1[1])
            parentBOW = enrichedBagOfWord.get(row1[1])
            #if set(childNP).issuperset(set(parentNP)):
            if set(childNP).issuperset(set(parentNP)) and set(childBOW).issuperset(set(parentBOW)):
                writer1.writerow(tuple(row1))            
file1.close()
output1.close()
#add nls information
import csv
file1 = open("PotentiallyMissingIS-A_Filtered(<>10)_npBOW.csv","r")
reader1 = csv.reader(file1)
output1 = open("PotentiallyMissingIS-A_Filtered(<>10)_npBOW_NLSIndexed.csv","w")
file2 = open("SNOMED_20200301_nlss.txt","r")
writer1 = csv.writer(output1)
nlsInfo = []
for lines2 in file2:
    line2 = lines2.split("\n")[0].split(";")
    nlsInfo.append(line2[2].split("|"))
file2.close()
j = 0
for row1 in reader1:
    j = j+1
    print(j)
    row1nls = []
    for i in range(len(nlsInfo)):
        if row1[0] in nlsInfo[i] and row1[1] in nlsInfo[i]:
            row1nls.append(str(i)+":"+str(len(nlsInfo[i])))
    writer1.writerow(tuple(row1)+("|".join(row1nls),))
file1.close()
output1.close()
###############################################################################
#Step8: Redundancy removal 
############################################################################### 
import csv
import networkx as nx
file2 = open("hierarchicalRelation(childParent)20200301.txt","rb")
Dirgraph1 = nx.read_edgelist(file2, create_using = nx.DiGraph(), nodetype = str)
file2.close()
file1 = open("PotentiallyMissingIS-A_Filtered(<>10)_npBOW_NLSIndexed.csv","r")
reader1 = csv.reader(file1)
allMissingISA = []
for row1 in reader1:
    Dirgraph1.add_edge(row1[0],row1[1])
    allMissingISA.append(row1)
file1.close()
reductedNewGraph = nx.transitive_reduction(Dirgraph1)
newEdges = list(reductedNewGraph.edges)   
file3 = open("PotentiallyMissingIS-A_Filtered(<>10)_npBOW_NLSIndexed_NonRedundant.csv","w")
writer3 = csv.writer(file3)
for eachMissingISA in allMissingISA:
    if (eachMissingISA[0],eachMissingISA[1]) in newEdges:
        writer3.writerow(tuple(eachMissingISA))       
file3.close()