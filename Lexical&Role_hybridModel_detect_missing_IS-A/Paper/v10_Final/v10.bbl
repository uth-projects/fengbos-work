\begin{thebibliography}{10}

\bibitem{ncitsource}
NCI Thesaurus;.
\newblock [Accessed 15-Feb-2020].
\newblock \url{https://ncit.nci.nih.gov/ncitbrowser/}.

\bibitem{hartel2005modeling}
Hartel FW, de~Coronado S, Dionne R, Fragoso G, Golbeck J.
\newblock Modeling a description logic vocabulary for cancer research.
\newblock Journal of biomedical informatics. 2005;38(2):114--129.

\bibitem{de2004nci}
De~Coronado S, Haber MW, Sioutos N, Tuttle MS, Wright LW, et~al.
\newblock NCI Thesaurus: using science-based terminology to integrate cancer
  research results.
\newblock In: Medinfo; 2004. p. 33--37.

\bibitem{sioutos2007nci}
Sioutos N, de~Coronado S, Haber MW, Hartel FW, Shaiu WL, Wright LW.
\newblock NCI Thesaurus: a semantic model integrating cancer-related clinical
  and molecular information.
\newblock Journal of biomedical informatics. 2007;40(1):30--43.

\bibitem{fragoso2004overview}
Fragoso G, de~Coronado S, Haber M, Hartel F, Wright L.
\newblock Overview and utilization of the NCI thesaurus.
\newblock International Journal of Genomics. 2004;5(8):648--654.

\bibitem{haendel2018census}
Haendel MA, McMurry JA, Relevo R, Mungall CJ, Robinson PN, Chute CG.
\newblock A census of disease ontologies.
\newblock Annual Review of Biomedical Data Science. 2018;1:305--331.

\bibitem{zheng2019hybrid}
Zheng F, Abeysinghe R, Cui L.
\newblock A Hybrid Method to Detect Missing Hierarchical Relations in NCI
  Thesaurus.
\newblock In: 2019 IEEE International Conference on Bioinformatics and
  Biomedicine (BIBM). IEEE; 2019. p. 1948--1953.

\bibitem{ochs2015tribal}
Ochs C, Geller J, Perl Y, Chen Y, Agrawal A, Case JT, et~al.
\newblock A tribal abstraction network for SNOMED CT target hierarchies without
  attribute relationships.
\newblock Journal of the American Medical Informatics Association.
  2015;22(3):628--639.

\bibitem{chen2009structural}
Chen Y, Gu HH, Perl Y, Geller J.
\newblock Structural group-based auditing of missing hierarchical relationships
  in UMLS.
\newblock Journal of biomedical informatics. 2009;42(3):452--467.

\bibitem{bodenreider2016identifying}
Bodenreider O.
\newblock Identifying Missing Hierarchical Relations in SNOMED CT from Logical
  Definitions Based on the Lexical Features of Concept Names.
\newblock ICBO/BioCreative. 2016;2016.

\bibitem{quesada2016suggesting}
Quesada-Mart{\'\i}nez M, Fern{\'a}ndez-Breis JT, Karlsson D.
\newblock Suggesting Missing Relations in Biomedical Ontologies Based on
  Lexical Regularities.
\newblock In: MIE; 2016. p. 384--388.

\bibitem{abeysinghe2018lexical}
Abeysinghe R, Zheng F, Hinderer EW, Moseley HN, Cui L.
\newblock A Lexical Approach to Identifying Subtype Inconsistencies in
  Biomedical Terminologies.
\newblock In: 2018 IEEE International Conference on Bioinformatics and
  Biomedicine (BIBM). IEEE; 2018. p. 1982--1989.

\bibitem{liu2018can}
Liu H, Zheng L, Perl Y, Geller J, Elhanan G.
\newblock Can a Convolutional Neural Network Support Auditing of NCI Thesaurus
  Neoplasm Concepts?
\newblock In: ICBO; 2018. .

\bibitem{zhang2010large}
Zhang GQ, Bodenreider O.
\newblock Large-scale, exhaustive lattice-based structural auditing of SNOMED
  CT.
\newblock In: AMIA Annual Symposium Proceedings. vol. 2010. American Medical
  Informatics Association; 2010. p. 922--926.

\bibitem{zhang2014maple}
Zhang GQ, Zhu W, Sun M, Tao S, Bodenreider O, Cui L.
\newblock MaPLE: a MapReduce pipeline for lattice-based evaluation and its
  application to SNOMED CT.
\newblock In: 2014 IEEE International Conference on Big Data (Big Data). IEEE;
  2014. p. 754--759.

\bibitem{cui2016biomedical}
Cui L, Tao S, Zhang GQ.
\newblock Biomedical ontology quality assurance using a big data approach.
\newblock ACM Transactions on Knowledge Discovery from Data (TKDD).
  2016;10(4):41.

\bibitem{cui2017mining}
Cui L, Zhu W, Tao S, Case JT, Bodenreider O, Zhang GQ.
\newblock Mining non-lattice subgraphs for detecting missing hierarchical
  relations and concepts in SNOMED CT.
\newblock Journal of the American Medical Informatics Association.
  2017;24(4):788--798.

\bibitem{cui2018auditing}
Cui L, Bodenreider O, Shi J, Zhang GQ.
\newblock Auditing SNOMED CT hierarchical relations based on lexical features
  of concepts in non-lattice subgraphs.
\newblock Journal of biomedical informatics. 2018;78:177--184.

\bibitem{abeysinghe2017quality}
Abeysinghe R, Brooks MA, Talbert J, Cui L.
\newblock Quality assurance of NCI Thesaurus by mining structural-lexical
  patterns.
\newblock In: AMIA Annual Symposium Proceedings. vol. 2017. American Medical
  Informatics Association; 2017. p. 364--373.

\bibitem{abeysinghe2019leveraging}
Abeysinghe R, Brooks MA, Cui L.
\newblock Leveraging Non-lattice Subgraphs to Audit Hierarchical Relations in
  NCI Thesaurus.
\newblock In: AMIA Annual Symposium Proceedings. vol. 2019. American Medical
  Informatics Association; 2019. p. 982–--991.

\bibitem{sun2019validating}
Sun Q, Zhang G, Zhu W, Cui L.
\newblock Validating Auto-Suggested Changes for SNOMED CT in Non-Lattice
  Subgraphs Using Relational Machine Learning.
\newblock Studies in health technology and informatics. 2019;264:378--382.

\bibitem{zheng2018exploring}
Zheng F, Cui L.
\newblock Exploring Deep Learning-based Approaches for Predicting Concept Names
  in SNOMED CT.
\newblock In: 2018 IEEE International Conference on Bioinformatics and
  Biomedicine (BIBM). IEEE; 2018. p. 808--813.

\bibitem{zhang2018efficient}
Zhang GQ, Xing G, Cui L.
\newblock An efficient, large-scale, non-lattice-detection algorithm for
  exhaustive structural auditing of biomedical ontologies.
\newblock Journal of biomedical informatics. 2018;80:106--119.

\bibitem{spacynlp}
SpaCy: Industrial-Strength Natural Language Processing;.
\newblock [Accessed 15-Feb-2020].
\newblock \url{https://spacy.io/}.

\bibitem{ncitdownloads}
NCI Thesaurus Downloads;.
\newblock [Accessed 19-July-2020].
\newblock \url{https://evs.nci.nih.gov/evs-download/thesaurus-downloads}.

\bibitem{ochs2015scalable}
Ochs C, Geller J, Perl Y, Chen Y, Xu J, Min H, et~al.
\newblock Scalable quality assurance for large SNOMED CT hierarchies using
  subject-based subtaxonomies.
\newblock Journal of the American Medical Informatics Association.
  2015;22(3):507--518.

\bibitem{ochs2016utilizing}
Ochs C, He Z, Zheng L, Geller J, Perl Y, Hripcsak G, et~al.
\newblock Utilizing a structural meta-ontology for family-based quality
  assurance of the BioPortal ontologies.
\newblock Journal of biomedical informatics. 2016;61:63--76.

\bibitem{doi:10.1200/CCI.19.00124}
Cui L, Abeysinghe R, Zheng F, Tao S, Zeng N, Hands I, et~al.
\newblock Enhancing the Quality of Hierarchic Relations in the National Cancer
  Institute Thesaurus to Enable Faceted Query of Cancer Registry Data.
\newblock JCO Clinical Cancer Informatics. 2020;(4):392--398.

\end{thebibliography}
