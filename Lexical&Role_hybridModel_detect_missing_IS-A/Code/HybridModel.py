#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 11 10:09:40 2019

@author: fengbozheng
"""

######################################################################################
#Precompute lexical features and enriched lexical features
######################################################################################
import networkx as nx
file2 = open("hierarchicalRelation(ChildParent)1908.txt","rb")
Dirgraph1 = nx.read_edgelist(file2, create_using = nx.DiGraph(), nodetype = str)

def findAncestors(node):
    Dic = dict(nx.bfs_successors(Dirgraph1,node))
    d = Dic.values()
    c = []
    for e in d:
        c.extend(e)
    return c  
file2.close()
allNodesInNCIt =  list(Dirgraph1.nodes)
#
allSubRoot = [n for n,d in Dirgraph1.out_degree() if d==0] 
'''
output3 = open("conceptAndAncestors.txt","w")
for eachConcept in allNodesInNCIt:
    ancestors = findAncestors(eachConcept)
    output3.write(eachConcept+"\t"+"\t".join(ancestors)+"\n")
output3.close()
'''
ancestorInfo = {}
file3 = open("conceptAndAncestors.txt","r")
for lines3 in file3:
    line3 = lines3.split()
    ancestorInfo[line3[0]] = line3[1:]
file3.close()
#
#conceptLexicalFeature.csv
import csv
#conceptLexicalSplitBySpace.csv
file4 = open("conceptInformation1908Inferred.txt","r")
output4 = open("conceptLexicalSplitBySpace.csv","w")
writer4 = csv.writer(output4)
for lines4 in file4:
    line4 = lines4.split("\n")[0].split("\t")
    conceptID = line4[0]
    conceptName = line4[2].replace(","," ,").replace(":"," :").replace("."," .")
    lexicalFeature = []
    lexicalFeature.append(conceptID)
    for token in conceptName.split(" "):
        if token != "":
            lexicalFeature.append(token.lower())
    writer4.writerow(tuple(lexicalFeature))
file4.close()
output4.close()
#
conceptLexicalSplitBySpace = {}
file5 = open("conceptLexicalSplitBySpace.csv","r")
reader5 = csv.reader(file5)
for row5 in reader5:
    conceptLexicalSplitBySpace[row5[0]] = row5[1:]
file5.close()
#Stop Words Version:
stopWordList = ["and", "and/or", "or", "no", "not", "without", "except", "by", "after", "able", "removal", "replacement", "nos"]
stopPhraseList = [["due","to"],["secondary", "to"]]
def subList(a,b): #check if a in b
    j = 0
    for i in range(len(b)):
        if b[i:i+len(a)] == a:
            j = j+1
    if j ==0:
        return False
    else:
        return True
#       
output5 = open("conceptEnrichedLexicalSBS_noStopWordInherited.csv","w")
writer5 = csv.writer(output5)
for concepts in allNodesInNCIt:
    itsLexical = []
    itsLexical.append(concepts)
    itsLexical = itsLexical + conceptLexicalSplitBySpace.get(concepts)
    ancestors = ancestorInfo.get(concepts)
    for ancestor in ancestors:
        ancestorLex = conceptLexicalSplitBySpace.get(ancestor)
        if any(x in ancestorLex for x in stopWordList) == False:
            if any(subList(a,ancestorLex) for a in stopPhraseList) == False:
                for lexItem in ancestorLex:
                    if lexItem not in itsLexical:
                        itsLexical.append(lexItem)
    writer5.writerow(tuple(itsLexical))
output5.close()
#
enrichedLexicalSBS = {}
file6 = open("conceptEnrichedLexicalSBS_noStopWordInherited.csv","r")
reader6 =csv.reader(file6)
for row6 in reader6:
    enrichedLexicalSBS[row6[0]] = row6[1:]
file6.close()
#
import spacy
nlp = spacy.load("en_core_web_lg")
file4 = open("conceptInformation1908Inferred.txt","r")
output4 = open("conceptNPRoot.csv","w")
writer4 = csv.writer(output4)
for lines4 in file4:
    line4 = lines4.split("\n")[0].split("\t")
    conceptID = line4[0]
    conceptName = line4[2]
    doc = nlp(conceptName)
    NPRoot = []
    NPRoot.append(conceptID)
    NPRoot.append(conceptName)
    for chunk in doc.noun_chunks:
        NPRoot.append(chunk.root.text.lower())
    writer4.writerow(tuple(NPRoot))
file4.close()
output4.close()
#
conceptNPRoot = {}
file12 = open("conceptNPRoot_sci.csv","r")
reader12 = csv.reader(file12)
for row12 in reader12:
    conceptNPRoot[row12[0]] = row12[2:]
file12.close()
#
output5 = open("conceptEnrichedNPRoot_noStopWordInherited_sci.csv","w")
writer5 = csv.writer(output5)
for concepts in allNodesInNCIt:
    itsNPRoot = []
    itsNPRoot.append(concepts)
    itsNPRoot = itsNPRoot + conceptNPRoot.get(concepts)
    ancestors = ancestorInfo.get(concepts)
    for ancestor in ancestors:
        ancestorLex = conceptLexicalSplitBySpace.get(ancestor)
        if any(x in ancestorLex for x in stopWordList) == False:
            if any(subList(a,ancestorLex) for a in stopPhraseList) == False:
                ancestorNPR = conceptNPRoot.get(ancestor)
                for lexItem in ancestorNPR:
                    if lexItem not in itsNPRoot:
                        itsNPRoot.append(lexItem)
        writer5.writerow(tuple(itsNPRoot))
output5.close()
#
enrichedNPRoot = {}
file13 = open("conceptEnrichedNPRoot_noStopWordInherited_sci.csv","r")
reader13 = csv.reader(file13)
for row13 in reader13:
    enrichedNPRoot[row13[0]] = row13[1:]
file13.close()
###############################################################################
#IsaRemovedDefinition1908Inferred.txt
#IasRemovedDefinition1908Asserted.txt
conceptDefPlain = {}  #conceptID: list of (type, value) pairs
file7 = open("conceptRelation1908Inferred.txt","r")
for lines7 in file7:
    line7 = lines7.split("\n")[0].split("\t")
    if conceptDefPlain.get(line7[0],"default") == "default":
        conceptDefPlain[line7[0]] = [(line7[1],line7[2])]
    else:
        conceptDefPlain[line7[0]].append((line7[1],line7[2]))
file7.close()
#
IsaRemovedDef = {}
for eachNode in allNodesInNCIt:
    originalDef = conceptDefPlain.get(eachNode,"empty")
    if originalDef != "empty":
        IsaRemovedDefinition = []
        for eachRoleRelation in originalDef:
            if eachRoleRelation[0]!= "ISA2019FZ":
                IsaRemovedDefinition.append(eachRoleRelation)
        IsaRemovedDef[eachNode] = IsaRemovedDefinition
    else:
        IsaRemovedDef[eachNode] = []
#        
output6 = open("IsaRemovedDefinition1908Asserted.txt","w")
for node in allNodesInNCIt:
    nodeIsaRemovedDef = IsaRemovedDef.get(node)
    if len(nodeIsaRemovedDef)>0:
        newIsaDef =[x[0]+","+x[1] for x in nodeIsaRemovedDef]
        output6.write(node+"\t"+"|".join(newIsaDef)+"\n")
    else:
        output6.write(node+"\t"+"empty"+"\n")
output6.close()
#
file8 = open("IsaRemovedDefinition1908Inferred.txt","r")
#file9 = open("IsaRemovedDefinition1908Asserted.txt","r")
IsaRemovedDefInferred = {}
#IsaRemovedDefAsserted = {}
for lines8 in file8:
    line8 = lines8.split("\n")[0].split("\t")
    concept = line8[0]
    if line8[1]!= "empty":
        defList = []
        conceptDefStringL = line8[1].split("|")
        for segment in conceptDefStringL:
            defList.append((segment.split(",")[0],segment.split(",")[1]))
        IsaRemovedDefInferred[concept] = defList
    else:
        IsaRemovedDefInferred[concept] = []
file8.close()
'''
for lines9 in file9:
    line9 = lines9.split("\n")[0].split("\t")
    concept = line9[0]
    if line9[1]!= "empty":
        defList = []
        conceptDefStringL = line9[1].split("|")
        for segment in conceptDefStringL:
            defList.append((segment.split(",")[0],segment.split(",")[1]))
        IsaRemovedDefAsserted[concept] = defList
    else:
        IsaRemovedDefAsserted[concept] = []
file9.close()
'''
###############################################################################
#Logical definition subsumption checking
###############################################################################
#check if attributeB is more detailed than attribute A
def checkSingleDetailed(attributeB,attributeA):
    if attributeB[0] == attributeA[0]:
        if attributeB[0] not in ["R135","R136","R137","R138","R139","R140","R141","R142"]:
            if (attributeA[1] in ancestorInfo.get(attributeB[1])) or (attributeB[1] == attributeA[1]):
                return True
            else:
                return False
        else:
# EXCLUDE SOMETHING, THEN IF EXCLUDE SOME SEPCIFIC THING WILL BE MORE GENERAL (less individual)
            if (attributeB[1] in ancestorInfo.get(attributeA[1])) or (attributeB[1] == attributeA[1]):
                return True
            else:
                return False
    else:
        return False
#check if group of attribute -- attributeBL is more detailed than attributeA
def checkGroupSingleDetaild(attributeBL,attributeA):
    if any(checkSingleDetailed(singleAttr,attributeA) for singleAttr in attributeBL):
        return True
    else:
        return False        
###############################################################################
#Detection of Missing IS-A relations within NLS
###############################################################################
#Definition Status and ConCept Name
conceptInfo = {}    #conceptID: (P/D, Label)
file11 = open("conceptInformation1908Inferred.txt","r")
for lines11 in file11:
    line11 = lines11.split("\n")[0].split("\t")
    if conceptInfo.get(line11[0],"default") == "default":
        conceptInfo[line11[0]] = (line11[1],line11[2])
    else:
        print("error")
file11.close()
#
associationPair = {}
file12 = open("association1908Inferred.txt","r")
for lines12 in file12:
    line12 = lines12.split()
    if associationPair.get(line12[0],"default") == "default":
        associationPair[line12[0]] = [line12[2]]
    else:
        if line12[2] not in associationPair.get(line12[0]):
            associationPair[line12[0]].append(line12[2])
    if associationPair.get(line12[2],"default") == "default":
        associationPair[line12[2]] = [line12[0]]
    else:
        if line12[0] not in associationPair.get(line12[2]):
            associationPair[line12[2]].append(line12[0])
file12.close()
#Version3: Use Stop Words
file10= open("NCIT_1908_nonlattice_bottom_up_closure.txt","r")
i = 0  #NLS sequence number
output7 = open("missingIS-A_noStopWord_LongVSShort_sci.csv","w")
import csv
writer7 = csv.writer(output7)
for lines10 in file10:
    i = i+1
    print(i)
    line10 = lines10.split(";")[2]
    nodesInNLS = line10.split("|") 
    for j in range(0,(len(nodesInNLS)-1)):
        #concept1Def = conceptDefPlain.get(nodesInNLS[j])
        concept1DefISARemoved = IsaRemovedDefInferred.get(nodesInNLS[j])
        #concept1ShortLex = conceptLexical.get(nodesInNLS[j])
        #concept1LongLex = enrichedLexical.get(nodesInNLS[j])
        concept1ShortLex = conceptLexicalSplitBySpace.get(nodesInNLS[j])
        concept1LongLex = enrichedLexicalSBS.get(nodesInNLS[j])
        concept1LongNPR = enrichedNPRoot.get(nodesInNLS[j])
        concept1ShortNPR = conceptNPRoot.get(nodesInNLS[j])
        #concept1Stated = IsaRemovedDefAsserted.get(nodesInNLS[j])
        if len(concept1DefISARemoved) >0 and any(x in concept1ShortLex for x in stopWordList) == False and any(subList(a,concept1ShortLex) for a in stopPhraseList) == False: #and len(concept1Stated)>0:
            for k in range(j+1,(len(nodesInNLS))):
                if (nodesInNLS[j] not in ancestorInfo.get(nodesInNLS[k])) and (nodesInNLS[k] not in ancestorInfo.get(nodesInNLS[j])):
                    concept2DefISARemoved = IsaRemovedDefInferred.get(nodesInNLS[k])   
                    #concept2Def = conceptDefPlain.get(nodesInNLS[k])
                    #concept2ShortLex =  conceptLexical.get(nodesInNLS[k])
                    #concept2LongLex = enrichedLexical.get(nodesInNLS[k])
                    concept2ShortLex =  conceptLexicalSplitBySpace.get(nodesInNLS[k])
                    concept2LongLex = enrichedLexicalSBS.get(nodesInNLS[k])
                    concept2LongNPR = enrichedNPRoot.get(nodesInNLS[k])
                    concept2ShortNPR = conceptNPRoot.get(nodesInNLS[k])
                    #concept2Stated = IsaRemovedDefAsserted.get(nodesInNLS[k])
                    if len(concept2DefISARemoved)>0 and any(x in concept2ShortLex for x in stopWordList) == False and any(subList(a,concept2ShortLex) for a in stopPhraseList) == False: # and len(concept2Stated)>0:
                        #NO EQUAL
                        if sorted(concept1DefISARemoved)!= sorted(concept2DefISARemoved):
                            #ISA Removed    
                            if all(checkGroupSingleDetaild(concept1DefISARemoved,Attr2) for Attr2 in concept2DefISARemoved) == True:
                                if set(concept1LongLex).issuperset(set(concept2ShortLex)):
                                    if set(concept1LongNPR).issuperset(set(concept2ShortNPR)):
                                        if nodesInNLS[k] not in associationPair.get(nodesInNLS[j],[]) and nodesInNLS[j] not in associationPair.get(nodesInNLS[k],[]):
                                            writer7.writerow((nodesInNLS[j],conceptInfo.get(nodesInNLS[j])[1],nodesInNLS[k],conceptInfo.get(nodesInNLS[k])[1],conceptInfo.get(nodesInNLS[j])[0],conceptInfo.get(nodesInNLS[k])[0],str(i)))
                            if all(checkGroupSingleDetaild(concept2DefISARemoved,Attr1) for Attr1 in concept1DefISARemoved) == True:
                                if set(concept2LongLex).issuperset(set(concept1ShortLex)):
                                    if set(concept2LongNPR).issuperset(set(concept1ShortNPR)):
                                        if nodesInNLS[k] not in associationPair.get(nodesInNLS[j],[]) and nodesInNLS[j] not in associationPair.get(nodesInNLS[k],[]):
                                            writer7.writerow((nodesInNLS[k],conceptInfo.get(nodesInNLS[k])[1],nodesInNLS[j],conceptInfo.get(nodesInNLS[j])[1],conceptInfo.get(nodesInNLS[k])[0],conceptInfo.get(nodesInNLS[j])[0],str(i)))
file10.close()
output7.close()    