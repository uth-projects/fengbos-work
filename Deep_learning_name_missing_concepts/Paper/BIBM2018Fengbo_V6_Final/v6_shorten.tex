\documentclass[conference]{IEEEtran}
\IEEEoverridecommandlockouts
% The preceding line is only needed to identify funding in the first footnote. If that is unneeded, please comment it out.
\usepackage{cite}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{algorithmic}
%\usepackage{titlesec}
\usepackage{graphicx}
\usepackage{textcomp}
\usepackage{xcolor}
\def\BibTeX{{\rm B\kern-.05em{\sc i\kern-.025em b}\kern-.08em
    T\kern-.1667em\lower.7ex\hbox{E}\kern-.125emX}}
    
%\setlength{\intextsep}{1.0pt plus 1.0pt minus 2.0pt}
%\setlength{\parskip}{0em} 
%\setlength{\abovecaptionskip}{0cm}
%\setlength{\belowcaptionskip}{0cm}


\begin{document}
\title{Exploring Deep Learning-based Approaches for Predicting Concept Names in SNOMED CT\\  
\iffalse {\footnotesize \textsuperscript{*}Note: Sub-titles are not captured in Xplore and
should not be used} \fi
\thanks{This work was supported by the National Science Foundation under grants 1816805 and 1657306, and by the National Institutes of Health under grant R21CA231904. Correspondence: licong.cui@uky.edu}
}

\author{\IEEEauthorblockN{Fengbo Zheng\IEEEauthorrefmark{1},
Licong Cui\IEEEauthorrefmark{1}\IEEEauthorrefmark{2}}

\IEEEauthorblockA{\IEEEauthorrefmark{1}Department of Computer Science}
\IEEEauthorblockA{\IEEEauthorrefmark{2}Institute for Biomedical Informatics\\ 
	University of Kentucky,
	Lexington, Kentucky, USA}
}

%\author{\IEEEauthorblockN{Fengbo Zheng}
%\IEEEauthorblockA{\textit{Department of Computer Science} \\
%\textit{University of Kentucky}\\
%Lexington, Kentucky, USA \\
%fengbo.zheng@uky.edu}
%\and
%\IEEEauthorblockN{Licong Cui}
%\IEEEauthorblockA{\textit{Department of Computer Science} \\
%\textit{Institute for Biomedical Informatics}\\
%\textit{University of Kentucky}\\
%Lexington, Kentucky, USA \\
%licong.cui@uky.edu}
%}

\maketitle

\begin{abstract}
Ontologies or terminologies have been widely used as formal representation of biomedical knowledge. New concepts are constantly added to biomedical ontologies due to the evolving nature of biomedical knowledge. Much progress has been made to identify new concepts in SNOMED CT, the largest clinical healthcare terminology. However, proper naming of new concepts remains challenging and relies on the ontology curators' manual effort. In this paper, we explore three deep learning-based approaches, given bags of words, to automatically predict concept names that comply with the naming convention of SNOMED CT. These deep learning models are simple neural network, Long Short-Term Memory (LSTM), and Convolutional Neural Network (CNN) combined with LSTM. Our experiments showed that LSTM-based approach achieved the best performance: a precision of 65.98\%, a recall of 61.04\%, and an F1 score of 63.41\% for predicting concept names for newly added concepts in the March 2018 Edition of SNOMED CT. It also achieved a precision of 74.58\%, a recall of 73.33\%, and an F1 score of 73.95\% for naming missing concepts identified by our previous work. Further examination of results revealed inconsistencies within SNOMED CT which may be leveraged for quality assurance purpose. 
\end{abstract}

\begin{IEEEkeywords}
Biomedical Ontologies, Naming Prediction, Neural Networks, Ontology Completeness
\end{IEEEkeywords}

\section{Introduction}
Ontologies provide formalized representation of concepts and relations among concepts for a specific domain. Biomedical ontologies or terminologies provide domain knowledge to support applications such as semantic annotation of biomedical data, knowledge discovery and exchange, data integration, natural language processing and decision support~\cite{bodenreider2008biomedical,hoehndorf2015role}. 

To keep up with the rapid development of knowledge in biomedicine, biomedical ontologies are often constantly evolving~\cite{cui2016biomedical} and new concepts are regularly added to newer versions of ontologies. %during the evolution process. 
For example, SNOMED CT, the most comprehensive clinical healthcare terminology product, is released regularly in every six months~\cite{SNOMEDCTRelease}.

Therefore, identifying missing or new concepts in biomedical ontologies (i.e., ontology enrichment) has been an active research area~\cite{he2015comparative,he2016topological,jiang2009auditing,zhu2017spark,cui2017mining,abeysinghe2017quality}.  
He et al. developed a topological-pattern-based method by leveraging mappings between the Unified Medical Language System (UMLS) and its source terminologies to identify potentially missing concepts in SNOMED CT~\cite{he2015comparative} and the National Cancer Institute Thesaurus (NCIt)~\cite{he2016topological}.
Jiang and Chute proposed a Formal Concept Analysis (FCA)-based method to construct the formal context of SNOMED CT and then searched for possible missing concepts~\cite{jiang2009auditing}. 
Zhu et al. improved Jiang and Chute's work and developed a scalable multistage algorithm called Spark-MCA~\cite{zhu2017spark} which enabled an exhaustive FCA evaluation on all the SNOMED CT concepts within a reasonable amount of time. 
Cui et al. introduced a structural-lexical method by mining lexical patterns in non-lattice subgraphs, where one of the patterns can identify missing concepts in SNOMED CT and NCIt~\cite{cui2017mining,abeysinghe2017quality}. 

Although much progress has been made to identify missing or new concepts, proper naming of those new concepts remains challenging and relies on the curators of biomedical ontologies. However, it is hard for different curators to maintain the same standard and keep it consistent while naming thousands of concepts. Also, according to the experiment results from Zhu et al's work~\cite{zhu2017spark}, even in well-constructed and mature ontologies such as SNOMED CT, there still exists a large amount of missing concepts. It is labor-intensive and time-consuming for curators to manually find appropriate and unambiguous names for a large number of concepts. Therefore, automated methods are highly desirable to provide suggestions on concept names to reduce curators' manual burden and accelerate the ontology maintenance process. 

Deep learning approaches have been widely used in text related tasks such as language modeling, textual analysis, information retrieval and sequence generation, and showed better performance than traditional machine learning methods in these tasks~\cite{deng2014deep}. 
In this paper, we explore deep learning-based approaches to provide suggestions on potential names for new concepts in SNOMED CT. 

Many existing works on ontology enrichment are capable of generating necessary words to construct a concept name. However, the words may be unordered, or the order of the words may not be consistent with the naming convention of the given ontology. For instance, one of the structural-lexical patterns in~\cite{cui2017mining} can suggest a bag of words for each potentially missing concept, such as \{{\em of, neoplasm, malignant, upper, lobe, right, of, lung}\} whose proper name should be ``{\em malignant neoplasm of right upper lobe of lung}". Thus, our focus in this paper is generating proper sequence order for a given bag of words. We conquer this task by dividing it into two subtasks. The first subtask is a binary classification problem to determine if a given sequence of words is a correct concept name or not, for which we explore three types of neural networks: a simple neural network, a Long Short-Term Memory (LSTM) neural network, and a combination of convolutional neural network (CNN) and LSTM. The second subtask is, given a bag of words, to predict correct sequence of words using the trained models in the first subtask. To the best of our knowledge, this is the first work that automatically predict names for new or missing concepts in biomedical ontologies.

The remainder of this paper is organized as follows: Section~\ref{method} specifies the problem and the deep learning models we use. Section~\ref{result} introduces the experiments we conducted and discusses the experiment result. Section~\ref{limitation} discusses the limitations of our approach and future work. Section~\ref{conclusion} concludes this paper.

\section{Method}~\label{method}
In this paper, we focus on addressing the problem of predicting the word sequence given a bag of words for naming a concept. To achieve this, we divide this task into the following two subtasks. 
Firstly, given a word sequence, we train the models to determine whether the given sequence or ordering is correct (meaningful and satisfying the naming convention of SNOMED CT) or not (binary classification). 
Secondly, given a bag of words, we utilize the trained models to predict its correct word sequence. Since the trained models return different confidence levels of the correctness judgment for different sequences, we test all the possible sequences of the given words and choose the one(s) with the highest confidence as the predicted concept name(s). Further, we implement a two-step filter to eliminate those potential incorrect candidate concept name(s).

\subsection{Word Embedding \& Data Preprocessing}
We first use Word2Vec~\cite{mikolov2013efficient} provided by Gensim~\cite{rehurek2010software} to learn vector representation of words in SNOMED CT concept names. Each word is mapped to a 125-length vector (word embedding) based on the word(s) surrounding it, and thus mapped to a higher dimensional space. The mappings between words and their word embeddings are stored in a matrix which later will be reused in training the neural network models. 

To train the models, we need both positive and negative training data. The original sequences of words (concept names) are labeled as ``1" or correct. Then, for each correct sequence, we generate $n$ incorrect sequences ($n=5$ in this paper) by randomly disordering the sequence and the generated sequences are labeled as ``0" or wrong. 
Since concept names in SNOMED CT are in different lengths (ranging from 1 to 39), all sequences are padded to the same length of 45.

\subsection{Neural Networks for Classifying Word Sequences}
We use Keras~\cite{chollet2015keras}, a high-level neural networks API, to implement three types of deep learning models as shown in Fig.~\ref{threeModels}. For all three models, there is an embedding layer right after the input layer. It is responsible for mapping each word in the input sequence to its word embedding. The pre-computed word embedding matrix will serve as the trained weight for this layer.

\begin{figure*}[h!]
\includegraphics[scale = 0.27]{models.png}
%\vspace{-3mm}
\caption{Three neural network models used for classification task. (a) is a simple neural network for binary classification; (b) is an LSTM neural network; (c) is the combination of convolutional neural network and LSTM. ``None" means that the dimension is variable.}
\label{threeModels}
\end{figure*}

The first model (Fig.~\ref{threeModels}(a)) is a simple neural network for binary classification. Since word embedding increases the input dimension, we need to flatten the input. Then we use a dense layer which has a {\em sigmoid} activation function to generate the prediction result. For the classification task, we use {\em binary\_crossentropy} in Keras, which is often used for binary classification problems, in all three models while training.

The concept names in SNOMED CT are in various lengths, and the position of a word may not only depend on the words next to it. This requires the model to be able to learn long-term context and dependencies between words in the input sequence. LSTM is proven to be good at learning such dependencies~\cite{greff2017lstm}. Thus we also adopt LSTM (Fig.~\ref{threeModels}(b)) for binary classification that contains a single LSTM layer with 100 LSTM memory unit in the middle. 

When it comes to determining the correctness of a sequence, there may exist featured patterns which can indicate whether a sequence order is correct or not. Thus, to capture those potential featured patterns, we employed a model in which convolutional neural network (CNN) is adopted and combined with LSTM (Fig.~\ref{threeModels}(c)). CNN is commonly applied to analyzing visual imagery and it can identify lower level features from the minimum unit of the input which eventually may improve the classification process~\cite{krizhevsky2012imagenet}. CNN also benefits sentence classification, such as sentiment analysis and question classification~\cite{kalchbrenner2014convolutional}. Regarding our work, the input is sequence of words (concept name) which can be considered as one dimension spatially. By using one-dimension CNN (Conv1D), certain word combinations or patterns will be selected as lower level feature and these learned spatial features will then be learned by an LSTM layer. In this model, the number of output filters in the Conv1D layer is 32 and the window size is set to three. The {\em pool\_size}~\cite{ciregan2012multi} for the {\em MaxPooling1D} layer is two.

As mentioned in the data preprocessing step, the number of input data labeled as ``1" is less than those labeled as ``0", with a proportion of $1:5$. To lessen the impact of unbalanced data, we assign different weights to different classes so that during training, the model will weight class ``1" more when adjusting the weight.

\subsection{Predicting Concept Names Given Bags of Words}
To suggest candidate concept name for a given bag of words, we first generate all its permutations (i.e., all possible sequences). Then we use the trained models to classify those generated sequences to check which one is valid. While performing classification, the neural network models could return a confidence score (probability) for a sequence to be valid. Thus, we select the sequence(s) with the highest confidence score to be valid as the potential concept name(s) for a given bag of words. Because the number of permutations of $n$ distinct objects is $n$ factorial. When $n$ becomes relatively large, the computation time increases dramatically. Thus, in this paper, we only provide prediction for concept names whose length is less than or equal to 9. 

Since there may exist multiple sequences with the highest confidence score for a given bag of words, we implement a two-step filtering process to further select the ``best" candidate(s). In the first step, we leverage the idea of Viterbi algorithm~\cite{forney1973viterbi} which returns the most likely sequence of hidden states, to removed those ``invalid" ones. It is based on the assumption that if a word $A$ has never been placed before (or after) another word $B$ in the training data set, then $A$ is not likely to appear before (or after) $B$ in the candidate concept names. 
In the second step, we leverage similar concept names to further reduce the candidate list. For a bag of words, we first find the most similar concept name (in terms of the bag of words) in the training data, where similarity is calculated by dividing the number of words that appear in both bags of words by the total number of distinct words in two bags. Then we utilize ``Levenshtein distance"~\cite{levenshtein1966binary} to compute the distances between each candidate name and the most similar concept name, and the one with the least distance will be selected as the ``best" candidate concept names.

\section{Experiment \& Result}~\label{result}
To validate the effectiveness of our models, we focused on two research questions:
\begin{enumerate}
\item Are the deep learning models able to determine if a sequence of words is a valid concept name in SNOMED CT? (binary classification)
\item Given a bag of words, can our method generate the correct sequence using those words and thus provide suggestions on how to name a concept in SNOMED CT? (sequence prediction)
\end{enumerate}

\subsection{Experiment Setup}
To explore the first question regarding binary classification, we performed two experiments. In the first experiment, we randomly separated the concept names in the March 2018 US Edition of SNOMED CT into two groups: training and test datasets. There are 1,753,513 labeled sequences in the training data set and 1,784,744 labeled sequences in test data set. In the second experiment, we trained our models by all the concept names in September 2017 US Edition of SNOMED CT (along with randomly disordered ones).
For testing, we extracted all the new concept names that were added into the March 2018 US Edition of SNOMED CT and generated disordered ones.
For each of these two experiments, after training our models with the training dataset, we evaluated our models using the test dataset. 

For the second question regarding sequence prediction, we evaluated our method in two ways. In the first way, we considered the sequence orders of the newly added concept names in the March 2018 US Edition of SNOMED CT as the ground truth for sequence predication. For each concept name, we regarded it as a bag of words and used our method to generate candidate concept names. Then we compared these suggestions with the ground truth. 
In the second way, we identified a collection of missing concepts (in the September 2017 US Edition of SNOMED CT) and the corresponding bags of words that are necessary to construct their names using Cui et al.'s method~\cite{cui2017mining}.  A total of 60 concepts in the form of bags of words were obtained for testing the performance of word sequence prediction with the help of a human annotator for validation.

\subsection{Result for Binary Classification}
In the first experiment for binary classification, 
we first tested different thresholds of confidence (for a sequence to be labeled as positive) for three models to achieve the best F1 score. The results are 0.5 for simple neural network, 0.8 for LSTM, and 0.7 for CNN and LSTM, respectively. The results of binary classification for three models using these thresholds are shown in Table~\ref{2018halfandhalf}, where it can seen that the LSTM model outperformed the other two models, and achieved the best performance: an accuracy of 94.72\%, a precision of 84.59\%, a recall of 83.51\%, an F1 score of 84.05\%, and an FP-rate of 3.04\%. The simple neural network performed the worst, and generated more false positives than the other two models. This is not surprising because while determining if a sequence is correct or not, the order of words matters and the simple neural network may not be able to learn long-term dependencies. In addition, it is shown that the combination of CNN and LSTM did not improve the performance of binary classification.

\begin{table}[h]
%\vspace{-2mm}
\renewcommand{\arraystretch}{1.25}
\centering
\caption{Result of binary classification for Experiment I.}
%\vspace{-2mm}
\label{2018halfandhalf}
\begin{tabular}{|l|r|r|r|}
\hline
          & Simple Neural Network & CNN and LSTM & LSTM    \\ \hline
Accuracy  & 73.58\%               & 92.15\%      & 94.72\% \\ \hline
Precision    & 35.42\%               & 74.23\%      & 84.59\% \\ \hline
Recall & 71.08\%               & 81.03\%      & 83.51\% \\ \hline
F1 Score  & 47.28\%               & 77.48\%      & 84.05\% \\ \hline
FP-rate   & 25.91\%               & 5.63\%      & 3.04\%  \\ \hline
\end{tabular}
\end{table}

The result of the second experiment for binary classification is shown in Table~\ref{2017Alland2018New}. It can be seen that the LSTM model still achieved the best performance. The main difference between the two experiments is that the first one's evaluation is within the same version of SNOMED CT, however, the second one is using concept names from a newer version of SNOMED CT to test the model which was trained by the older version. Although the training data in the second experiment is much more than the testing data, it exhibited a similar performance as the first experiment. 

\begin{table}[h]
%\vspace{-2mm}
\renewcommand{\arraystretch}{1.2}
\centering
\caption{Result of binary classification for Experiment II.}
%\vspace{-2mm}
\label{2017Alland2018New}
\begin{tabular}{|l|r|r|r|}
\hline
          & Simple Neural Network & CNN and LSTM & LSTM    \\ \hline
Accuracy  & 71.75\%               & 93.35\%      & 95.05\% \\ \hline
Precision    & 31.67\%               & 81.63\%      & 87.56\% \\ \hline
Recall & 60.05\%               & 77.56\%      & 81.97\% \\ \hline
F1 Score  & 41.47\%               & 79.54\%      & 84.67\% \\ \hline
FP-rate   & 25.90\%               & 3.49\%      & 2.33\%  \\ \hline
\end{tabular}
\end{table}

Since the LSTM model achieved the best performance in the binary classification subtask, we utilized it to perform sequence prediction for given bags of words in the next step. 

\subsection{Result for Sequence Prediction}

For the sequence prediction subtask, we first used the trained LSTM model to predict correct sequence orders for the newly added concept names in the March 2018 US Edition of SNOMED CT. Given the computational challenge for testing all possible sequences and the fact that less than 5\% of concept names in SNOMED CT are in the length of more than or equal to ten (see Fig.~\ref{lengthStatis} for the distribution of concept names in terms of their lengths), we performed the sequence prediction for concepts whose lengths are less than ten. 

\begin{figure}[h]
%\vspace{-2mm}
\includegraphics[scale = 0.35]{Picture1.png}
%\vspace{-6mm}
\caption{Number of concept names in terms of the name length for all concepts in the March 2018 US Edition of SNOMED CT.}
\label{lengthStatis}
%\vspace{-2mm}
\end{figure}

\begin{table*}[t]
%\vspace{-2mm}
\renewcommand{\arraystretch}{1.2}
\centering
\caption{Result of LSTM-based sequence prediction in terms of the length of concept names. Training data is from September 2017 US Edition of SNOMED CT and test data is the newly added concepts in the March 2018 Edition.}
%\vspace{-2mm}
\label{2017predict2018F1}
\begin{tabular}{|l|r|r|r|r|r|r|r|r|r|}
\hline
Length of Concept Name & 2       & 3       & 4       & 5       & 6       & 7       & 8       & 9       & All     \\ \hline
Number of True Positives          & 735     & 1274    & 909     & 918     & 565     & 446     & 316     & 184     & 5347    \\ \hline
Number of False Positives         & 154     & 310     & 556     & 465     & 375     & 278     & 287     & 332     & 2757    \\ \hline
Number of False Negatives         & 155     & 321     & 590     & 665     & 627     & 465     & 368     & 222     & 3413    \\ \hline
Precision              & 82.68\% & 80.43\% & 62.05\% & 66.38\% & 60.11\% & 61.60\% & 52.40\% & 35.66\% & 65.98\% \\ \hline
Recall                 & 82.58\% & 79.87\% & 60.64\% & 57.99\% & 47.40\% & 48.96\% & 46.20\% & 45.32\% & 61.04\% \\ \hline
F1 Score               & 82.63\% & 80.15\% & 61.34\% & 61.90\% & 53.00\% & 54.56\% & 49.10\% & 39.91\% & 63.41\% \\ \hline
\end{tabular}
%\vspace{-2mm}
\end{table*}

For a bag of words, our method will provide a set of candidate concept names. If a candidate name is in a correct order (i.e., a valid name), then it is considered as a true positive case; otherwise, it is considered as a false positive case. If the correct sequence is not included in the set of candidate concept names, we have one false negative case. 

Table~\ref{2017predict2018F1} shows the performance of our LSTM-based sequence prediction approach for predicting concept names with an overall F1 score of 63.41\%. It can be seen that concepts whose names are in the length of two and three received an F1 score of above 80\%, concepts in length of four and five received an F1 score of above 60\%, concepts in length of six and seven received an F1 score of above 50\%,  concepts in length of eight received an F1 score of 49.1\% , and concepts in length of nine received an F1 score of 39.91\%. This indicates that as the length of concept names grows, more sequences (false positive cases) might be included in the candidate set which leads to the decreasing performance. However, even for concept names with length of six, seven or eight, the performance are still acceptable. Overall, the F1 score of our model is 63.41\%.

We also performed another way to evaluate the performance of our LSTM-based sequence prediction approach, by utilizing Cui et al.'s method in~\cite{cui2017mining} to generate bags of words which are necessary to construct the names of new concepts. We obtained 60 bags of words and performed name prediction for them. % from small non-lattice subgraphs (sizes of 4, 5, and 6). Then, we used the trained model to perform prediction for these 60 bags of words. 
For each predicted name, a human annotator manually examined whether the generated sequence order is correct and conforming to the name convention of SNOMED CT. Among 60 cases, 44 out of them were considered as correct by the human annotator. Table~\ref{Cui} shows our LSTM-based approach achieved an F1 score of 73.95\%. The positive examples include ``{\em malignant neoplasm of blood vessel of thorax}", ``{\em structure of layer of tunica vaginalis}" and ``{\em open wound of limb without complication}". 
%The negative examples include ``{\em structure of fascial fascia compartment of limb}" and ``{\em congenital blind fistula}". 
This indicates that the LSTM-based method can be applied for naming a new concept based on the bags of words featuring a concept. 
\begin{table}[h]
%\vspace{-2mm}
\renewcommand{\arraystretch}{1.2}
\centering
\caption{Result of LSTM-based sequence prediction for names of missing concepts identified by Cui et al.'s method in~\cite{cui2017mining}.}
%\vspace{-2mm}
\label{Cui}
\begin{tabular}{|l|r|r|r|}
\hline
Number of Concepts Names  & 60                \\ \hline
Number of True Positives & 44 \\ \hline
Number of False Positives & 15 \\ \hline
Number of False Negatives & 16\\ \hline
Precision   & 74.58\%               \\ \hline
Recall & 73.33\%                \\ \hline
F1 Score  & 73.95\%             \\ \hline
\end{tabular}
%\vspace{-5mm}
\end{table}

\subsection{Potential Factors Affecting the Prediction Performance}
There are mainly two factors which potentially affect the performance of our prediction model -- the size of bags of words and the words in the bag. 

For the first factor, as the length of concept names increases, it becomes more difficult for the model to predict the correct sequence. This is because when the size of a bag of words increases, the number of sequences that need to be classified by the model increases dramatically. Since we assume that there is only one correct concept name for a bag of words, the false positive cases may greatly lower the model's precision. For instance, for a bag of words of size five without duplicate words, it has 120 permutations and only one of them is correct. If we only have two false positive cases, the accuracy is about 98\%, however, the actual precision is only 33\% and F1 measure is 49\%. Thus, length of concept name is an important factor affecting the performance of model. In our experiments, 50\% of the testing data are of length that is larger than or equal to five.

The second factor that may affect the model's performance involves the words in the bag. Our model may generate multiple candidate concept names for a given bag of words. 
This may be because that some words in the bag has the same role in other concept names. For example, for the concept name ``{\em ultrasound guided biopsy of left and right breast}", our model may be confused about the order of words ``{\em left}" and ``{\em right}", because they may appear separately in other concepts but with the same roles or patterns. Another typical case is related to duplicate words in a bag. If a bag contain two (or more) identical words (e.g. multiple ``{\em of}" or multiple ``{\em and}") such as ``{\em MRI \underline{of} joint \underline{of} right lower extremity}", it is even harder for the model to decide which word should be attached with the first one, which word should be attached with the second one, and the order of these two parts. Thus in this paper, we also compared the performance of the LSTM-based model applied to the bags of words containing duplicate words and those which do not contain duplicate words. The result is shown in Table~\ref{2017predict2018Duplication}. The model achieved a better F1 score when applied to the bags of words without duplicates.

\begin{table}[h]
%\vspace{-4mm}
\renewcommand{\arraystretch}{1.2}
\centering
\caption{Result of LSTM-based sequence prediction in terms of whether concept names contain duplicate words or not.}
%\vspace{-2mm}
\label{2017predict2018Duplication}
\begin{tabular}{|l|r|r|}
\hline
                    & Without Duplicates & With Duplicates \\ \hline
Number of True Positives       & 4789                & 558             \\ \hline
Number of False Positives      & 2224                & 533            \\ \hline
Number of False Negatives      & 3101                & 312              \\ \hline
Precision              & 68.29\%                & 51.15\%          \\ \hline
Recall           & 60.70\%             & 64.14\%          \\ \hline
F1 Score            & 64.27\%             & 56.91\%          \\ \hline
\end{tabular}
%\vspace{-3mm}
\end{table}

\subsection{Analysis of False Positives}
We manually examined some of the false positive cases in Experiment II of binary classification for potential patterns that our model is not able to deal with. Meanwhile, we compared our sequence predictions for those false positive cases with the ground truth to explore possible causes for mislabeling. Two observed patterns are listed as follows. 

The first pattern is related to ``{\em of}". In SNOMED CT, ``{\em B A}" and ``{\em A of B}" are both acceptable names for certain cases. One of them is often defined as the fully specified name (FSN), while the other one is listed as its synonym. For instance, ``{\em cyst of lung}" is an FSN, and ``{\em lung cyst}" is its synonym. However, in some cases, only one of them is included (e.g., concept ``{\em lung mass}" does not have a synonym ``{\em mass of lung}"). Therefore, if a concept name falls into this pattern, our model sometimes cannot predict it correctly. 

The second pattern involves two items (e.g., noun or noun phrase) that are connected by a preposition or conjunction, in which case our model sometimes cannot decide which one should be placed first. For example, for the concept ``{\em naproxen sodium and sumatriptan}", our predicted name was ``{\em sumatriptan and naproxen sodium}". Another example is that, for the concept ``{\em fluoroscopy of left and right hip}", our predicted name was ``{\em fluoroscopy of right and left hip}". In such cases, even the predicted names were valid, they were considered as false positives since they differ from the sequence of words provided in the ground truth. In other words, our evaluation was performed in a conservative way.

\subsection{Beyond Naming Purpose}
While analyzing false positive cases, we also noticed that this work could identify potential inconsistencies in the naming convention of concepts which can be considered as part of the quality assurance process for biomedical ontologies. For an existing name, we can use our trained model to check if it complies with the naming convention of SNOMED CT. For example, in the March 2018 US Edition of SNOMED CT, a new concept ``{\em Lesion of bone right upper arm}" is added. Our model labeled it as wrong. We found that it is a synonym of ``{\em Lesion of right upper arm bone}". However, when it comes to another similar concept ``{\em Lesion of left lower leg bone}", it does not have a synonym ``{\em Lesion of bone left lower leg}". Instead, it has ``{\em Lesion of bone in left lower leg}" whose pattern does not appear as a synonym of ``{\em Lesion of right upper arm bone}". 

Another example is ``{\em Liver of normal size}" that has been added to SNOMED CT in the March 2018 Edition. Our model labeled it as wrong. We found that it is an FSN, and ``{\em Normal sized liver}" is its synonym. However, in other similar concepts such as ``{\em Normal sized tonsils}" and ``{\em Normal sized ear canal}", they are considered as FSNs, but they do not have any synonym that has the pattern ``{\em xx of normal size}". This indicates a name inconsistency. A potential fix is that ``{\em Normal sized liver}" should be the FSN, and the name ``{\em Liver of normal size}" should become inactive. These two examples indicate that our model to some extent can reveal the inconsistency in SNOMED CT names. 

\section{Limitations \& Future Work}~\label{limitation}
Even though the experiment results showed that our method can provide suggestion in naming concept for biomedical ontologies, there are still some limitations. 
First, when it comes to training word embedding, even though we have 597,571 concept names which can be used as input for Word2Vec, the context may still not be profound enough (e.g. some words may only appear once or twice) to generate accurate word embedding.
Second, our sequence prediction method is based on classifying all possible combinations of words, which is computationally challenging when the length of names is large. 
Third, the structures of our neural network models are simple, a more complex and well-optimized model to address these limitations is expected. 

In future work, we plan to build a more powerful neural network model which utilizes both the word orders within the concept names and the logical definitions of concepts to predict potential concept names. Logical definitions are related to concepts' semantic meanings and thus could potentially be a determinant in how to name concepts. In this case, we no longer need to rely on the bags of words and test all the possible combinations.  In addition, as mentioned before, our work may also be used for quality assurance purpose -- discovering inconsistencies in concept names. However, in this work, we only manually discovered the inconsistencies. We plan to develop a more systematic way to uncover those inconsistencies.

\section{Conclusion}~\label{conclusion}
In this paper, we explored three deep learning-based approaches -- simple neural network, LSTM, and CNN combined with LSTM, to predict concept names for new concepts in biomedical ontologies. 
Our experiments showed that the LSTM-based approach achieved the best performance with an F1 score of 63.41\% for predicting names for newly added concepts in the March 2018 Edition of SNOMED CT and an F1 score of 73.95\% for naming missing concepts identified by Cui et al.'s method in~\cite{cui2017mining}.  This indicates that the LSTM-based approach is effective in predicting concept names given bags of words. Further analysis of the false positive cases revealed that this work may also be leveraged for identifying potential inconsistencies within the concept names of SNOMED CT. 

%\section*{Acknowledgment}
\bibliographystyle{IEEEtran}
\bibliography{IEEEexample}
\iffalse %sample in the original file
\begin{thebibliography}{00}
\bibitem{b1} G. Eason, B. Noble, and I. N. Sneddon, ``On certain integrals of Lipschitz-Hankel type involving products of Bessel functions,'' Phil. Trans. Roy. Soc. London, vol. A247, pp. 529--551, April 1955.
\end{thebibliography}
\fi

\end{document}
