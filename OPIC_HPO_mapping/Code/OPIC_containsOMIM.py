#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 24 14:08:58 2020

@author: fengbozheng
"""
############################################################
#Parse hp.owl file
############################################################
from owlready2 import *
onto = get_ontology("hp.owl").load()
allClasses = list(onto.classes())
allProperties = list(onto.properties())
#
synonymProp = []
for property1 in allProperties:
    if property1.label!=[]:
        if property1.label[0] in ["has_exact_synonym","has_related_synonym","has_broad_synonym","has_narrow_synonym"]:
            synonymProp.append(property1)
#the possible attribute name for "synonyms":         
#has_exact_synonym
#has_related_synonym
#has_broad_synonym            
#has_narrow_synonym
#
file1 = open("conceptIDAndNames_HPO.csv","w")
import csv
writer1 = csv.writer(file1)
for class1 in allClasses:
    conceptID = class1.name
    preferredName = class1.label
    for x in preferredName:
        writer1.writerow((conceptID,"has_label",x))
    for prop in class1.get_class_properties():
        if prop in synonymProp:
            synonym = prop[class1]
            for y in synonym:
                writer1.writerow((conceptID,prop.label[0],y))
file1.close()
#    
file2 = open("hierarchicalRelations(ChildtoParent)_HPO.txt","w")
file3 = open("hierarchicalRelations(ParenttoChild)_HPO.txt","w")
for class1 in allClasses:
    conceptID = class1.name
    x = class1.is_a
    for y in x:
        if type(y) == owlready2.entity.ThingClass:
            file2.write(conceptID+"\t"+y.name+"\n")
            file3.write(y.name+"\t"+conceptID+"\n")
file2.close()
file3.close()
###############################################################################
import networkx as nx
file2 = open("hierarchicalRelations(ParenttoChild)_HPO.txt","rb")
Dirgraph2 = nx.read_edgelist(file2, create_using = nx.DiGraph(), nodetype = str)
#
def findSubhierarchy(node):
    Dic = dict(nx.bfs_successors(Dirgraph2,node))
    d = Dic.values()
    c = []
    for e in d:
        c = c+e
    return c  
file2.close()
#Phenotypic abnormality
subhierarchy1 = set(findSubhierarchy("HP_0000118"))
#
import csv
import spacy
from spacy.tokenizer import Tokenizer #split by space #default
nlp = spacy.load("en_core_web_lg")
nlp.tokenizer = Tokenizer(nlp.vocab)
#
IDtoName = {}
NametoID = {}
file1 = open("conceptIDAndNames_HPO.csv","r")
reader1 = csv.reader(file1)
for row1 in reader1:
    if row1[0] in subhierarchy1:
        conceptNameDoc = nlp(row1[2].lower())
        conceptName = " ".join([token.lemma_ for token in conceptNameDoc])
        ID = row1[0].replace("HP_","HP:")
        if IDtoName.get(ID,"default") == "default":
            IDtoName[ID] = [(row1[1],conceptName,row1[2])]
        else:
            if (row1[1],conceptName,row1[2]) not in IDtoName.get(ID):
                IDtoName[ID].append((row1[1],conceptName,row1[2]))
        if NametoID.get(conceptName,"default") == "default":
            NametoID[conceptName] = [(row1[1],ID,row1[2])]
        else:
            if (row1[1],ID,row1[2]) not in NametoID.get(conceptName):
                NametoID[conceptName].append((row1[1],ID,row1[2]))
file1.close()        
#
def levenshtein(a,b):
    #Calculates the Levenshtein distance between a and b
    n, m = len(a), len(b)
    if n > m:
        # Make sure n <= m, to use O(min(n,m)) space
        a,b = b,a
        n,m = m,n
    #    
    current = range(n+1)
    #print(current)
    for i in range(1,m+1):
        previous, current = current, [i]+[0]*n
        #print(previous)
        #print(current)
        for j in range(1,n+1):
            add, delete = previous[j]+1, current[j-1]+1
            change = previous[j-1]
            if a[j-1] != b[i-1]:
                change = change + 1
            current[j] = min(add, delete, change)        
    return current[n]
############################################################
#Mappings between phenotype and its correpsonding genes
############################################################
phenotypeToGene = {}
file4 = open("ALL_SOURCES_ALL_FREQUENCIES_phenotype_to_genes.txt","r")
for lines4 in file4:
    line4 = lines4.split("\n")[0].split("\t")
    phenotype = line4[0]
    gene = line4[2]
    geneName = line4[3]
    if phenotypeToGene.get(phenotype,"Default") == "Default":
        #phenotypeToGene[phenotype] = [gene+": "+geneName]
        phenotypeToGene[phenotype] = [geneName]
    else:
        #phenotypeToGene[phenotype].append(gene+": "+geneName)
        phenotypeToGene[phenotype].append(geneName)
file4.close()
############################################################
#HPO API Usage
############################################################
#HPO API
'''
#testing
import requests
import json
#find Disease
diseaseName1 = "Tuberous Sclerosis"
#queryLink used to search for diseaseID given a diseaseName
queryLink = "http://hpo.jax.org/api/hpo/search/?q="+diseaseName1 
#diseaselink used to retrieve disease related information given a diseaseID
diseaseLink = "http://hpo.jax.org/api/hpo/disease/"              
response = requests.get(queryLink)
print(response.status_code)
originalBytes = response.content
resultDic = json.loads(originalBytes.decode("utf-8"))
diseaseResult = resultDic.get("diseases")
for result1 in diseaseResult:
    #print(result1.get("db"))
    #print(result1.get("dbName"))
    #print(result1.get("diseaseId"))
    diseaseID = result1.get("diseaseId")
    diseaseName = result1.get("dbName")
    diseaseDB = result1.get("db")
    response2 = requests.get(diseaseLink+diseaseID)
    diseaseInfo = json.loads(response2.content.decode("utf-8"))
    geneAsso = diseaseInfo.get("geneAssoc")
    #print(geneAsso)
    if diseaseName == diseaseName1:
        print(diseaseName)
        print(diseaseID)
'''
############################################################
#Given a concept name in OPIC, check if it has an exact matched concept in HPO
############################################################ 
import csv   
import requests
import json  
inputFile = open("opic_concepts.csv","r")
outputFile = open("opic_concepts_formatted.csv","w")
reader2 = csv.reader(inputFile)
originalHeader = next(reader2)
writer2 = csv.writer(outputFile)
diseaseLink = "http://hpo.jax.org/api/hpo/disease/"  
#i = 0
#j = 0
for row2 in reader2:
    case = "Need_Review"
    shortNameOri = row2[1].lower()
    shortNameDoc = nlp(shortNameOri)
    shortName = " ".join(tokens.lemma_ for tokens in shortNameDoc)
    #if name is included in HPO phenotype concepts   
    if NametoID.get(shortName,"default") != "default":
        #i = i+1
        case = "Exact_Match"
        conceptIDL = NametoID.get(shortName)
        appearance = ";\t".join([x[1]+"\t"+x[2] for x in conceptIDL])
        if len(row2)>=5:
            writer2.writerow((row2[0],row2[1],row2[2],row2[3],row2[4],case,appearance))
        else:
            writer2.writerow((row2[0],row2[1],row2[2],row2[3],"",case,appearance))
    else:
        queryLink = "http://hpo.jax.org/api/hpo/search/?q="+shortNameOri
        response = requests.get(queryLink)
        originalBytes = response.content
        resultDic = json.loads(originalBytes.decode("utf-8"))
        diseaseResult = resultDic.get("diseases")
        writingResult = []
        shortNameOriL = shortNameOri.split(" ")
        allCandidatesName = [x.get("dbName").lower() for x in diseaseResult]
        if shortNameOri in allCandidatesName:
            case = "Exact_Match"
            for result1 in diseaseResult:
                diseaseID = result1.get("diseaseId")
                diseaseName = result1.get("dbName")
                if shortNameOri == diseaseName.lower():
                    #response2 = requests.get(diseaseLink+diseaseID)
                    #diseaseInfo = json.loads(response2.content.decode("utf-8"))
                    #geneAsso = diseaseInfo.get("geneAssoc")
                    #x = [str(a.get("entrezGeneId"))+": "+a.get("entrezGeneSymbol") for a in geneAsso]
                    #writingResult.append((diseaseID+"\t"+diseaseName+":\t"+", ".join(x)))
                    writingResult.append(diseaseID+"\t"+diseaseName)
        else:
            case = "Need_Review"
            for result1 in diseaseResult:
                diseaseID = result1.get("diseaseId")
                diseaseName = result1.get("dbName")
                diseaseNameL = diseaseName.lower().split(" ")
                if levenshtein(shortNameOriL,diseaseNameL) <2 and (len(shortNameOriL)>2 or len(diseaseNameL)>2):
                    #response2 = requests.get(diseaseLink+diseaseID)
                    #diseaseInfo = json.loads(response2.content.decode("utf-8"))
                    #geneAsso = diseaseInfo.get("geneAssoc")
                    #x = [str(a.get("entrezGeneId"))+": "+a.get("entrezGeneSymbol") for a in geneAsso]
                    #writingResult.append((diseaseID+"\t"+diseaseName+":\t"+", ".join(x)))  
                    writingResult.append(diseaseID+"\t"+diseaseName)  
        if len(writingResult)>0:
            if len(row2)>=5:
                writer2.writerow((row2[0],row2[1],row2[2],row2[3],row2[4],case,";\t".join(writingResult)))
            else:
                writer2.writerow((row2[0],row2[1],row2[2],row2[3],"",case,";\t".join(writingResult)))
        else:
            case = "Need_Review"
            shortNameL = shortName.split(" ")
            #if len(shortNameL)>1:
            potentialIDList = []
            for key in NametoID.keys():
                q = key.split(" ")
                #if len(q)>1:#2 or len(shortNameL)>2:
                a = levenshtein(q,shortNameL)
                if a <2 and (len(shortNameL)>2 or len(q)>2):
                    potentialIDList.extend([y[1]+"\t"+y[2] for y in NametoID.get(key)])
            if len(potentialIDList)>0:
                appearance2 = ";\t".join(list(set(potentialIDList)))
            else:
                appearance2 = ""
            if len(row2)>=5:
                if appearance2 == "":
                    case = "No_Candidate"
                    writer2.writerow((row2[0],row2[1],row2[2],row2[3],row2[4],case,appearance2))
                else:
                    writer2.writerow((row2[0],row2[1],row2[2],row2[3],row2[4],case,appearance2)) 
            else:
                if appearance2 == "":
                    case = "No_Candidate"
                    writer2.writerow((row2[0],row2[1],row2[2],row2[3],"",case,appearance2))
                else:
                    writer2.writerow((row2[0],row2[1],row2[2],row2[3],"",case,appearance2)) 
inputFile.close()       
outputFile.close()    
#
###############################################################################
#Find HPO concept whose name contains short name of opic concepts
#And vice vers(if needed, NOT needed now)
###############################################################################
import networkx as nx
file2 = open("hierarchicalRelations(ParenttoChild)_HPO.txt","rb")
Dirgraph2 = nx.read_edgelist(file2, create_using = nx.DiGraph(), nodetype = str)
#
def findSubhierarchy(node):
    Dic = dict(nx.bfs_successors(Dirgraph2,node))
    d = Dic.values()
    c = []
    for e in d:
        c = c+e
    return c  
file2.close()
#Phenotypic abnormality
subhierarchy1 = set(findSubhierarchy("HP_0000118"))
#
import csv
import sys
csv.field_size_limit(sys.maxsize)
import spacy
#from spacy.tokenizer import Tokenizer #split by space #default
nlp = spacy.load("en_core_web_lg")
#nlp.tokenizer = Tokenizer(nlp.vocab)
IDtoName = {}
file1 = open("conceptIDAndNames_HPO.csv","r")
reader1 = csv.reader(file1)
for row1 in reader1:
    if row1[0] in subhierarchy1 and row1[1] == "has_label":
        conceptNameDoc = nlp(row1[2].lower())
        #conceptName = " ".join([token.lemma_ for token in conceptNameDoc])
        conceptName = [token.lemma_ for token in conceptNameDoc]
        ID = row1[0].replace("HP_","HP:")
        if IDtoName.get(ID,"default") == "default":
            IDtoName[ID] = [(row1[1],conceptName,row1[2])]
        else:
            if (row1[1],conceptName,row1[2]) not in IDtoName.get(ID):
                IDtoName[ID].append((row1[1],conceptName,row1[2]))
file1.close()             
#
file2 = open("OMIM.csv","r")
reader2 = csv.reader(file2)
for row2 in reader2:
    if row2[1] !="Preferred Label":
        ID2 = "OMIM:"+row2[0].split("/")[-1]
        conceptName2Doc = nlp(row2[1].lower())
        conceptName2 = [token.lemma_ for token in conceptName2Doc]
        if IDtoName.get(ID2,"default") == "default":
            IDtoName[ID2] = [("has_label",conceptName2,row2[1])]
        else:
            if ("has_label",conceptName2,row2[1]) not in IDtoName.get(ID2):
                IDtoName[ID2].append(("has_label",conceptName2,row2[1]))
file2.close()
#
file3 = open("ORDO.csv","r")
reader3 = csv.reader(file3)
for row3 in reader3:
    if row3[1]!= "Preferred Label":
        ID3 = row3[0].split("/")[-1].replace("_",":")
        conceptName3Doc = nlp(row3[1].lower())
        conceptName3 = [token.lemma_ for token in conceptName3Doc]
        if IDtoName.get(ID3,"default") == "default":
            IDtoName[ID3] = [("has_label",conceptName3,row3[1])]
        else:
            if ("has_label",conceptName3,row3[1]) not in IDtoName.get(ID3):
                IDtoName[ID3].append(("has_label",conceptName3,row3[1]))
file3.close()
######
import csv
file1 = open("opic_concepts_formatted.csv","r")
reader1 = csv.reader(file1)
opicConcept = {}
for row1 in reader1:
    #if row1[2] == "No_Candidate":
    if row1[5] != "Exact_Match":  
        if "(" in row1[1]:
            thingBeforeP = row1[1].lower().split("(")[0]
            thingAfterP = row1[1].lower().split(")")[1]
            if thingAfterP == "":
                if thingBeforeP.endswith(" "):
                    processed = thingBeforeP[:-1]+thingAfterP
                else:
                    processed = thingBeforeP+thingAfterP
                print([processed])
            else:
                if thingAfterP.startswith(" "):
                    processed = thingBeforeP+thingAfterP[1:]
                else:
                    processed = thingBeforeP+thingAfterP
                print([processed])
        #if len(testL)>2:
        #    print(row1)
        else:        
            processed = row1[1].lower()  
        oriNameDoc = nlp(processed)
        #opicShortName = " ".join([token.lemma_ for token in oriNameDoc])
        opicShortName = [token.lemma_ for token in oriNameDoc]
        if opicConcept.get(row1[0],"default") == "default":
            opicConcept[row1[0]] = [(opicShortName,row1[0],row1[1])]
        else:
            print("error")
            opicConcept[row1[0]].append((opicShortName,row1[0],row1[1]))
file1.close()
# 
def containList(A,B): # check if list A is contained entirely in list B
    n = len(A)
    return any(A == B[i:i+n] for i in range(len(B)-n+1))
###############################################################################
#1 Check if opic concept name is part of hpo concept name
###############################################################################
mapping = {}
file2 = open("opic_concepts_HPOContains.csv","w")
writer2 = csv.writer(file2)
for hpoConceptID in IDtoName.keys():
    queryResult = IDtoName.get(hpoConceptID)
    for result1 in queryResult:
        if result1[0] == "has_label":        
            IDPreferredName = result1[2]
    testingName = [x[1] for x in queryResult]
    #print(testingName)
    writingResult = []
    for hpoConceptName in testingName:
        for opicConceptID in opicConcept.keys():
            opicIDQuery = opicConcept.get(opicConceptID)
            if containList(opicIDQuery[0][0],hpoConceptName) == True:
                #writingResult.extend(opicIDQuery)
                if mapping.get(opicConceptID,"default") == "default":
                    mapping[opicConceptID] = [(hpoConceptID,IDPreferredName)]
                else:
                    mapping[opicConceptID].append((hpoConceptID,IDPreferredName))
    #newWriting = [y[1]+"\t"+y[2] for y in writingResult]
    #if len(newWriting)>0:
    #    writer2.writerow((hpoConceptID,IDPreferredName)+tuple(newWriting))
#file2.close()
       
file3 = open("opic_concepts_formatted.csv","r")
reader3 = csv.reader(file3)
for row3 in reader3:
    if row1[5] != "No_Candidate":
        writer2.writerow(tuple(row3))
    else:
        if row3[0] in mapping.keys():
            hpoConceptIDs = mapping.get(row3[0])
            writingTuples = [x[0]+"\t"+x[1] for x in mapping.get(row3[0])]
            writer2.writerow((row3[0],row3[1],"Part_of_Name")+tuple(writingTuples))
        else:
            writer2.writerow(tuple(row3))
file2.close()
file3.close()   
###############################################################################
#2 Check if opic concept name is part of HPO/OMIM/ORDO concept name
###############################################################################
mappingOPICtoHPO = {}
mappingHPOtoOPIC = {}
file2 = open("HPO_OMIM_ORDO_Contain_EPIC(NON_Overlap_Match).csv","w")
writer2 = csv.writer(file2)
for hpoConceptID in IDtoName.keys():
    queryResult = IDtoName.get(hpoConceptID)
    #print(queryResult)
    for result1 in queryResult:
        if result1[0] == "has_label":        
            IDPreferredName = result1[2]
    testingName = [x[1] for x in queryResult]
    writingResult = []
    for hpoConceptName in testingName:
        for opicConceptID in opicConcept.keys():
            opicIDQuery = opicConcept.get(opicConceptID)
            if containList(opicIDQuery[0][0],hpoConceptName) == True:
                #print(opicIDQuery)
                if mappingOPICtoHPO.get(opicConceptID,"default") == "default":
                    mappingOPICtoHPO[opicConceptID] = [(hpoConceptID,IDPreferredName)]
                else:
                    mappingOPICtoHPO[opicConceptID].append((hpoConceptID,IDPreferredName))
        
                if mappingHPOtoOPIC.get(hpoConceptID,"default") == "default":
                    mappingHPOtoOPIC[hpoConceptID] = [(opicConceptID,opicIDQuery[0][0])]
                else:
                    mappingHPOtoOPIC[hpoConceptID].append((opicConceptID,opicIDQuery[0][0]))
#
def returnIndex(tupleA, listB):
    for i in range(len(listB)):
        if list(tupleA) == listB[i:i+len(tupleA)]:
            return i
#    
matchingDic = []
for keys1 in mappingHPOtoOPIC.keys():
    for result2 in IDtoName.get(keys1):
        if result2[0] == "has_label":
            keys1List = result2[1]
            keys1PreferredName = result2[2]
    keys1Query = [(z[0],tuple(z[1])) for z in mappingHPOtoOPIC.get(keys1)]
    #mappingHPOtoOPIC.get(keys1): [(opicConceptID,shortNameList),(),...]
    #keys1Query covert to [(conpicConceptID, shortNameTuple),(),...]
    content = list(set(keys1Query))
    #content remove redundancy from keys1Query, still [(conpicConceptID, shortNameTuple),(),...]
    combMatching = [] # a list of lists of tuples of three [[(conpicConceptID, shortNameTuple,index),(),...],[],[],...]
    for item1 in content: #item1:(conpicConceptID, shortNameTuple)
        combMatching.append([item1+(returnIndex(item1[1],keys1List),)])
        for item2 in content:
            if len(set(item2[1]).intersection(set(item1[1]))) == 0:
                flag = 0
                for currentMatch in combMatching:
                    totalSet = tuple()
                    for x in currentMatch:
                        totalSet = totalSet+x[1]
                    if len(set(item2[1]).intersection(set(totalSet))) ==0:
                        currentMatch.append(item2+(returnIndex(item2[1],keys1List),))
                        flag = 1
                if flag == 0:
                    combMatching.append([item1+(returnIndex(item1[1],keys1List),),item2+(returnIndex(item2[1],keys1List),)])
    newCombMatching = list(set([tuple(sorted(x,key = lambda m:m[2])) for x in combMatching]))            
    matchingDic.append((keys1+":\t"+keys1PreferredName,newCombMatching,len(content)))
#a = sorted(matchingDic, key = lambda y:len(y[1]), reverse = True)
a = sorted(matchingDic, key = lambda y:y[2], reverse = True)   
for result in a:
    for matching in result[1]:
        writer2.writerow((result[0],)+tuple([x[0]+":\t"+opicConcept.get(x[0])[0][2] for x in matching]))
file2.close()