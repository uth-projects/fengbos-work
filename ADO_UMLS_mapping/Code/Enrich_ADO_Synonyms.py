###############################################
#Relation file in the UMLS
###############################################
import csv
output1 = open("conceptRelation2019AB.csv","w")
writer1 = csv.writer(output1)
file1 = open("MRREL.RRF","r")
for lines1 in file1:
    #print([lines1])
    #break
    line1 = lines1.split("\n")[0].split("|")
    if line1[14]!= "O":     
        CUI1 = line1[0]
        CUI2 = line1[4]
        REL = line1[3]
        SAB = line1[10]
        description = line1[7]
        AUI1 = line1[1]
        AUI2 = line1[5]
        writer1.writerow((CUI2,REL,CUI1,description,SAB,AUI2,AUI1))
file1.close()
output1.close()
#
#Could futher extract Hierarchical Relations between AUIs
#REL --> "CHD", "PAR"; description --> "isa", "inverse_isa"
#Establish directed graph
import networkx as nx
file2 = open("hierarchicalRelation(childParent)2019AB.txt","rb")
#file2: child+\t+parent+\n
Dirgraph = nx.read_edgelist(file2, create_using = nx.DiGraph(), nodetype = str)
Dirgraph1 = Dirgraph.reverse(copy=True)
file2.close()
AllAUIs = set(Dirgraph1.nodes())
#
def findDescendants(node):
    Dic = dict(nx.bfs_successors(Dirgraph1,node))
    d = Dic.values()
    c = []
    for e in d:
        c = c+e
    return c   
###############################################
#Concept file in the UMLS
###############################################
import csv
output2 = open("conceptName2019AB_WithTTY.csv","w")
writer2 = csv.writer(output2)
file2 = open("MRCONSO.RRF","r")
for lines2 in file2:
    #print([lines2])
    #break
    line2 = lines2.split("\n")[0].split("|")
    if line2[16] != "O" and line2[1] == "ENG": #not obsolete and from English
        AUI1 = line2[7]
        CUI1 = line2[0]
        String1 = line2[14] 
        SAB1 = line2[11]
        Code1 = line2[13]
        ISPREF = line2[6]
        writer2.writerow((String1,CUI1,SAB1,Code1,ISPREF,AUI1,line2[12]))        
        #line2[12] TTY: PN preferred Name in UMLS Browser, PT is perferred Term in source terminologies
file2.close()
output2.close()
###############################################
#Display name in the UMLS
###############################################
rank = {}
file2 = open("MRRANK.RRF","r")
for lines2 in file2:
    line2 = lines2.split("\n")[0].split("|")
    rank[(line2[1],line2[2])] = line2[0]  #Key:(source, tty); value: ranking
file2.close()
#
CUI_to_Names = {}
import csv
file1 = open("conceptName2019AB_WithTTY.csv","r")
reader1 = csv.reader(file1)
for row1 in reader1:
    if rank.get((row1[2],row1[6]),"DEFAULT") == "DEFAULT":
        print("error")
        break
    else:
        rank1 = rank.get((row1[2],row1[6]))
    if CUI_to_Names.get(row1[1],"DEFAULT") == "DEFAULT":
        CUI_to_Names[row1[1]] = set()
        CUI_to_Names[row1[1]].add((rank1,row1[0]))
    else:
        CUI_to_Names[row1[1]].add((rank1,row1[0]))
file1.close()
#
output = open("CUIandPreferredName.txt","w")
for key1 in CUI_to_Names.keys():
    names = list(CUI_to_Names.get(key1))
    preferred_Name = sorted(names, key=lambda x: x[0], reverse=True)[0][1]
    output.write(key1+"|"+preferred_Name +"\n")
output.close()
###############################################
#Semantic Type file in the UMLS
###############################################
output4 = open("conceptSemanticType2019AB.csv","w")
writer4 = csv.writer(output4)
file4 = open("MRSTY.RRF","r")
for lines4 in file4:
    line4 = lines4.split("\n")[0].split("|")
    CUI = line4[0]
    TUI = line4[1]
    STY = line4[3]
    writer4.writerow((CUI,TUI,STY))
file4.close()
output4.close()
#*********************************************#
###############################################
#Step1: Map ADO concept names to UMLS CUIs and AUIs
###############################################
name_to_CUI = {} # Key: concept name; Value: CUIs
CUI_to_AUI = {} # Key: CUI; Value: AUIs' information [(Name String, SAB, Code, TTY, AUI)]
CUI_to_DisplayName = {} #Key: CUI; Value: Display Name
ADO_Existing_Names = set()
for preferred_Name in ADO_Names:
    #First check if an ADO concept name appears in the UMLS
    if name_to_CUI.get(preferred_Name.lower(),"DEFAULT") != "DEFAULT":
        mapped_CUIs = name_to_CUI.get(preferred_Name.lower())
        if len(mapped_CUIs)>1:
            new_Mapped_CUIs = []
            j = 0
            for i in range(len(mapped_CUIs)):
                if preferred_Name.lower() == CUI_to_DisplayName.get(mapped_CUIs[i]).lower():
                    new_Mapped_CUIs.append(mapped_CUIs[i])
                    j = j+1
            if j ==0:
                #No exact matched CUI
                output(mapped_CUIs[:] related information)
            else:
                #Has exact matched CUI
                output(new_Mapped_CUIs related information)
        else:
            #One mapped CUI
            output(mapped_CUIs[0] related information)
#manual review result CUIs
###############################################
#Step2: Map ADO concept names to UMLS CUIs and AUIs
###############################################
def enrichSynonym(CUI1):
    mapped_AUIs = CUI_to_AUI.get(CUI1)
    for AUI in mapped_AUIs:
        enrich_Synonym = {} #Key: Name String; Value: corresponding AUIs' information
        if AUI[0].lower() not in ADO_Existing_Names and all(x not in AUI[0].lower() for x in [" nos","unspecified","not otherwise specified"]):
            if AUI[1] in ["OMIM","HGNC","GO","SNOMEDCT_US","NCI","MSH","HPO"]:
                if enrich_Synonym.get(AUI[0],"DEFAULT") == "DEFAULT":
                    enrich_Synonym[AUI[0]] = set()
                    enrich_Synonym[AUI[0]].add((AUI[4],AUI[1],AUI[2]))
                else:
                    enrich_Synonym[AUI[0]].add((AUI[4],AUI[1],AUI[2]))                
    return enrich_Synonym 
#manual review result AUIs
###############################################
#***In the future, find reviewed AUIs' descendants***
###############################################
def enrichDescendant(AUI1):
    descendants = findDescendants(AUI1)
    return(descendants related information)