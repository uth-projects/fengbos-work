#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 26 14:08:53 2018

@author: fengbozheng
"""
###############################################################################
#Step1 Pre-processing:
###############################################################################
#20170901 US Edition
file1 = open("sct2_Concept_Full_US1000124_20170901.txt","r")
activeConcept = {}
for lines1 in file1:
    line1 = lines1.split("\n")[0].split("\t")
    if line1[0] != "id":
        conceptID = line1[0]
        effectiveTime = line1[1]
        if effectiveTime <= "20170901":
            content = (effectiveTime,line1[2],line1[4])
            if activeConcept.get(conceptID,"default") == "default":
                activeConcept[conceptID] = content
            else:
                if effectiveTime > activeConcept.get(conceptID)[0]:
                    activeConcept[conceptID] = content
#Sufficiently Defined: 900000000000073002
#Primitive: 900000000000074008
output1 = open("activeConcept20170901.txt","w")
for key in activeConcept.keys():
    if activeConcept.get(key)[1] == "1":
        output1.write(key+"\t"+activeConcept.get(key)[2]+"\n")
output1.close()
file1.close()
#
activeFSN = {}
file2 = open("sct2_Description_Full-en_US1000124_20170901.txt","r")
for lines2 in file2:
    line2 = lines2.split("\n")[0].split("\t")    
    if line2[0] != "id":
        if line2[1] <= "20170901":
            content = (line2[1],line2[2],line2[4],line2[6],line2[7])
            #(effectiveTime, active, conceptID, typeID, term)
            if activeFSN.get(line2[0],"default") == "default":
                activeFSN[line2[0]] = content
            else:
                if line2[1] > activeFSN.get(line2[0])[0]:
                    activeFSN[line2[0]] = content
#FSN: 900000000000003001
#Synonym: 900000000000013009
output2 = open("activeFSN20170901.txt","w")
for key in activeFSN.keys():
    if activeFSN.get(key)[1] == "1":
        if (activeFSN.get(key)[3]=="900000000000003001"):
            output2.write(activeFSN.get(key)[2]+"\t"+activeFSN.get(key)[4]+"\n")
output2.close()
file2.close()
#evaluation-suppl.csv map to conceptID
activeConcept = {}
file2 = open("activeConcept20170901.txt","r")
for lines2 in file2:
    line2 = lines2.split()
    if line2[1] == "900000000000073002":
        activeConcept[line2[0]] = "defined"
    else:
        activeConcept[line2[0]] = "primitive"
file2.close()
activeID = set(activeConcept.keys())
#
FSNtoID = {}
file1 = open("activeFSN20170901.txt","r")
for lines1 in file1:
    line1 = lines1.split("\n")[0].split("\t")
    if line1[0] in activeID:
        if FSNtoID.get(line1[1],"default") == "default":
            FSNtoID[line1[1]] = line1[0]
        else:
            print("error")
file1.close()
#
import csv
file2 = open("evaluation-suppl.csv","r")
reader2 = csv.reader(file2)
file3 = open("evaluation-suppl_conceptID.csv","w")
writer3 = csv.writer(file3)
for row2 in reader2:
    if row2 != []:
        if row2[0] != "Subgraph No.":
            missingISAText = row2[1].split(" IS-A ")
            if FSNtoID.get(missingISAText[0],"default")!="default" and FSNtoID.get(missingISAText[1],"default")!="default":
                cID = FSNtoID.get(missingISAText[0])
                pID = FSNtoID.get(missingISAText[1])
                writer3.writerow((row2[0],missingISAText[0],missingISAText[1],cID,pID,activeConcept.get(cID),activeConcept.get(pID),row2[2]))
            else:
                print(row2)
file2.close()
file3.close()
#
###############################################################################
#20190301 US Edition
#20200301 US Edition
file1 = open("sct2_Concept_Full_US1000124_20200301.txt","r")
file2 = open("sct2_Description_Full-en_US1000124_20200301.txt","r")
activeConcept = {}
for lines1 in file1:
    line1 = lines1.split("\n")[0].split("\t")
    if line1[0] != "id":
        conceptID = line1[0]
        effectiveTime = line1[1]
        if effectiveTime <= "20200301":
            content = (effectiveTime,line1[2],line1[4])
            if activeConcept.get(conceptID,"default") == "default":
                activeConcept[conceptID] = content
            else:
                if effectiveTime > activeConcept.get(conceptID)[0]:
                    activeConcept[conceptID] = content  
output1 = open("activeConcept20200301.txt","w")
for key in activeConcept.keys():
    if activeConcept.get(key)[1] == "1":
        output1.write(key+"\t"+activeConcept.get(key)[2]+"\n")
output1.close()
file1.close()
activeFSN = {}
for lines2 in file2:
    line2 = lines2.split("\n")[0].split("\t")    
    if line2[0] != "id":
        if line2[1] <= "20200301":
            content = (line2[1],line2[2],line2[4],line2[6],line2[7])
            if activeFSN.get(line2[0],"default") == "default":
                activeFSN[line2[0]] = content
            else:
                if line2[1] > activeFSN.get(line2[0])[0]:
                    activeFSN[line2[0]] = content
output2 = open("activeFSN20200301.txt","w")
for key in activeFSN.keys():
    if activeFSN.get(key)[1] == "1":
        if (activeFSN.get(key)[3]=="900000000000003001"):
            output2.write(activeFSN.get(key)[2]+"\t"+activeFSN.get(key)[4]+"\n")
output2.close()
file2.close()
#SOME concepts have their corresponding FSNs (FSNs are active) but are inactive 
file3 = open("sct2_Relationship_Full_US1000124_20200301.txt","r")
activeRelation = {}
for lines3 in file3:
    line3 = lines3.split("\n")[0].split("\t")
    if line3[0] != "id":
        if line3[1] <= "20200301":
            content = (line3[1],line3[2],line3[4],line3[5],line3[6],line3[7]) 
            #effectiveTime, active, sourceID, destiniationID, Group, typeID
            if activeRelation.get(line3[0],"default") == "default":
                activeRelation[line3[0]] = content
            else:
                if line3[1] > activeRelation.get(line3[0])[0]:
                    activeRelation[line3[0]] = content
#
output3 = open("activeInferredRelation(GroupNum)20200301.txt","w")
for key in activeRelation.keys():
    if activeRelation.get(key)[1] == "1":
        output3.write(activeRelation.get(key)[2]+"\t"+activeRelation.get(key)[5]+"\t"+activeRelation.get(key)[3]+"\t"+activeRelation.get(key)[4]+"\n")
output3.close()
file3.close()
#
file4 = open("activeInferredRelation(GroupNum)20200301.txt","r")
output4 = open("hierarchicalRelation(childParent)20200301.txt","w")
output5 = open("hierarchicalRelation(parentChild)20200301.txt","w")
for lines4 in file4:
    line4 = lines4.split()
    if line4[1] == "116680003":
        output4.write(line4[0]+"\t"+line4[2]+"\n")
        output5.write(line4[2]+"\t"+line4[0]+"\n")
output4.close()
output5.close()
file4.close()
#
file1 = open("SNOMED_20200301_isaRels.txt","r")
file2 = open("SNOMED_20200301_attributeRels.txt","r")
file3 = open("activeStatedRelation(GroupNum)20200301.txt","w")
for lines1 in file1:
    line1 = lines1.split()
    file3.write(line1[0]+"\t"+"116680003"+"\t"+line1[1]+"\t"+"0"+"\n")
file1.close()
for lines2 in file2:
    line2 = lines2.split()
    file3.write(line2[0]+"\t"+line2[1]+"\t"+line2[2]+"\t"+line2[3]+"\n")
file2.close()
file3.close()
#
###############################################################################
#filter evaluation file
activeConcept = {}
file2 = open("activeConcept20200301.txt","r")
for lines2 in file2:
    line2 = lines2.split()
    if line2[1] == "900000000000073002":
        activeConcept[line2[0]] = "defined"
    else:
        activeConcept[line2[0]] = "primitive"
file2.close()
activeID = set(activeConcept.keys()) #validate how many nodes in the DAG
#
import networkx as nx
file2 = open("hierarchicalRelation(childParent)20200301.txt","rb")
Dirgraph1 = nx.read_edgelist(file2, create_using = nx.DiGraph(), nodetype = str)
#
def findAncestors(node):
    Dic = dict(nx.bfs_successors(Dirgraph1,node))
    d = Dic.values()
    c = []
    for e in d:
        c.extend(e)
    return c  
file2.close()
allNodesInSNOMED = list(Dirgraph1.nodes)
#
import csv
file3 = open("evaluation-suppl_conceptID.csv","r")
file4 = open("evaluation-suppl_Original.csv","w")
reader3 = csv.reader(file3)
writer4 = csv.writer(file4)
outdated = 0
pprimitive = 0
eval_no = 0
added = 0
added_inverse = 0
for row3 in reader3:
    if row3[7] == "yes":
        if row3[3] in activeID and row3[4] in activeID:
            if activeConcept.get(row3[4]) == "defined":
                if row3[3] not in findAncestors(row3[4]):
                    if row3[4] not in findAncestors(row3[3]):
                        writer4.writerow((row3[0],row3[1],row3[2],"ID: "+row3[3],"ID: "+row3[4],activeConcept.get(row3[3]),activeConcept.get(row3[4]),row3[7]))
                    else:
                        added = added+1
                else:
                    added_inverse = added_inverse+1
            else:
                pprimitive = pprimitive+1
        else:
            outdated = outdated+1
    else:
        eval_no = eval_no+1
file3.close()
file4.close()
###############################################################################
#Step2: Initialize each concept's attributes (stated and inferred ones)
###############################################################################
statedAttributes = {}    #key(sourceID): value [(groupNum, typeID, destinationID),....(groupNum, typeID, destinationID)]
inferredAttributes = {}  #key(sourceID): value (a list of all its attributes (attribute + groupNum)
#
file2 = open("activeStatedRelation(GroupNum)20200301.txt","r")
file3 = open("activeInferredRelation(GroupNum)20200301.txt","r")
#
for lines2 in file2:
    line2 = lines2.split()
    sourceID = line2[0]
    attributeWithGroup = line2[3]+" "+line2[1]+" "+line2[2]  # attributeWithGroup = GroupNum + typeID + destinationID
    if statedAttributes.get(sourceID,"default") == "default":
        statedAttributes[sourceID] = [attributeWithGroup]
    else:
        statedAttributes[sourceID].append(attributeWithGroup)
#
for lines3 in file3:
    line3 = lines3.split()
    sourceID = line3[0]
    attributeWithGroup = line3[3]+" "+line3[1]+" "+line3[2]  # attributeWithGroup = GroupNum + typeID + destinationID
    if inferredAttributes.get(sourceID,"default") == "default":
        inferredAttributes[sourceID] = [attributeWithGroup]
    else:
        inferredAttributes[sourceID].append(attributeWithGroup)   
file2.close()
file3.close()
#
###############################################################################
#Step3: Given a specific concept A, got its grouped stated/inferrred attributes
###############################################################################
def computeGroupedStatedAttributes(conceptA):
    groupedAttributesDic = {}
    allungroupedAttributes = statedAttributes.get(conceptA)
    for attributeWithGroupNum in allungroupedAttributes:
        attributesPlusGroupNum = attributeWithGroupNum.split()
        groupNum = attributesPlusGroupNum[0]
        attribute = (attributesPlusGroupNum[1], attributesPlusGroupNum[2])    #tuple: (typeID, destinationID)
        if groupedAttributesDic.get(groupNum,"default") =="default":
            groupedAttributesDic[groupNum] = [attribute]
        else:
            groupedAttributesDic[groupNum].append(attribute)
    #return groupedAttributesDic
    attributeList = []
    for keys in groupedAttributesDic.keys():
        if keys == "0":   #ungrouped attributes aer considered as separate groups
            for eachAttribute in groupedAttributesDic.get(keys):
                attributeList.append([eachAttribute])                
        else:
            attributeList.append(sorted(groupedAttributesDic.get(keys)))
    return attributeList
#      
def computeGroupedInferredAttributes(conceptA):
    groupedAttributesDic = {}
    allungroupedAttributes = inferredAttributes.get(conceptA)
    for attributeWithGroupNum in allungroupedAttributes:
        attributesPlusGroupNum = attributeWithGroupNum.split()
        groupNum = attributesPlusGroupNum[0]
        attribute = (attributesPlusGroupNum[1],attributesPlusGroupNum[2])
        if groupedAttributesDic.get(groupNum,"default") =="default":
            groupedAttributesDic[groupNum] = [attribute]
        else:
            groupedAttributesDic[groupNum].append(attribute)
    #return groupedAttributesDic
    attributeList = []
    for keys in groupedAttributesDic.keys():
        if keys == "0":
            for eachAttribute in groupedAttributesDic.get(keys):
                attributeList.append([eachAttribute])                
        else:
            attributeList.append(sorted(groupedAttributesDic.get(keys)))
    return attributeList
#
###############################################################################
#Step4: For a specific node(concept) in the hierarchy, find all its supertype ancestors
###############################################################################
import networkx as nx
file2 = open("hierarchicalRelation(childParent)20200301.txt","rb")
Dirgraph1 = nx.read_edgelist(file2, create_using = nx.DiGraph(), nodetype = str)
#
def findAncestors(node):
    Dic = dict(nx.bfs_successors(Dirgraph1,node))
    d = Dic.values()
    c = []
    for e in d:
        c.extend(e)
    return c  
file2.close()
#The one below is for "property chain"
file3 = open("hierarchicalRelation_WithIsModificationOf(childParent)20200301.txt","rb")
Dirgraph2 = nx.read_edgelist(file3, create_using = nx.DiGraph(), nodetype = str)
file3.close()
def findAncestors2(node):
    Dic = dict(nx.bfs_successors(Dirgraph2,node))
    d = Dic.values()
    c = []
    for e in d:
        c.extend(e)
    return c 
###############################################################################
#Step5: Given two attribute (attribute group) attributeA and attributeB, check if attributeB is more general than attributeA
###############################################################################
propertyChainList = set(["127489000","424361007","363701004","246093002","246075003","762949000"])
def checkSingleGeneral(attributeB, attributeA):
    if (attributeB[0] == attributeA[0]) or (attributeB[0] in findAncestors(attributeA[0])):
        if attributeA[0] in propertyChainList and attributeB[0] in propertyChainList:
            if (attributeB[1] in findAncestors2(attributeA[1])) or (attributeB[1] == attributeA[1]):
                return True
            else:
                return False
        elif (attributeB[1] in findAncestors(attributeA[1])) or (attributeB[1] == attributeA[1]):
            return True
        else:
            return False
    else:
        return False
#Given two attribute: attributeB (one single), attributeA (one group), check if attributeB is more general than attributeA
def checkSingleGroupGeneral(attributeB, attributeAList):
    if any(checkSingleGeneral(attributeB, singleAttribute) for singleAttribute in attributeAList):
        return True
    else:
        return False 
#
def checkGroupGroupGeneral(attributeBList, attributeAList):
    if all(checkSingleGroupGeneral(singleAttribute, attributeAList) for singleAttribute in attributeBList):
        return True
    else:
        return False    
#
###############################################################################
#Step 6: Diagnose missing IS-A
###############################################################################
#check if parent is more general than child
#for each group of attribute(s) of concept B, 
#we should find a corresponding group of attribute(s) of concept A which is more detailed than it (corresponding pair)
#testPair = ("109521001","28070007")
def explainStated(parentGroup,childGroup):
    for keyC in GroupNumChild.keys():
        if GroupNumChild.get(keyC) == childGroup:
            childCoGroupNum = keyC
    for keyP in GroupNumParent.keys():
        if GroupNumParent.get(keyP) == parentGroup:
            parentCoGroupNum = keyP
    doc.append(TextColor(color1,"* Corresponding group pair (c"+childCoGroupNum+", p"+parentCoGroupNum+"): c"+ childCoGroupNum +" is more specific than p"+ parentCoGroupNum+". Because:"))
    with doc.create(Enumerate(enumeration_symbol = "\\color{"+color1+"}\Roman*")) as itemize:
        #Explain For why corresponding group
        for explainSingleAttrP in parentGroup:
            for explainSingleAttrC in childGroup:
                if checkSingleGeneral(explainSingleAttrP,explainSingleAttrC):
                    if explainSingleAttrC == explainSingleAttrP:
                        for itemizedAttrC in GroupNumChildII.get(childCoGroupNum):
                            for itemizedAttrP in GroupNumParentII.get(parentCoGroupNum):
                                if (itemizedAttrC[1] ==explainSingleAttrC) and (itemizedAttrP[1] ==explainSingleAttrP): 
                                    itemize.add_item(TextColor(color1,itemizedAttrC[0]+" equals to "+itemizedAttrP[0]+". "))
                    else:
                        for itemizedAttrC in GroupNumChildII.get(childCoGroupNum):
                            for itemizedAttrP in GroupNumParentII.get(parentCoGroupNum):
                                if (itemizedAttrC[1] ==explainSingleAttrC) and (itemizedAttrP[1] ==explainSingleAttrP): 
                                    itemize.add_item(TextColor(color1, itemizedAttrC[0]+" is more specifc than "+itemizedAttrP[0]+". Because ")) 
                                    roleEqual = 0
                                    if explainSingleAttrP[0] in findAncestors(explainSingleAttrC[0]):
                                        itemize.append(TextColor(color1,italic(IDToFSN.get(explainSingleAttrC[0])+" ")))
                                        itemize.append(TextColor(color1," is a subtype of "))
                                        itemize.append(TextColor(color1,italic(IDToFSN.get(explainSingleAttrP[0]))))
                                        itemize.append(TextColor(color1,". "))
                                        roleEqual = 1
                                        #print(child,parent)
                                    if explainSingleAttrP[0] in propertyChainList:
                                        if explainSingleAttrP[1] in findAncestors2(explainSingleAttrC[1]):
                                            if roleEqual == 1:                   
                                                itemize.append(TextColor(color1,"And "))
                                                itemize.append(TextColor(color1,italic(IDToFSN.get(explainSingleAttrC[1])+" ")))
                                                itemize.append(TextColor(color1," is a subtype of "))
                                                itemize.append(TextColor(color1,italic(IDToFSN.get(explainSingleAttrP[1]))))
                                                itemize.append(TextColor(color1,". "))
                                            else:
                                                itemize.append(TextColor(color1,italic(IDToFSN.get(explainSingleAttrC[1])+" ")))
                                                itemize.append(TextColor(color1," is a subtype of "))
                                                itemize.append(TextColor(color1,italic(IDToFSN.get(explainSingleAttrP[1]))))
                                                itemize.append(TextColor(color1,". "))  
                                    else:
                                        if explainSingleAttrP[1] in findAncestors(explainSingleAttrC[1]):
                                            if roleEqual == 1:                   
                                                itemize.append(TextColor(color1,"And "))
                                                itemize.append(TextColor(color1,italic(IDToFSN.get(explainSingleAttrC[1])+" ")))
                                                itemize.append(TextColor(color1," is a subtype of "))
                                                itemize.append(TextColor(color1,italic(IDToFSN.get(explainSingleAttrP[1]))))
                                                itemize.append(TextColor(color1,". "))
                                            else:
                                                itemize.append(TextColor(color1,italic(IDToFSN.get(explainSingleAttrC[1])+" ")))
                                                itemize.append(TextColor(color1," is a subtype of "))
                                                itemize.append(TextColor(color1,italic(IDToFSN.get(explainSingleAttrP[1]))))
                                                itemize.append(TextColor(color1,". "))  
#
def explainInferred(parentGroup,childGroup):
    for keyP in GroupNumParent.keys():
        if GroupNumParent.get(keyP) == parentGroup:
            parentCoGroupNum = keyP
    childInferredCoGroupList = []
    for singleAttrInGroup in childGroup:
        singleAttrInGroupLabeled = IDToFSN.get(singleAttrInGroup[0]).replace(" (attribute)","")+": "+IDToFSN.get(singleAttrInGroup[1])                       
        childInferredCoGroupList.append(singleAttrInGroupLabeled) 
    childInferredCoGroup = "("+"; ".join(childInferredCoGroupList)+")"
    doc.append(TextColor(color1,"* Corresponding group pair: a group of relationships in child's inferred definition: "))
    doc.append(TextColor(color1,italic(childInferredCoGroup+" ")))
    doc.append(TextColor(color1,"is more specific than p"+parentCoGroupNum+". "))
    doc.append(TextColor(color1,"Because: "))
    with doc.create(Enumerate(enumeration_symbol = "\\color{"+color1+"}\Roman*")) as itemize1:
        for explainSingleAttrP in parentGroup:
            for explainSingleAttrC in childGroup:
                if checkSingleGeneral(explainSingleAttrP,explainSingleAttrC):
                    if explainSingleAttrC == explainSingleAttrP:
                        for itemizedAttrP in GroupNumParentII.get(parentCoGroupNum):
                            if itemizedAttrP[1] ==explainSingleAttrP:
                                itemize1.add_item(TextColor(color1,italic("("+IDToFSN.get(explainSingleAttrC[0]).replace(" (attribute)","")+": "+IDToFSN.get(explainSingleAttrC[1])+") ")))
                                itemize1.append(TextColor(color1," equals to "+itemizedAttrP[0]+"."))
                    else:
                        for itemizedAttrP in GroupNumParentII.get(parentCoGroupNum):
                            if itemizedAttrP[1] ==explainSingleAttrP:
                                #itemize1.add_item(TextColor(color1,"Thus, "))
                                itemize1.add_item(TextColor(color1,italic("("+IDToFSN.get(explainSingleAttrC[0]).replace(" (attribute)","")+": "+IDToFSN.get(explainSingleAttrC[1])+") ")))
                                itemize1.append(TextColor(color1," is more specific than "))
                                itemize1.append(TextColor(color1,itemizedAttrP[0]+". Because "))
                                roleEqual = 0
                                if explainSingleAttrP[0] in findAncestors(explainSingleAttrC[0]):
                                    itemize1.append(TextColor(color1,italic(IDToFSN.get(explainSingleAttrC[0])+" ")))
                                    itemize1.append(TextColor(color1," is a subtype of "))
                                    itemize1.append(TextColor(color1,italic(IDToFSN.get(explainSingleAttrP[0]))))
                                    itemize1.append(TextColor(color1,". "))
                                    roleEqual = 1   
                                    #print(child,parent)
                                if explainSingleAttrP[0] in propertyChainList:   
                                    if explainSingleAttrP[1] in findAncestors2(explainSingleAttrC[1]):
                                        if roleEqual == 1:
                                            itemize1.append(TextColor(color1,"And "))
                                            itemize1.append(TextColor(color1,italic(IDToFSN.get(explainSingleAttrC[1])+" ")))
                                            itemize1.append(TextColor(color1," is a subtype of "))
                                            itemize1.append(TextColor(color1,italic(IDToFSN.get(explainSingleAttrP[1]))))
                                            itemize1.append(TextColor(color1,". "))
                                        else:
                                            itemize1.append(TextColor(color1,italic(IDToFSN.get(explainSingleAttrC[1])+" ")))
                                            itemize1.append(TextColor(color1," is a subtype of "))
                                            itemize1.append(TextColor(color1,italic(IDToFSN.get(explainSingleAttrP[1]))))
                                            itemize1.append(TextColor(color1,". "))          
                                else:
                                    if explainSingleAttrP[1] in findAncestors(explainSingleAttrC[1]):
                                        if roleEqual == 1:
                                            itemize1.append(TextColor(color1,"And "))
                                            itemize1.append(TextColor(color1,italic(IDToFSN.get(explainSingleAttrC[1])+" ")))
                                            itemize1.append(TextColor(color1," is a subtype of "))
                                            itemize1.append(TextColor(color1,italic(IDToFSN.get(explainSingleAttrP[1]))))
                                            itemize1.append(TextColor(color1,". "))
                                        else:
                                            itemize1.append(TextColor(color1,italic(IDToFSN.get(explainSingleAttrC[1])+" ")))
                                            itemize1.append(TextColor(color1," is a subtype of "))
                                            itemize1.append(TextColor(color1,italic(IDToFSN.get(explainSingleAttrP[1]))))
                                            itemize1.append(TextColor(color1,". "))                             
#
def checkPotentialRelated(parentGroup,childGroup):
    FoundOne = 0
    for singleRelationP in parentGroup:
        for singleRelationC in childGroup:
            if singleRelationP[0] == singleRelationC[0] or singleRelationP[0] in findAncestors(singleRelationC[0]) or singleRelationC[0] in findAncestors(singleRelationP[0]):
                FoundOne = FoundOne +1
    if FoundOne > 0 :
        return True
    else:
        return False
#
def analyzeQualityIssue2(parentGroup,childGroup,solutionNum,parentCoGroupNum,childCoGroupNum):
    global priority1Solution
    priority1Solution = 0
    if checkGroupGroupGeneral(childGroup,parentGroup):
        doc.append("("+str(solutionNum)+")"+ "There is no group of relationships in child's stated definition that is more specific than p"+parentCoGroupNum+".")
        with doc.create(Itemize()) as itemize4:    
            itemize4.add_item("A potentially corresponding group pair is (c"+childCoGroupNum+", p"+parentCoGroupNum+").")
            itemize4.add_item("Problem in child's stated definition: p"+ parentCoGroupNum+" is more specific than c"+childCoGroupNum+".")
            itemize4.add_item("Fix Suggestion: Replace c"+ childCoGroupNum+" with p"+parentCoGroupNum+".")
        priority1Solution = priority1Solution + 1
    else:#####need rewrite
        pointer2 = 0
        candidateMissingValueSubsumption = []
        candidateMoreSpecific = []
        candidateMissingAttr = []
        correspondingP1 = 0
        for everyAttribute in parentGroup:    #对于每一个b’中的attribute，我们都需要找到具有相同attribute type的attribute才可以，否则去找下一个
            y=0
            for everyPrecedantAttr in childGroup:
                if checkSingleGeneral(everyAttribute,everyPrecedantAttr):
                    y = y+1
                else:
                    if checkSingleGeneral(everyPrecedantAttr,everyAttribute):
                        y = y+1
                        candidateMoreSpecific.append((everyAttribute,everyPrecedantAttr))#[0] is more specific than [1] (parentSingle,childSingle)
                    else:
                        if everyAttribute[0] == everyPrecedantAttr[0] or everyPrecedantAttr[0] in findAncestors(everyAttribute[0]) or everyAttribute[0] in findAncestors(everyPrecedantAttr[0]):
                            y = y+1
                            candidateMissingValueSubsumption.append((everyPrecedantAttr[1],everyAttribute[1],everyPrecedantAttr[0],everyAttribute[0]))
            if y ==0:
                candidateMissingAttr.append(everyAttribute)
        if len(candidateMissingValueSubsumption) == 0:
            priority1Solution = priority1Solution + 1
            doc.append("("+str(solutionNum)+")"+ "There is no group of relationships in child's stated definition that is more specific than p"+parentCoGroupNum+".")
            with doc.create(Itemize()) as itemize8: 
                itemize8.add_item("A potentially corresponding group pair is (c"+childCoGroupNum+", p"+parentCoGroupNum+").")
                list1 = []
                for eachMissingAttr in candidateMissingAttr:
                    for itemizedAttrP in GroupNumParentII.get(parentCoGroupNum):
                        if itemizedAttrP[1] == eachMissingAttr:
                            list1.append(itemizedAttrP[0])
                if len(list1)!=0:
                    itemize8.add_item("Problem in child's stated definition: c"+ childCoGroupNum+" is missing relationship(s): "+", ".join(list1)+".")
                    itemize8.add_item("Fix Suggestion: Insert "+", ".join(list1)+" into c" +childCoGroupNum+".")
                if len(candidateMoreSpecific)!=0:
                    itemize8.add_item("Problem in stated definition: One (or more) relationship in child is more general than parent's: ")
                    a= Itemize()
                    for eachMoreSpecific in candidateMoreSpecific:
                        for itemizedAttrC in GroupNumChildII.get(childCoGroupNum):
                            for itemizedAttrP in GroupNumParentII.get(parentCoGroupNum):
                                if (itemizedAttrC[1] ==eachMoreSpecific[1]) and (itemizedAttrP[1] ==eachMoreSpecific[0]): 
                                    a.add_item(itemizedAttrP[0]+" is more specific than "+itemizedAttrC[0]+".")
                                    a.add_item("Fix Suggstion: Replace "+itemizedAttrC[0]+" with "+itemizedAttrP[0]+".\n")
                    itemize8.append(a) 

def analyzeQualityIssue1(parentGroup,childGroup,solutionNum,parentCoGroupNum,childCoGroupNum):
    if checkGroupGroupGeneral(childGroup,parentGroup):
        doc.append("("+str(solutionNum)+")"+ "There is no group of relationships in child's stated definition that is more specific than p"+parentCoGroupNum+".")
        with doc.create(Itemize()) as itemize4:    
            itemize4.add_item("A potentially corresponding group pair is (c"+childCoGroupNum+", p"+parentCoGroupNum+").")
            itemize4.add_item("Problem in child's stated definition: p"+ parentCoGroupNum+" is more specific than c"+childCoGroupNum+".")
            itemize4.add_item("Fix Suggestion: Replace c"+ childCoGroupNum+" with p"+parentCoGroupNum+".")
    else:#####need rewrite
        pointer2 = 0
        candidateMissingValueSubsumption = []
        candidateMoreSpecific = []
        candidateMissingAttr = []
        correspondingP1 = 0
        for everyAttribute in parentGroup:    #对于每一个b’中的attribute，我们都需要找到具有相同attribute type的attribute才可以，否则去找下一个
            y=0
            for everyPrecedantAttr in childGroup:
                if checkSingleGeneral(everyAttribute,everyPrecedantAttr):
                    y = y+1
                else:
                    if checkSingleGeneral(everyPrecedantAttr,everyAttribute):
                        y = y+1
                        candidateMoreSpecific.append((everyAttribute,everyPrecedantAttr))#[0] is more specific than [1] (parentSingle,childSingle)
                    else:
                        if everyAttribute[0] == everyPrecedantAttr[0] or everyPrecedantAttr[0] in findAncestors(everyAttribute[0]) or everyAttribute[0] in findAncestors(everyPrecedantAttr[0]):
                            y = y+1
                            candidateMissingValueSubsumption.append((everyPrecedantAttr[1],everyAttribute[1],everyPrecedantAttr[0],everyAttribute[0]))
            if y ==0:
                candidateMissingAttr.append(everyAttribute)
        doc.append("("+str(solutionNum)+")"+ "There is no group of relationships in child's stated definition that is more specific than p"+parentCoGroupNum+".")
        with doc.create(Itemize()) as itemize8: 
            itemize8.add_item("A potentially corresponding group pair is (c"+childCoGroupNum+", p"+parentCoGroupNum+").")
            list1 = []
            for eachMissingAttr in candidateMissingAttr:
                for itemizedAttrP in GroupNumParentII.get(parentCoGroupNum):
                    if itemizedAttrP[1] == eachMissingAttr:
                        list1.append(itemizedAttrP[0])
            if len(list1)!=0:
                itemize8.add_item("Problem in child's stated definition: c"+ childCoGroupNum+" is missing relationship(s): "+", ".join(list1)+".")
                itemize8.add_item("Fix Suggestion: Insert "+", ".join(list1)+" into c" +childCoGroupNum+".")
            if len(candidateMoreSpecific)!=0:
                itemize8.add_item("Problem in stated definition: One (or more) relationship in child is more general than parent's: ")
                a= Itemize()
                for eachMoreSpecific in candidateMoreSpecific:
                    for itemizedAttrC in GroupNumChildII.get(childCoGroupNum):
                        for itemizedAttrP in GroupNumParentII.get(parentCoGroupNum):
                            if (itemizedAttrC[1] ==eachMoreSpecific[1]) and (itemizedAttrP[1] ==eachMoreSpecific[0]): 
                                a.add_item(itemizedAttrP[0]+" is more specific than "+itemizedAttrC[0]+".")
                                a.add_item("Fix Suggstion: Replace "+itemizedAttrC[0]+" with "+itemizedAttrP[0]+".\n")
                itemize8.append(a)
                #doc.append("Part of child's stated definition is more general than parent's. Need to replace"+ str(eachMoreSpecific[1])+" with a relationship that equals to or is more specific than "+ str(eachMoreSpecific[0])+"\n")
            for eachMissingValueSub in candidateMissingValueSubsumption:
                pointer2 = pointer2 +1
                if eachMissingValueSub[2] != "116680003":
                    itemize8.add_item("Problem in child's stated definition: There might be a missing subsumption relationship between destination concepts: ")
                    itemize8.append(italic(IDToFSN.get(eachMissingValueSub[0])+" "))
                    itemize8.append(" and ")
                    itemize8.append(italic(IDToFSN.get(eachMissingValueSub[1])+". \n"))
                    itemize8.append("OR\n")
                    itemize8.append("Problem in stated definition: child might be missing a group of attributes. \nFix Suggestion: Add p"+parentCoGroupNum+" to child's stated definition.")
                else:
                    itemize8.add_item("Problem in child's stated definition: Child might be missing an IS-A relationship. \nFix Suggestion: Add p"+parentCoGroupNum+" to child's stated definition.\n OR \n")
                    itemize8.append("Problem in stated definition: There might be a missing subsumption relationship between between destination concepts: ")
                    itemize8.append(italic(IDToFSN.get(eachMissingValueSub[0])+" "))
                    itemize8.append(" and ")
                    itemize8.append(italic(IDToFSN.get(eachMissingValueSub[1])+". "))    
#   
'''                                         
def checkChildParentGeneral(child,parent):
    childState = computeGroupedStatedAttributes(child)
    parentStateOriginal = computeGroupedStatedAttributes(parent)
    parentState = sorted(parentStateOriginal,key = lambda x: (len(x),x[0])) #sort by len, then by typeID
    childInferred = computeGroupedInferredAttributes(child)        
    SpIcGroup = []
    candidatePrecedant = list(childState)
    for Ym in parentState:
        i = 0
        for Xn1 in childState:
            if checkGroupGroupGeneral(Ym,Xn1):
                i = i+1
                if Xn1 in candidatePrecedant:
                    candidatePrecedant.remove(Xn1)
                explainStated(Ym,Xn1)
        if i == 0:
            for Xn2 in childInferred:
                if checkGroupGroupGeneral(Ym,Xn2):
                    i = i+1
                    explainInferred(Ym,Xn2)
                    break
        if i == 0:
            SpIcGroup.append(Ym)
    countSolutionNumber = 0
    for Ym in SpIcGroup:
        j = 0
        currentPrecedant = list(candidatePrecedant)
        for keyP in GroupNumParent.keys():
            if GroupNumParent.get(keyP) == Ym:
                parentCoGroupNum = keyP
        for Xn in currentPrecedant:
            if checkPotentialRelated(Ym,Xn):
                j = j+1
                if Xn in candidatePrecedant:
                    candidatePrecedant.remove(Xn)
                countSolutionNumber = countSolutionNumber + 1
                for keyC in GroupNumChild.keys():
                    if GroupNumChild.get(keyC) == Xn:
                        childCoGroupNum = keyC
                analyzeQualityIssue1(Ym,Xn,countSolutionNumber,parentCoGroupNum,childCoGroupNum)
        if j ==0:
            #Ym is potentially missing for child
            countSolutionNumber = countSolutionNumber + 1
            doc.append("("+str(countSolutionNumber)+")"+ "There is no group of relationships in child's stated definition that is more specific than p"+parentCoGroupNum+".")
            #doc.append("("+str(countSolutionNumber)+")"+" p"+parentCoGroupNum+" does not have a corresponding group in child that is more specific than it"+".")
            with doc.create(Itemize()) as itemize3:
                itemize3.add_item("There is no potentially corresponding group pair for p"+ parentCoGroupNum+".")
                itemize3.add_item("Problem in child's stated definition: Child is missing a group of relationships.")
                #itemize3.add_item("There is no potential corresponding group pair for p"+ parentCoGroupNum+".")
                itemize3.add_item("Fix Suggestion: Add p"+ parentCoGroupNum +" to child's stated definition.")    
    #print(SpIcGroup)
'''
#              
def checkChildParentGeneral2(child,parent):
    childState = computeGroupedStatedAttributes(child)
    parentStateOriginal = computeGroupedStatedAttributes(parent)
    parentState = sorted(parentStateOriginal,key = lambda x: (len(x),x[0])) #sort by len, then by typeID
    childInferred = computeGroupedInferredAttributes(child)        
    SpIcGroup = []
    candidatePrecedant = list(childState)
    for Ym in parentState:
        i = 0
        for Xn1 in childState:
            if checkGroupGroupGeneral(Ym,Xn1):
                i = i+1
                if Xn1 in candidatePrecedant:
                    candidatePrecedant.remove(Xn1)
                explainStated(Ym,Xn1)
        if i == 0:
            for Xn2 in childInferred:
                if checkGroupGroupGeneral(Ym,Xn2):
                    i = i+1
                    explainInferred(Ym,Xn2)
                    break
        if i == 0:
            SpIcGroup.append(Ym)
    countSolutionNumber = 0
    SpIcGroupCopy = list(SpIcGroup)
    for Ym in SpIcGroupCopy:
        j = 0
        currentPrecedant = list(candidatePrecedant)
        for keyP in GroupNumParent.keys():
            if GroupNumParent.get(keyP) == Ym:
                parentCoGroupNum = keyP
        flag = 0
        for Xn in currentPrecedant:
            if checkPotentialRelated(Ym,Xn):
                j = j+1
                countSolutionNumber = countSolutionNumber + 1
                for keyC in GroupNumChild.keys():
                    if GroupNumChild.get(keyC) == Xn:
                        childCoGroupNum = keyC
                analyzeQualityIssue2(Ym,Xn,countSolutionNumber,parentCoGroupNum,childCoGroupNum)
                if priority1Solution >0:
                    flag = 1
                    if Xn in candidatePrecedant:
                        candidatePrecedant.remove(Xn)
                else:
                    countSolutionNumber = countSolutionNumber-1 
        if flag == 1:
            SpIcGroup.remove(Ym)
    for Ym in SpIcGroup:
        k = 0
        currrentPrecedant = list(candidatePrecedant)
        for keyP in GroupNumParent.keys():
            if GroupNumParent.get(keyP) == Ym:
                parentCoGroupNum = keyP
        for Xn in currentPrecedant:
            if checkPotentialRelated(Ym,Xn):
                k = k+1
                if Xn in candidatePrecedant:
                    candidatePrecedant.remove(Xn)
                countSolutionNumber = countSolutionNumber + 1
                for keyC in GroupNumChild.keys():
                    if GroupNumChild.get(keyC) == Xn:
                        childCoGroupNum = keyC        
                analyzeQualityIssue1(Ym,Xn,countSolutionNumber,parentCoGroupNum,childCoGroupNum)
        if k ==0:
            #Ym is potentially missing for child
            countSolutionNumber = countSolutionNumber + 1
            doc.append("("+str(countSolutionNumber)+")"+ "There is no group of relationships in child's stated definition that is more specific than p"+parentCoGroupNum+".")
            #doc.append("("+str(countSolutionNumber)+")"+" p"+parentCoGroupNum+" does not have a corresponding group in child that is more specific than it"+".")
            with doc.create(Itemize()) as itemize3:
                itemize3.add_item("There is no potentially corresponding group pair for p"+ parentCoGroupNum+".")
                itemize3.add_item("Problem in child's stated definition: Child is missing a group of relationships.")
                #itemize3.add_item("There is no potential corresponding group pair for p"+ parentCoGroupNum+".")
                itemize3.add_item("Fix Suggestion: Add p"+ parentCoGroupNum +" to child's stated definition.")    
    #print(SpIcGroup)    
    
    

###############################################################################
#if __name__ == "__main__":
activeID = set()
file1 = open("activeConcept20200301.txt","r")
for lines in file1:
    line = lines.split()
    activeID.add(line[0]) 
file1.close()
#
activeFSN = {}
IDToFSN = {}
file1= open("activeFSN20200301.txt","r")
for lines in file1:
    line = lines.split("\n")[0]
    linee = line.split("\t")
    conceptID = linee[0]
    conceptFSN = linee[1]
    if conceptID in activeID:
        if activeFSN.get(conceptFSN,"default")=="default":
            activeFSN[conceptFSN] = conceptID
        else:#validation
            if conceptID != activeFSN.get(conceptFSN):
                print("error" + "multiple ID for one FSN")
                print(conceptFSN)
                print(activeFSN.get(conceptFSN))
                print(conceptID)
                continue
        if IDToFSN.get(conceptID,"default") == "default":
            IDToFSN[conceptID] = conceptFSN
        else:#validation
            if conceptFSN !=IDToFSN.get(conceptID):
                print("error"+"multiple FSN for one ID")
                print(conceptID)
                print(conceptFSN)
                print(IDToFSN.get(conceptID))
                continue        
file1.close()
#
###############################################################################
def LabledGenerate2ColumnTableForStated(childID,roles):
    if roles == "c":
        doc.append("Child's Stated Definition: \n")
    else:
        doc.append("Parent's Stated Definition: \n")
    cState = computeGroupedStatedAttributes(childID)#used to switch between inferred and stated definitions
    #doc.append("Inferred Relationships: \n")
    #cState = computeGroupedInferredAttributes(childID)
    sortedcState = sorted(cState,key = lambda x: (len(x),x[0])) #sort by len, then by typeID
    maxlen = len(cState)
    # Generate data table with
    table1 = Tabular("|l|l|")
    table1.add_hline()
    #table1.add_row("Group#",childID+" "+IDToFSN.get(childID))
    table1.add_row("Group#",IDToFSN.get(childID))
    table1.add_hline()
    ilen = 0
    while ilen <maxlen: 
        groupNum = ilen+1
        childGroup = sorted(sortedcState[ilen])
        GroupNumDic[str(groupNum)] = childGroup    #GroupNumDic is used for annotating group# in function checkChildParentGeneral(child, parent)
        numOfRow = len(childGroup)
        jlen = 0
        CPLabeledAttr = []
        while jlen < numOfRow:
            ItemAttrLabel = roles+str(groupNum)+"."+str(jlen+1)
            if jlen ==0:
                #table1.add_row(MultiRow(numOfRow,data = groupNum), childGroup[jlen][0]+" "+IDToFSN.get(childGroup[jlen][0])+": "+childGroup[jlen][1]+" "+IDToFSN.get(childGroup[jlen][1]))
                table1.add_row(MultiRow(numOfRow,data = roles+str(groupNum)), ItemAttrLabel+" "+IDToFSN.get(childGroup[jlen][0]).replace(" (attribute)","")+": "+IDToFSN.get(childGroup[jlen][1]))
                jlen = jlen+1
            else:
                table1.add_row("", ItemAttrLabel+" "+IDToFSN.get(childGroup[jlen][0]).replace(" (attribute)","")+": "+IDToFSN.get(childGroup[jlen][1]))
                jlen = jlen+1   
            CPLabeledItemAttr = (ItemAttrLabel,childGroup[jlen-1])        
            CPLabeledAttr.append(CPLabeledItemAttr)
        table1.add_hline()
        ilen = ilen+1  
        GroupNumDicII[str(groupNum)] = CPLabeledAttr        
    doc.append(table1)    
    doc.append(VerticalSpace("2mm",star=False))
    doc.append(LineBreak())
#GroupNumDic: key: group number (1,2,3,4...), value: a group of attributes [(type,desination),(),...]
#GroupNumDicII: key: group number (1,2,3,4...),value: a group of attributes and each attribute is assigned a sequence
#GroupNumDicII: value: [(c1.1, (type,destination)),(c1.2,(type,destination)),...]
import csv
import copy
file1 = open("evaluation-suppl_filtered.csv","r")
reader = csv.reader(file1)
from random import randint
from pylatex import Center
from pylatex.utils import bold
import os
from pylatex import NewLine, TextBlock, Itemize, Enumerate, VerticalSpace, HorizontalSpace, Document, PageStyle, Tabular, Head, Foot, MiniPage, MultiRow, StandAloneGraphic, MultiColumn, Tabu, LongTabu, LargeText, MediumText, LineBreak, NewPage, Tabularx, TextColor, simple_page_number
from pylatex.utils import italic, NoEscape
color1 = "gray"
#remember to add "xcolor package to enumerit"
geometry_options = {
    "landscape": False
  #  "margin": "1.5in",
  #  "headheight": "1pt",
  #  "headsep": "10pt",
  #  "includeheadfoot": False
}
doc = Document(indent = False, page_numbers=True, geometry_options=geometry_options)
iiii = 0
for row in reader:
    iiii = iiii+1
    #PageNum = row[0]
    SubgraphNum = row[0]
    childID = row[3]
    parentID = row[4]
    #doc.append("Sequence Number: "+PageNum +";      ")
    doc.append("Sequence Number: "+ str(iiii)+";      Subgraph Number from Previous Work: "+SubgraphNum+";\n")
    doc.append(childID+" "+IDToFSN.get(childID)+"\n" +" IS-A "+ "\n"+parentID+" "+IDToFSN.get(parentID)+"\n")
    #doc.append(VerticalSpace("10mm"))
    #doc.append("")
    doc.append(LineBreak())
    GroupNumDic = {}
    GroupNumDicII = {}
    LabledGenerate2ColumnTableForStated(childID,"c")
    GroupNumChild = copy.deepcopy(GroupNumDic)
    GroupNumChildII = copy.deepcopy(GroupNumDicII)
    GroupNumDic = {}
    GroupNumDicII = {}
    LabledGenerate2ColumnTableForStated(parentID,"p")
    GroupNumParent = copy.deepcopy(GroupNumDic)
    GroupNumParentII = copy.deepcopy(GroupNumDicII)
    #LabeledGenerate3ColumnTableForStated(childID, parentID)
    doc.append(LineBreak())
    checkChildParentGeneral2(childID,parentID)####################### not checked yet
    doc.append(NewPage())
doc.generate_tex("propertyChain2")
'''
#Suitable Header  
\documentclass[10pt]{article}%
%\usepackage[landscape=False]{geometry}%
\usepackage{multirow}%
\usepackage{comment} % enables the use of multi-line comments (\ifx \fi) 
\usepackage{lipsum} %This package just generates Lorem Ipsum filler text. 
\usepackage{fullpage} % changes the margin
%
%
%
\linespread{1.5}
\begin{document}%
\setlength\parindent{0pt}    
'''