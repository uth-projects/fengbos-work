#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 11 14:02:01 2018

@author: fengbozheng
"""
# Extract IS-A Relationships from RRF file
fileRelation = open("MRREL.RRF","rb+")
outputFile = open("hierarchicalRelationInUMLS.txt","w")
for lines in fileRelation:
    line = lines.split("\n")[0]
    lineList = line.split("|")
    firstConcept = lineList[0].lower()
    secondConcept = lineList[4].lower()
    relationshipType = lineList[3].lower()
    relationshipSource = lineList[10].lower()
    relationshipTypeSpecific = lineList[7].lower()
    if (relationshipType == "chd") or (relationshipType == "par"):#(relationshipType == "chd")or(relationshipType == "rn") or (relationshipType == "rb")or(relationshipType == "par"):
        outputFile.write(firstConcept+"|"+secondConcept+"|"+relationshipType+"|"+relationshipSource+"|"+relationshipTypeSpecific+"\n")
outputFile.close()
#Extract ConceptName and its corresponding CUI from RRF file    
fileConceptName = open("MRCONSO.RRF","rb+")
outputFile = open("conceptNameAndCUIInUMLS.txt","w")
for lines in fileConceptName:
    line = lines.split("\n")[0]
    lineList = line.split("|")
    CUI = lineList[0].lower()
    language = lineList[1].lower()
    conceptName = lineList[14].lower()
    Source = lineList[11].lower()
    if (language == "eng"):
        outputFile.write(CUI+"|"+conceptName+"\n")
outputFile.close()
# Create hierarchical relation dictionary to record hierarchical relation (key) and its stated source (value: List)
ISAWithSourceDic = {}
fileHierarchical = open("hierarchicalRelationInUMLS.txt","rb+")
for lines in fileHierarchical:
    line = lines.split("\n")[0]
    lineList = line.split("|")
    firstConcept = lineList[0]
    secondConcept = lineList[1]
    relationshipType = lineList[2] 
    relationshipTypeSpecific = lineList[4]
    relationshipSource = lineList[3]
    if (relationshipType == "par"): # (relationshipType == "rb")or
        if (relationshipTypeSpecific =="inverse_isa") or (relationshipTypeSpecific ==""):
            childParentPair = (firstConcept,secondConcept)
            if ISAWithSourceDic.get(childParentPair,"default") =="default":
                ISAWithSourceDic[childParentPair] = [relationshipSource]
            else:
                if relationshipSource not in  ISAWithSourceDic.get(childParentPair):
                    ISAWithSourceDic[childParentPair].append(relationshipSource)
    else:
        if (relationshipType == "chd"):#or(relationshipType == "rn"):
            if (relationshipTypeSpecific =="isa") or (relationshipTypeSpecific ==""):
                childParentPair = (secondConcept,firstConcept)
                if ISAWithSourceDic.get(childParentPair,"default") =="default":
                    ISAWithSourceDic[childParentPair] = [relationshipSource]
                else:
                    if relationshipSource not in ISAWithSourceDic.get(childParentPair):
                        ISAWithSourceDic[childParentPair].append(relationshipSource)           
fileHierarchical.close()
#Get all hierarchical relation in UMLS
outputfile = open("IS-ARelationWithoutSource.txt","w")
for key in ISAWithSourceDic.keys():
    outputfile.write(key[0]+" "+key[1]+"\n")
outputfile.close()
#Create dictionary to store concept name (key) and its CUI (value: List)
conceptNameAndCUI = {}
fileConceptName = open("conceptNameAndCUIInUMLS.txt","rb+")
for lines in fileConceptName:
    line = lines.split("\n")[0]
    lineList = line.split("|")
    CUI = lineList[0]
    conceptName = lineList[1]
    if conceptNameAndCUI.get(conceptName,"default") =="default":
        conceptNameAndCUI[conceptName] = [CUI]
    else:
        if CUI not in conceptNameAndCUI.get(conceptName):
            conceptNameAndCUI[conceptName].append(CUI)
import networkx as nx
with open('IS-ARelationWithoutSource.txt') as f: 
    lines= f.readlines()
    Dirgraph = nx.read_edgelist(lines, create_using = nx.DiGraph(), nodetype = str)
#
def findShortestPath(child,parent):
    b = nx.shortest_path(Dirgraph,child,parent)
    return b
file2 = open("IS-ARelationWithoutSource.txt","rb+")
allNodeInISA = set()
for lines in file2:
    line = lines.split()
    allNodeInISA.add(line[0])
    allNodeInISA.add(line[1])
#Using Name to map to CUI        
import csv
file1 = open("SNOMEDCT_linked_diff=2.csv","rb+")
file2 = open("SNOMEDCT_output2linked.csv","wb")
writer = csv.writer(file2)
hierarchicalReader = csv.reader(file1)
numOfTotal = 0
numOfNoPath = 0
numOfPathOnlyGO = 0
numOfGoodPath = 0
numOfNotInUMLS = 0
count = 0
for row in hierarchicalReader:
    #print row
    numOfTotal = numOfTotal +1
    child = row[0].lower()
    parent = row[1].lower()
    childCUI = []
    parentCUI = []
    if conceptNameAndCUI.get(child,"default")!="default":
        childCUI = conceptNameAndCUI.get(child)
    else:
        print "error"+child+" not in UMLS !"
        count = count +1
        #continue
    if conceptNameAndCUI.get(parent,"default")!="default":
        parentCUI = conceptNameAndCUI.get(parent)
    else:
        print "error"+parent+" not in UMLS !"
        #continue
        count = count +1
    if (len(childCUI) ==0) or (len(parentCUI) ==0):
        numOfNotInUMLS = numOfNotInUMLS +1
        writer.writerow((child,parent,"no path founded"))
    else:
        j=0
        for childConceptCUI in childCUI:
            for parentConceptCUI in parentCUI:
                if j <1:
                    if (childConceptCUI in allNodeInISA) and (parentConceptCUI in allNodeInISA): 
                        testPair = (childConceptCUI,parentConceptCUI)
                        if ISAWithSourceDic.get(testPair,"default") != "default":
                            j = j+1
                            if ISAWithSourceDic.get(testPair) ==["snomedct_us"]:
                                writer.writerow((child,parent,str(ISAWithSourceDic.get(testPair)),"********"))
                                numOfPathOnlyGO = numOfPathOnlyGO +1
                            else:
                                writer.writerow((child,parent,str(ISAWithSourceDic.get(testPair))))
                                numOfGoodPath = numOfGoodPath +1
                        else:
                            if nx.has_path(Dirgraph,childConceptCUI, parentConceptCUI)==True:
                                j = j+1
                                pathBetweenCP = findShortestPath(childConceptCUI, parentConceptCUI)
                                ISAalongThePath = []
                                for i in range(0,(len(pathBetweenCP)-1)):
                                    ISAalongThePath.append((pathBetweenCP[i],pathBetweenCP[i+1]))
                                Path = ""
                                i = 0
                                for ISAs in ISAalongThePath:
                                    Path = Path+"("+ISAs[0]+" "+ISAs[1]+" "+str(ISAWithSourceDic.get(ISAs))+")"
                                    if ISAWithSourceDic.get(ISAs)!=["snomedct_us"]:
                                        i = i+1
                                if i >0:
                                    numOfGoodPath = numOfGoodPath +1
                                    writer.writerow((child,parent,Path))                        
                                else:
                                    numOfPathOnlyGO = numOfPathOnlyGO+1 
                                    writer.writerow((child,parent,Path,"********"))    
        if j ==0:
            numOfNoPath = numOfNoPath +1
            writer.writerow((child,parent,"no path founded"))
file1.close()
file2.close()  
#Directly using CUI      
import csv
file1 = open("unlinked_diff=1.csv","rb+")
file2 = open("output1unlinked.csv","wb")
writer = csv.writer(file2)
hierarchicalReader = csv.reader(file1)
numOfTotal = 0
numOfNoPath = 0
numOfPathOnlyGO = 0
numOfGoodPath = 0
numOfNotInUMLS = 0
count = 0
for row in hierarchicalReader:
    #print row
    childConceptCUI = row[0].lower()
    parentConceptCUI = row[1].lower()
    numOfTotal = numOfTotal +1
    j=0
    if (childConceptCUI in allNodeInISA) and (parentConceptCUI in allNodeInISA): 
        testPair = (childConceptCUI,parentConceptCUI)
        if ISAWithSourceDic.get(testPair,"default") != "default":
            j = j+1
            if ISAWithSourceDic.get(testPair) ==["nci"]:
                writer.writerow((child,parent,str(ISAWithSourceDic.get(testPair)),"********"))
                numOfPathOnlyGO = numOfPathOnlyGO +1
            else:
                writer.writerow((child,parent,str(ISAWithSourceDic.get(testPair))))
                numOfGoodPath = numOfGoodPath +1
        else:
            if nx.has_path(Dirgraph,childConceptCUI, parentConceptCUI)==True:
                j = j+1
                pathBetweenCP = findShortestPath(childConceptCUI, parentConceptCUI)
                ISAalongThePath = []
                for i in range(0,(len(pathBetweenCP)-1)):
                    ISAalongThePath.append((pathBetweenCP[i],pathBetweenCP[i+1]))
                Path = ""
                i = 0
                for ISAs in ISAalongThePath:
                    Path = Path+"("+ISAs[0]+" "+ISAs[1]+" "+str(ISAWithSourceDic.get(ISAs))+")"
                    if ISAWithSourceDic.get(ISAs)!=["nci"]:
                        i = i+1
                if i >0:
                    numOfGoodPath = numOfGoodPath +1
                    writer.writerow((child,parent,Path))                        
                else:
                    numOfPathOnlyGO = numOfPathOnlyGO+1 
                    writer.writerow((child,parent,Path,"********"))    
    else:
        print "not in UMLS hierarchy"
        count = count +1
    if j ==0:
        numOfNoPath = numOfNoPath +1
        writer.writerow((child,parent,"no path founded"))
file1.close()
file2.close()    