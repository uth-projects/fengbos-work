#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 28 11:22:14 2020

@author: fengbozheng
"""
#Gene Ontology
#Given a missing IS-A, compute specificaiton level difference between subconcept and superconcept 
#Three sub-root: biological_process (0), cellular_compoenent (0), molecular_function(0)
#One root: Thing (0)
#Specification Level: len(path from a concept to Thing) - 1
import networkx as nx
file2 = open("hierarchy.txt","rb")
Dirgraph1 = nx.read_edgelist(file2, create_using = nx.DiGraph(), nodetype = str)
file2.close()
root = [n for n,d in Dirgraph1.out_degree() if d ==0][0]
#
import csv
filename = "r3_plus_modified_nls_3.csv"
file1 = open(filename,"r")
reader1 = csv.reader(file1)
output1 = open("LevelDiff_"+filename,"w")
writer1 = csv.writer(output1)
for row1 in reader1:
    child = row1[0].split(": ")[0]
    parent = row1[1].split(": ")[0]
    pathsC = max(nx.all_simple_paths(Dirgraph1,child,root),key=lambda x: len(x))
    pathsP = max(nx.all_simple_paths(Dirgraph1,parent,root),key=lambda y: len(y))
    LevelDiff = len(pathsC) - len(pathsP)
    writer1.writerow((row1[0],row1[1],len(pathsC)-1,len(pathsP)-1,LevelDiff))
file1.close()
output1.close()
#Showing Path
IDToName = {}
file3 = open("labels.txt","r")
for lines3 in file3:
    line3 = lines3.split("\n")[0].split("\t")
    IDToName[line3[0]] = line3[1]
file3.close()
explainConceptPath = "GO_0018960"
pathsConcept = max(nx.all_simple_paths(Dirgraph1,explainConceptPath,root),key=lambda x: len(x))
explainedPath = [IDToName.get(y) for y in pathsConcept]