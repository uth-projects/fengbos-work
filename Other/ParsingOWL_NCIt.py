from owlready2 import *
onto = get_ontology("ThesaurusInferred.owl").load()
allClasses = list(onto.classes())
allProperties = list(onto.properties())
isaRelationType = "ISA"
#excludeClass = []
#excludeClass.append(allClasses[7883].is_a[0])  # for 1905
#excludeClass.append(allClasses[10].is_a[0])    # for 1901
stop = 0
definitionStatus = ""
orFinder = []
#
def interpretDefSetL(defSetL,classIntro,i,j):
	global stop
	for partial in defSetL:
		if (type(partial) == owlready2.entity.ThingClass):   #is-a
			if (partial.name != "Thing"):
				definitionRelation.append((isaRelationType,partial.name,"0","0"))
		else: 
			if (type(partial) == owlready2.class_construct.Restriction): #attribute relation
				property = partial.property._python_name
				rangeClass = partial.value.name
				definitionRelation.append((property,rangeClass,"0","0"))
			else:
				if type(partial) == owlready2.class_construct.Or:
					orFinder.append(classIntro.name)
					j = j + 1
					roleGroupsL = partial.Classes
					for roleGroup in roleGroupsL:
						i = i+1
						#print(i)
						if type(roleGroup) == owlready2.class_construct.And:
							roleGroupSepL = roleGroup.Classes
							for roleGroupSep in roleGroupSepL:
								if type(roleGroupSep) == owlready2.class_construct.Restriction:
									roleGroupSepProperty = roleGroupSep.property._python_name
									rangeClassRoleGroupSep = roleGroupSep.value.name
									definitionRelation.append((roleGroupSepProperty,rangeClassRoleGroupSep,str(i),str(j)))
								else:
									print("error")
									stop = 1
						else:
							if type(roleGroup) == owlready2.class_construct.Restriction:
								roleGroupRProperty = roleGroup.property._python_name
								rangeClassRoleGroupR = roleGroup.value.name
								definitionRelation.append((roleGroupRProperty,rangeClassRoleGroupR,str(i),str(j)))
							else:
								stop =1
								print("OR inside type")
								print(type(roleGroup))
								print(classIntro.label[0])
				else:
					if type(partial) == owlready2.class_construct.And:
						miniPartL = partial.Classes
						revisedIJ = interpretDefSetL(miniPartL,classIntro,i,j)
						i = revisedIJ[0]
						j = revisedIJ[1]
						'''
						for miniPart in miniPartL:
							if type(miniPart) == owlready2.class_construct.Restriction:
								miniPartProperty = miniPart.property._python_name
								rangeClassMiniPart = miniPart.value.name
								definitionRelation.append((miniPartProperty,rangeClassMiniPart,"0","0"))
							else:
								if type(miniPart) == owlready2.entity.ThingClass:
									if (miniPart not in excludeClass):
										definitionRelation.append((isaRelationType,miniPart.name,"0","0"))	
								else:
									if type(miniPart) == owlready2.class_construct.And:
										miniMiniPartL = miniPart.Classes
										for miniMiniPart in miniMiniPartL:
											if type(miniMiniPart) == owlready2.class_construct.Restriction:
												miniMiniPartProperty = miniMiniPart.property._python_name
												rangeClassMiniMiniPart = miniMiniPart.value.name
												definitionRelation.append((miniMiniPartProperty,rangeClassMiniMiniPart,"0","0"))
											else:
												stop = 1
												print("miniMiniPart")
									else:
										stop = 1
										print("miniPart")
										print(type(miniPart))
										print(classIntro.label[0])
						'''
					else:
						print("outter most type")
						print(type(partial))
						print(classIntro.label[0])
						stop = 1
	return (i,j);
#
def introspectClass(classIntro):
	global definitionStatus
	global stop
	equivalentDefList = classIntro.equivalent_to
	if len(equivalentDefList) != 0:
		definitionStatus = "defined"
		equivalentDef = equivalentDefList[0]
		if type(equivalentDef) != owlready2.class_construct.And:
			stop =1
			print("definition is not logically And!")
		equivalentDefSetL = equivalentDef.Classes
		tupleT = interpretDefSetL(equivalentDefSetL,classIntro,0,0)
		inferredPartL = list(classIntro.is_a)
		redundant1 = interpretDefSetL(inferredPartL,classIntro,tupleT[0],tupleT[1])
	else:
		definitionStatus = "primitive"
		generalDefSetL = list(classIntro.is_a)
		redundant2 = interpretDefSetL(generalDefSetL,classIntro,0,0)
#
output1 = open("conceptInformation_2104.txt", "w")
output2 = open("conceptRelationInferred_2104.txt", "w")
q = 0
k = 0
output2.write("Source Concept\tRelation Type\tDestination Concept\tGroup Number\tParallel Group Index\n")
for eachClass in allClasses:
	k = k+1
	print(k)
	if stop !=1:
		if len(eachClass.label) > 0:
			definitionRelation = []
			introspectClass(eachClass)
			output1.write(eachClass.name+"\t"+definitionStatus+"\t"+eachClass.label[0]+"\n")
			if len(definitionRelation) >0:
				for eachRelation in definitionRelation:
					output2.write(eachClass.name+"\t"+eachRelation[0]+"\t"+eachRelation[1]+"\t"+eachRelation[2]+"\t"+eachRelation[3]+"\n")
		else:
			q = q+1
	else:
		break
output1.close()
output2.close()
#
output3 = open("relationType_2104.txt", "w")
for relationType in allProperties:
	if len(relationType.label)>0:
		writtenString = relationType._python_name+"\t"+relationType.label[0]+"\n"
		output3.write(writtenString)
output3.close()
#
output4 = open("association1901Inferred.txt","w")
for eachClass in allClasses:
    if len(eachClass.label)>0:
        for prop in eachClass.get_class_properties():
            if prop._python_name.startswith("A"):
                for value in prop[eachClass]:
                    output4.write(eachClass.name+"\t"+prop._python_name+"\t"+value.name+"\n")
output4.close()
#
output5 = open("conceptSynonym_2104.txt","w")
for eachClass in allClasses:
	synonymL = eachClass.P90
	output5.write(eachClass.name+"\t"+"\t".join(synonymL)+"\n")
output5.close()