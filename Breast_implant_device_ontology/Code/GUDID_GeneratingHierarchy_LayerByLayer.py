#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 26 18:48:09 2020

@author: fengbozheng
"""
###############################################################################
#View source file fields
###############################################################################
file1 = open("device.txt","r")
for lines1 in file1:
    line1 = lines1.split("|") 
    i = 0
    if line1[0] == "PrimaryDI":
        for field in line1:
            print(i,field)
            i = i+1
    else:
        if line1[0] == "00819409022649":
            for field in line1:
                print(i,field)
                i = i+1
            break
        #print((line1[0],line1[9],line1[13],line1[15]))
        #ID,
file1.close()
###############################################################################
#Device Selection (contains "Breast implant")
###############################################################################
FDAPremarket = {}
file3 = open("premarketSubmissions.txt","r")
for lines3 in file3:
    line3 = lines3.split("\n")[0].split("|")
    FDAPremarket[line3[0]] = line3[1]
file3.close()
#    
gmdnTerm = {}
file4 = open("gmdnTerms.txt","r")
for lines4 in file4:
    line4 = lines4.split("\n")[0].split("|")
    gmdnTerm[line4[0]] = line4[1]
file4.close()
#
file2 = open("TestDevices_BREAST IMPLANT_LBL.csv","w")
import csv
writer2 = csv.writer(file2)
file1 = open("device.txt","r")
for lines1 in file1:
    line1 = lines1.split("|")
    primaryDI = line1[0] #00812790020737
    brandName = line1[9] #SIENTRA Silicone Gel Breast Implant
    companyName = line1[13] #Sientra, Inc.
    deviceDescription = line1[15].lower() #Silicone Gel Breast Implant, Smooth Round, High Profile, 190 cc
    singleUse = line1[21] #True
    prescriptionUse = line1[30]#True
    deviceSterile = line1[32]#True
    #gmdnPT = gmdnTerm.get(line1[0],"")
    #OTC = line1[31] #
    #if companyName == "Sientra, Inc." and "breast implant" in deviceDescription.lower():
    if "breast implant" in deviceDescription:
        if deviceDescription[len(deviceDescription)-1] == " ":
            deviceDescription = deviceDescription[:len(deviceDescription)-1]
        if "cc" in deviceDescription:
            index =deviceDescription.index("cc")
            if deviceDescription[index-1] == " ":
                deviceDescription = deviceDescription[:index-1]+deviceDescription[index:] 
        #deviceDescription = deviceDescription.replace("breast implant,", "breast implant")
        deviceDescription = deviceDescription.replace("breast implant.", "breast implant,")
        deviceDescription = deviceDescription.replace("  "," ")
        writer2.writerow((primaryDI,brandName,companyName,"singleUse: "+singleUse,"prescriptionUse: "+prescriptionUse,"packageSterile: "+deviceSterile,deviceDescription))
file1.close()
file2.close()
###############################################################################
#FCA: Level1
###############################################################################
#use levenshtein distance (<=2) to limit intersection is only performed between 
#related/similar concepts for the first layer
def subList(a,b): #check if a in b
    j = 0
    for i in range(len(b)):
        if b[i:i+len(a)] == a:
            j = j+1
    if j ==0:
        return False
    else:
        return True
# 
def levenshtein(a,b):
   # "Calculates the Levenshtein distance between a and b."
    n, m = len(a), len(b)
    if n > m:
        # Make sure n <= m, to use O(min(n,m)) space
        a,b = b,a
        n,m = m,n        
    current = range(n+1)
    for i in range(1,m+1):
        previous, current = current, [i]+[0]*n
        for j in range(1,n+1):
            add, delete = previous[j]+1, current[j-1]+1
            change = previous[j-1]
            if a[j-1] != b[i-1]:
                change = change + 1
            current[j] = min(add, delete, change)
    return current[n]
###############################################################################
import csv
FCA = {}
#1083
file1 = open("TestDevices_BREAST IMPLANT_LBL.csv","r")
reader1 = csv.reader(file1)
for row1 in reader1:
    FCA[row1[0]] = row1[6]
file1.close()
#942
oldset = set() 
for keys1 in FCA.keys():
    itsFCA = FCA.get(keys1)
    oldset.add(itsFCA)
#
import string
allowed = string.ascii_letters + string.digits
#
def lcs(S,T):
    m = len(S)
    n = len(T)
    counter = [[0]*(n+1) for x in range(m+1)]
    longest = 0
    lcs_set = set()
    for i in range(m):
        for j in range(n):
            if S[i] == T[j]:
                c = counter[i][j] + 1
                counter[i+1][j+1] = c
                if c > longest:
                    lcs_set = set()
                    longest = c
                    lcs_set.add(" ".join(S[i-c+1:i+1]))
                    #print(lcs_set)
                elif c == longest:
                    lcs_set.add(" ".join(S[i-c+1:i+1]))
                    #print(lcs_set)
    if len(lcs_set)>0:
        #print(lcs_set)
        lcs_setL = list(lcs_set)
        newLcs_setL = [x for x in lcs_setL if "breast implant" in x and "cc" not in x]
        if len(newLcs_setL) >0:
            return newLcs_setL
        else:
            return False
    else:
        return False
#Example1
#a = "silicone gel breast implant, smooth round, low profile, 370 cc".split(" ")
#b = "silicone gel breast implant, textured round, low profile, 370 cc".split(" ")
Originalset = oldset.copy()
Initialset = oldset.copy()
NewlyAdded = oldset.copy()
mm = 0
while(len(NewlyAdded)!=0):
    print(mm)
    mm = mm +1
    LastIteration = Initialset.copy()
    LastIterationL = sorted(LastIteration)
    for p in range(0,len(LastIterationL)-1):
        pL = LastIterationL[p].split(" ")
        for q in range(p+1,len(LastIterationL)):
            qL = LastIterationL[q].split(" ")
            if levenshtein(pL,qL) <= 2: #and len(set(pL).intersection(set(qL)))>=2:
                inter = lcs(pL,qL)
                if inter != False:
                    #if "silicone gel breast implant," in inter:
                    #    print(LastIterationL[p])
                    #    print(LastIterationL[q])
                    #    break
                    for result in inter:
                        if result[len(result)-1] not in allowed:
                            Initialset.add(result[:len(result)-1])
                        else:
                            Initialset.add(result)
    NewlyAdded = Initialset - LastIteration
    print(len(NewlyAdded))
    break
Result = Initialset - Originalset 
print(len(Result))
file5 = open("newConcept_LBL_1stLayer.csv","w")
writer5 = csv.writer(file5)
for result1 in Result:
    writer5.writerow((result1,))
file5.close()
###############################################################################
#Level2&Upper
###############################################################################
import csv
FCA = {}
#
counter = 10000
file1 = open("newConcept_LBL_1stLayer.csv","r")
reader1 = csv.reader(file1)
for row1 in reader1:
    counter = counter+1
    createdID = "NEW"+str(counter)
    FCA[createdID] = row1[0]
file1.close()
#
oldset = set() 
for keys1 in FCA.keys():
    itsFCA = FCA.get(keys1)
    oldset.add(itsFCA)
#
def lcs(S,T):
    m = len(S)
    n = len(T)
    counter = [[0]*(n+1) for x in range(m+1)]
    longest = 0
    lcs_set = set()
    for i in range(m):
        for j in range(n):
            if S[i] == T[j]:
                c = counter[i][j] + 1
                counter[i+1][j+1] = c
                if c > longest:
                    lcs_set = set()
                    longest = c
                    lcs_set.add(" ".join(S[i-c+1:i+1]))
                    #print(lcs_set)
                elif c == longest:
                    lcs_set.add(" ".join(S[i-c+1:i+1]))
                    #print(lcs_set)
    if len(lcs_set)>0:
        #print(lcs_set)
        lcs_setL = list(lcs_set)
        newLcs_setL = [x for x in lcs_setL if "breast implant" in x and "cc" not in x]
        if len(newLcs_setL) >0:
            return newLcs_setL
        else:
            return False
    else:
        return False
#Example1
#a = "silicone gel breast implant, smooth round, low profile, 370 cc".split(" ")
#b = "silicone gel breast implant, textured round, low profile, 370 cc".split(" ")
Originalset = oldset.copy()
Initialset = oldset.copy()
NewlyAdded = oldset.copy()
mm = 0
while(len(NewlyAdded)!=0):
    print(mm)
    mm = mm +1
    LastIteration = Initialset.copy()
    LastIterationL = sorted(LastIteration)
    for p in range(0,len(LastIterationL)-1):
        pL = LastIterationL[p].split(" ")
        for q in range(p+1,len(LastIterationL)):
            qL = LastIterationL[q].split(" ")
            #if levenshtein(pL,qL) <2 and len(set(pL).intersection(set(qL)))>=2:
            inter = lcs(pL,qL)
            if inter != False:
                #if "silicone gel breast implant," in inter:
                #    print(LastIterationL[p])
                #    print(LastIterationL[q])
                #    break
                for result in inter:
                    if result[len(result)-1] not in allowed:
                        Initialset.add(result[:len(result)-1])
                    else:
                        Initialset.add(result)
    NewlyAdded = Initialset - LastIteration
    print(len(NewlyAdded))
Result = Initialset - Originalset 
print(len(Result))
file5 = open("newConcept_LBL_2nd&UpperLayer.csv","w")
writer5 = csv.writer(file5)
for result1 in Result:
    writer5.writerow((result1,))
file5.close()
###############################################################################
import csv
newConceptDic1 = {}
counter1 = 10000
file1 = open("newConcept_LBL_1stLayer.csv","r")
reader1 = csv.reader(file1)
for row1 in reader1:
    counter1 = counter1+1
    createdID = "NEW"+str(counter1)
    newConceptDic1[createdID] = row1[0]
file1.close()
#
newConceptDic2 = {}
counter2 = 20000
file2 = open("newConcept_LBL_2nd&UpperLayer.csv","r")
reader2 = csv.reader(file2)
for row2 in reader2:
    counter2 = counter2+1
    createdID = "NEW"+str(counter2)
    newConceptDic2[createdID] = row2[0]
file2.close()
###############################################################################
import csv
originalConceptDic = {}
duplicateDescription = {}
file1 = open("TestDevices_BREAST IMPLANT_LBL.csv","r")
reader1 = csv.reader(file1)
for row1 in reader1:
    originalConceptDic[row1[0]] = row1[6]
    if duplicateDescription.get(row1[6],"default") == "default":
        duplicateDescription[row1[6]] = [row1[0]]
    else:
        duplicateDescription[row1[6]].append(row1[0])
file1.close()
#
counter3 = 30000
duplicateDescriptionkeyL = sorted(duplicateDescription.keys())
for keyDup in duplicateDescriptionkeyL:
    if len(duplicateDescription.get(keyDup)) >1:
        counter3 = counter3+1
        createdID = "NEW"+str(counter3)
        newConceptDic2[createdID] = keyDup
#
ISA= set()
newConcepts1 = list(newConceptDic1.keys())
newConcepts2 = list(newConceptDic2.keys())
allNewConcepts = newConcepts1+newConcepts2
originalConcepts = list(originalConceptDic.keys())
for Concept3 in allNewConcepts:
    if Concept3 in newConcepts1:
        FCA3 = newConceptDic1.get(Concept3)
    else:
        FCA3 = newConceptDic2.get(Concept3)
    for Concept4 in allNewConcepts:
        if Concept3 != Concept4:
            if Concept4 in newConcepts1:
                FCA4 = newConceptDic1.get(Concept4)
            else:
                FCA4 = newConceptDic2.get(Concept4)
            if FCA4 in FCA3:
                ISA.add((Concept3, Concept4))
for Concept1 in originalConcepts:
    FCA1 = originalConceptDic.get(Concept1)
    for Concept2 in allNewConcepts:
        if Concept2 in newConcepts1:
            FCA2 = newConceptDic1.get(Concept2)
        else:
            FCA2 = newConceptDic2.get(Concept2)
        if FCA2 in FCA1:
            ISA.add((Concept1,Concept2))
#
import networkx as nx
G = nx.DiGraph()
for isa1 in ISA:
    G.add_edge(isa1[0],isa1[1])
G_reduction = nx.transitive_reduction(G)
a = list(G_reduction.edges)
#
file1 = open("GeneratedHierarchy_LBL_1st&2nd&UpperLayer.txt","w")
for edge in a:
    file1.write(edge[0]+"\t"+edge[1]+"\n")
file1.close()
###############################################################################
import networkx as nx
file2 = open("GeneratedHierarchy_LBL_1st&2nd&UpperLayer.txt","rb")
Dirgraph1 = nx.read_edgelist(file2, create_using = nx.DiGraph(), nodetype = str)
file2.close()
Dirgraph1Reverse = Dirgraph1.reverse(copy=True) 
topo_sortL = list(nx.topological_sort(Dirgraph1Reverse))
root = [n for n,d in Dirgraph1.out_degree() if d==0]
#
import csv
newConceptDic1 = {}
counter1 = 10000
file1 = open("newConcept_LBL_1stLayer.csv","r")
reader1 = csv.reader(file1)
for row1 in reader1:
    counter1 = counter1+1
    createdID = "NEW"+str(counter1)
    newConceptDic1[createdID] = row1[0]
file1.close()
#
newConceptDic2 = {}
counter2 = 20000
file2 = open("newConcept_LBL_2nd&UpperLayer.csv","r")
reader2 = csv.reader(file2)
for row2 in reader2:
    counter2 = counter2+1
    createdID = "NEW"+str(counter2)
    newConceptDic2[createdID] = row2[0]
file2.close()
###############################################################################
import csv
originalConceptDic = {}
duplicateDescription = {}
file1 = open("TestDevices_BREAST IMPLANT_LBL.csv","r")
reader1 = csv.reader(file1)
for row1 in reader1:
    originalConceptDic[row1[0]] = (row1[6],row1[2])
    if duplicateDescription.get(row1[6],"default") == "default":
        duplicateDescription[row1[6]] = [row1[0]]
    else:
        duplicateDescription[row1[6]].append(row1[0])
file1.close()
#
counter3 = 30000
duplicateDescriptionkeyL = sorted(duplicateDescription.keys())
for keyDup in duplicateDescriptionkeyL:
    if len(duplicateDescription.get(keyDup)) >1:
        counter3 = counter3+1
        createdID = "NEW"+str(counter3)
        newConceptDic2[createdID] = keyDup
#
a = list(newConceptDic1.keys())
b = list(newConceptDic2.keys())
d = list(originalConceptDic.keys())
a.extend(b)
a.extend(d)
c = set(a)-set(topo_sortL)
root.extend(list(c))
#
from owlready2 import *
import types
onto = get_ontology("http://test.org/testOnto.owl")
onto.save(file = "testOnto_LBL_All.owl",format = "rdfxml")
#
onto = get_ontology("testOnto_LBL_All.owl").load()
a = list(onto.classes())
#
with onto:
    class device_id(AnnotationProperty):
        pass
    class company_name(AnnotationProperty):
        pass
with onto:
    for concept1 in root:
        if concept1.startswith("NEW1"):
            generatedName = newConceptDic1.get(concept1)
            companyName = "N/A"
        elif concept1.startswith("NEW"):
            generatedName = newConceptDic2.get(concept1)
            companyName = "N/A"
        else:
            generatedName = originalConceptDic.get(concept1)[0]
            companyName  = originalConceptDic.get(concept1)[1]
        NewClass = types.new_class(concept1,(Thing,))
        NewClass.label.append(generatedName)
        NewClass.device_id = concept1
        NewClass.company_name = companyName
with onto:
    for concept2 in topo_sortL:
        if concept2 not in root:
            if concept2.startswith("NEW1"):
                generatedName = newConceptDic1.get(concept2)
                companyName = "N/A"
            elif concept2.startswith("NEW"):
                generatedName = newConceptDic2.get(concept2)
                companyName = "N/A"
            else:
                generatedName = originalConceptDic.get(concept2)[0]
                companyName  = originalConceptDic.get(concept2)[1]                    
            ancestor = list(Dirgraph1.successors(concept2))
            ancestorObject = []
            a = list(onto.classes())
            for ancestor1 in ancestor:
                for class1 in a:
                    if class1.name == ancestor1:
                        ancestorObject.append(class1)
            NewClass = types.new_class(concept2,tuple(ancestorObject))
            NewClass.label.append(generatedName)        
            NewClass.device_id = concept2
            NewClass.company_name = companyName
onto.save(file = "testOnto_LBL_All.owl",format = "rdfxml")   
#   
#destroy_entity(Class) 
a = list(onto.classes())   
for testclass in a:
    destroy_entity(testclass)
b = list(onto.properties())
for c in b:
    destroy_entity(c)
d = list(onto.annotation_properties())
for e in d:
    destroy_entity(e)            
###############################################################################
#Some statistical info
###############################################################################
import networkx as nx
file2 = open("GeneratedHierarchy_LBL_1st&2nd&UpperLayer.txt","rb")
#file2  = open("GeneratedHierarchy_ccRemoved.txt","rb")
Dirgraph1 = nx.read_edgelist(file2, create_using = nx.DiGraph(), nodetype = str)
file2.close()
Dirgraph1Reverse = Dirgraph1.reverse(copy=True) 
Longestpath = nx.dag_longest_path(Dirgraph1Reverse)# 
concept1 = "00812790021666"
root = "NEW20014"
Longestpath2 = max(nx.all_simple_paths(Dirgraph1Reverse,root,concept1),key=lambda x: len(x))
print(len(Longestpath))
for concept2 in Longestpath2:
    if concept2.startswith("NEW1"):
        generatedName = newConceptDic1.get(concept2)
        print(generatedName)
    elif concept2.startswith("NEW"):
        generatedName = newConceptDic2.get(concept2)
        print(generatedName)
    else:
        generatedName = originalConceptDic.get(concept2)[0]
        print(generatedName)
root = [n for n,d in Dirgraph1.out_degree() if d==0]
subhierarchyRoot = list(Dirgraph1Reverse.successors(root[0]))
for concept2 in subhierarchyRoot :
    if concept2.startswith("NEW1"):
        generatedName = newConceptDic1.get(concept2)
        print(generatedName)
    elif concept2.startswith("NEW"):
        generatedName = newConceptDic2.get(concept2)
        print(generatedName)
    else:
        generatedName = originalConceptDic.get(concept2)[0]
        print(generatedName)
#        
def findSubhiearchy(node):
    Dic = dict(nx.bfs_successors(Dirgraph1Reverse,node))
    d = Dic.values()
    c = []
    for e in d:
        c.extend(e)
    return c  
for concept2 in subhierarchyRoot:
    print(len(findSubhiearchy(concept2)))
#
i = 0
for key in newConceptDic2.keys():
    if key.startswith("NEW3"):
        i = i+1
print(i)
#
allNodes = list(Dirgraph1.nodes())
total = 0
LevelDic = {}
for concept in allNodes:
    if len(list(nx.all_simple_paths(Dirgraph1,concept,root)))>0:
        Level = len(max(nx.all_simple_paths(Dirgraph1,concept,root),key=lambda x: len(x)))
        total = total+ Level
        if LevelDic.get(Level,"empty") == "empty":
            LevelDic[Level] = 1
        else:
            LevelDic[Level] = LevelDic.get(Level)+1
print(float(total)/len(allNodes))
print(LevelDic)
#
directParent = {}
total=0
for concept in allNodes:
    a = len(list(Dirgraph1.successors(concept)))
    if a ==2:
        print(concept)
    total = total+a
    if directParent.get(a,"default") == "default":
        directParent[a] = 1
    else:
        directParent[a] = directParent.get(a)+1
print(float(total)/len(allNodes))
print(directParent)