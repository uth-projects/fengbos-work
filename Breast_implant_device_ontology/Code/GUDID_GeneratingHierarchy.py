#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 26 18:48:09 2020

@author: fengbozheng
"""
###############################################################################
#Pre-processing
###############################################################################
FDAPremarket = {}
file3 = open("premarketSubmissions.txt","r")
for lines3 in file3:
    line3 = lines3.split("\n")[0].split("|")
    FDAPremarket[line3[0]] = line3[1]
file3.close()
#    
gmdnTerm = {}
file4 = open("gmdnTerms.txt","r")
for lines4 in file4:
    line4 = lines4.split("\n")[0].split("|")
    gmdnTerm[line4[0]] = line4[1]
file4.close()
#
file2 = open("TestDevices_BREAST IMPLANT.csv","w")
import csv
writer2 = csv.writer(file2)
file1 = open("device.txt","r")
for lines1 in file1:
    line1 = lines1.split("|")
    primaryDI = line1[0] #00812790020737
    brandName = line1[9] #SIENTRA Silicone Gel Breast Implant
    companyName = line1[13] #Sientra, Inc.
    deviceDescription = line1[15].lower() #Silicone Gel Breast Implant, Smooth Round, High Profile, 190 cc
    singleUse = line1[21] #True
    prescriptionUse = line1[30]#True
    deviceSterile = line1[32]
    #gmdnPT = gmdnTerm.get(line1[0],"")
    #OTC = line1[31] #
    #if companyName == "Sientra, Inc." and "breast implant" in deviceDescription.lower():
    if "breast implant" in deviceDescription:
        if deviceDescription[len(deviceDescription)-1] == " ":
            deviceDescription = deviceDescription[:len(deviceDescription)-1]
        deviceDescription = deviceDescription.replace("breast implant.", "breast implant,")
        deviceDescription = deviceDescription.replace("  "," ")
        descriptionList = deviceDescription.split(", ")
        attr = []
        for item in descriptionList:
            if "breast implant" in item:
                usefulness = item
            else:
                if item != "":
                    attr.append(item)
        #could add "is_subject_device_of: "+FDAPremarket.get(primaryDI,"")
        writer2.writerow((primaryDI,brandName,companyName,"singleUse: "+singleUse,"prescriptionUse: "+prescriptionUse,"packageSterile: "+deviceSterile,deviceDescription,"mainBody: "+usefulness)+tuple(attr))
file1.close()
file2.close()
###############################################################################
import csv
attrTerm = ["single use", "single-use", "reusable", "sterile","non-sterile"]
singleSterile = ["single use", "single-use", "reusable", "sterile","non-sterile"]
file1 = open("TestDevices_BREAST IMPLANT.csv","r")
reader1 = csv.reader(file1)
for row1 in reader1:
    attrTerm.extend(row1[8:])
file1.close()
#
def subList(a,b): #check if a in b
    j = 0
    for i in range(len(b)):
        if b[i:i+len(a)] == a:
            j = j+1
    if j ==0:
        return False
    else:
        return True
#
attrTermList = sorted(set(attrTerm),key=len, reverse=True)    
#sort because "smooth round","round" e.g., #00081317007029
file2 = open("TestDevices_BREAST IMPLANT.csv","r")
file3 = open("TestDevices_BREAST IMPLANT_Processed.csv","w")
writer3 = csv.writer(file3)
reader2 = csv.reader(file2)
for row2 in reader2:
    mainText = row2[7].split("mainBody: ")[1]
    mainBody = mainText.split(" ")
    newAttr = row2[8:]
    for attrTerm1 in attrTermList:
        if any(subList(attrTerm1.split(" "),x.split(" ")) for x in newAttr):
            newAttr.append(attrTerm1)
        else:
            attrTermL = attrTerm1.split(" ")
            if subList(attrTermL,mainBody):
                 mainText = mainText.replace(attrTerm1,"").replace("  "," ")
                 if mainText.startswith(" "):
                     mainText = mainText[1:]
                 if mainText[len(mainText)-1] == " ":
                     mainText = mainText[:len(mainText)-1]
                 mainBody = mainText.split(" ")             
                 if attrTerm1 not in singleSterile:
                     newAttr.append(attrTerm1)         
    newAttr = sorted(set(newAttr))
    if row2[3].split("singleUse: ")[1] == "true":
        newAttr.append(("single use","true"))
    else:
        if row2[3].split("singleUse: ")[1] == "false":
            newAttr.append(("single use","false"))
        else:
            newAttr.append(("single use","not specified"))
    if row2[5].split("packageSterile: ")[1] == "true":
        newAttr.append(("sterile","true"))
    else:
        if row2[5].split("packageSterile: ")[1] == "false":
            newAttr.append(("sterile","false"))
        else:
            newAttr.append(("sterile","not specified"))
    for i in range(len(newAttr)):
        if "cc" in newAttr[i]:
            index = newAttr[i].index("cc")
            if newAttr[i][index-1] != " ":
                newAttrItem = newAttr[i][:index]+" "+newAttr[i][index:] 
                newAttr[i] = newAttrItem
    writer3.writerow(tuple(row2[:7])+("processedMainBody: "+mainText,)+tuple(newAttr))
file2.close()
file3.close()
##474 for example
###############################################################################
#FCA
###############################################################################
import csv
FCA = {}
#testDuplicate = set()
file1 = open("TestDevices_BREAST IMPLANT_Processed.csv","r")
reader1 = csv.reader(file1)
for row1 in reader1:
    attributeList = []
    mainBody = row1[7].split("processedMainBody: ")[1]
    attributeList.append(("MainBody",mainBody))
    attributeList.append(("CompanyName",row1[2]))
    for singleItem in row1[8:]:
        if "('single use'," in singleItem:
            value = singleItem.split("', '")[1].split("')")[0]
            attributeList.append(("single use",value))
        elif "('sterile'," in singleItem:
            value = singleItem.split("', '")[1].split("')")[0]
            attributeList.append(("sterile",value))
        else:
            attributeList.append(("lexAttr",singleItem))
    FCA[row1[0]] = sorted(attributeList)
    #if tuple(attributeList) not in testDuplicate:
    #    testDuplicate.add(tuple(attributeList))
    #else:
    #    print(row1)
file1.close()
#size: 920   
oldset = set() 
for keys1 in FCA.keys():
    itsFCA = FCA.get(keys1)
    oldset.add(tuple(itsFCA))
#
def lcs(S,T):
    m = len(S)
    n = len(T)
    counter = [[0]*(n+1) for x in range(m+1)]
    longest = 0
    lcs_set = set()
    for i in range(m):
        for j in range(n):
            if S[i] == T[j]:
                c = counter[i][j] + 1
                counter[i+1][j+1] = c
                if c > longest:
                    lcs_set = set()
                    longest = c
                    lcs_set.add(" ".join(S[i-c+1:i+1]))
                    #print(lcs_set)
                elif c == longest:
                    lcs_set.add(" ".join(S[i-c+1:i+1]))
                    #print(lcs_set)
    if len(lcs_set)>0:
        return list(lcs_set)[0]
    else:
        return False
#
def Lex_intersection(FCA1, FCA2):
    FCA1MainBody = [x[1] for x in FCA1 if x[0] == "MainBody"][0]
    FCA2MainBody = [y[1] for y in FCA2 if y[0] == "MainBody"][0]
    FCA1Rest = [x2 for x2 in FCA1 if x2[0] != "MainBody"]
    FCA2Rest = [y2 for y2 in FCA2 if y2[0] != "MainBody"]
    RestIntersection = set(FCA1Rest).intersection(set(FCA2Rest))
    MainBodyLCS = lcs(FCA1MainBody.split(" "),FCA2MainBody.split(" "))
    if MainBodyLCS != False and len(MainBodyLCS.split(" "))>1:# and all(" cc" not in pp[1] for pp in RestIntersection):        #and len(RestIntersection)>0 
        IntersectonResult = sorted((("MainBody",MainBodyLCS),)+tuple(RestIntersection))
        return tuple(IntersectonResult)
    else:
        return False
#
Originalset = oldset.copy()
Initialset = oldset.copy()
NewlyAdded = oldset.copy()
mm = 0
while(len(NewlyAdded)!=0):
    print(mm)
    mm = mm +1
    LastIteration = Initialset.copy()
    LastIterationL = sorted(LastIteration)
    for p in range(0,len(LastIterationL)-1):
        for q in range(p+1,len(LastIterationL)):
            inter = Lex_intersection(LastIterationL[p],LastIterationL[q])
            if inter != False:
                Initialset.add(inter)
    NewlyAdded = Initialset - LastIteration
    print(len(NewlyAdded))
Result = Initialset - Originalset 
print(len(Result))
file5 = open("newConcept_ccRemoved.csv","w")
writer5 = csv.writer(file5)
for result1 in Result:
    if all(" cc" not in pp[1] for pp in result1):
        writer5.writerow(result1)
file5.close()
###############################################################################
#Subsumption checking and hierarchy generation
###############################################################################
def moreSpecific(FCA1,FCA2): #check if FCA1 is more specifc than FCA2
    FCA1MainBody = [x[1] for x in FCA1 if x[0] == "MainBody"][0]
    #print(FCA1MainBody)
    FCA2MainBody = [y[1] for y in FCA2 if y[0] == "MainBody"][0]
    #print(FCA2MainBody)
    FCA1Rest = [x2 for x2 in FCA1 if x2[0] != "MainBody"]
    FCA2Rest = [y2 for y2 in FCA2 if y2[0] != "MainBody"]    
    #print(FCA1Rest)
    #print(FCA2Rest)
    if FCA2MainBody in FCA1MainBody and set(FCA2Rest).issubset(set(FCA1Rest)):
        return True
    else:
        return False
# 
import csv
FCA_to_DI = {}
ID_to_FCA = {}
file1 = open("TestDevices_BREAST IMPLANT_Processed.csv","r")
reader1 = csv.reader(file1)
for row1 in reader1:
    attributeList = []
    mainBody = row1[7].split("processedMainBody: ")[1]
    attributeList.append(("MainBody",mainBody))
    attributeList.append(("CompanyName",row1[2]))
    for singleItem in row1[8:]:
        if "('single use'," in singleItem:
            value = singleItem.split("', '")[1].split("')")[0]
            attributeList.append(("single use",value))
        elif "('sterile'," in singleItem:
            value = singleItem.split("', '")[1].split("')")[0]
            attributeList.append(("sterile",value))  
        else:
            attributeList.append(("lexAttr",singleItem))
    FCAtuple = tuple(sorted(attributeList))
    ID_to_FCA[row1[0]] = FCAtuple
    if FCA_to_DI.get(FCAtuple,"Default") == "Default":
        FCA_to_DI[FCAtuple] = [row1[0]]
    else:
        FCA_to_DI[FCAtuple].append(row1[0])
file1.close()
#
newConceptDic = {}
counter = 10000
file2 = open("newConcept_ccRemoved.csv","r")
reader2 = csv.reader(file2)
for row2 in reader2:
    counter = counter+1
    createdID = "NEW"+str(counter)
    attrList = []
    for tupleString in row2[:]:
        a = tupleString[2:len(tupleString)-2].split("', '")
        attrList.append((a[0],a[1]))
    newConceptDic[createdID] = tuple(sorted(attrList)) 
file2.close()
#
counter2 = 20000
duplicateDescriptionkeyL = sorted(FCA_to_DI.keys())
for keyDup in duplicateDescriptionkeyL:
    if len(FCA_to_DI.get(keyDup)) >1:
        counter2 = counter2+1
        createdID = "NEW"+str(counter2)
        newConceptDic[createdID] = keyDup
#
ISA= set()
newConcepts = list(newConceptDic.keys())
existingConcept = list(ID_to_FCA.keys())
for Concept1 in newConcepts:
    FCA1 = newConceptDic.get(Concept1)
    for Concept2 in existingConcept:
        FCA2 = ID_to_FCA.get(Concept2)
        if moreSpecific(FCA2,FCA1):
            ISA.add((Concept2, Concept1))
for Concept3 in newConcepts:
    FCA3 = newConceptDic.get(Concept3)
    for Concept4 in newConcepts:
        if Concept3 != Concept4:
            FCA4 = newConceptDic.get(Concept4)
            if moreSpecific(FCA3,FCA4):
                ISA.add((Concept3, Concept4))
#
import networkx as nx
G = nx.DiGraph()
for isa1 in ISA:
    G.add_edge(isa1[0],isa1[1])
G_reduction = nx.transitive_reduction(G)
a = list(G_reduction.edges)
#
file1 = open("GeneratedHierarchy_ccRemoved.txt","w")
for edge in a:
    file1.write(edge[0]+"\t"+edge[1]+"\n")
file1.close()
###############################################################################
#Create Ontology
###############################################################################
import networkx as nx
file2 = open("GeneratedHierarchy_ccRemoved.txt","rb")
Dirgraph1 = nx.read_edgelist(file2, create_using = nx.DiGraph(), nodetype = str)
file2.close()
Dirgraph1Reverse = Dirgraph1.reverse(copy=True) 
topo_sortL = list(nx.topological_sort(Dirgraph1Reverse))
root = [n for n,d in Dirgraph1.out_degree() if d==0]
#
import csv
newConceptDic = {}
counter = 10000
file2 = open("newConcept_ccRemoved.csv","r")
reader2 = csv.reader(file2)
for row2 in reader2:
    counter = counter+1
    createdID = "NEW"+str(counter)
    attrList = []
    separateAttr = []
    for tupleString in row2[:]:
        a = tupleString[2:len(tupleString)-2].split("', '")
        if a[0] in ["single use","sterile"]:
            separateAttr.append((a[0],a[1]))
        else:
            attrList.append((a[0],a[1]))
    generatedName = "; ".join([x[1] for x in attrList])
    newConceptDic[createdID] = (generatedName,)+tuple(separateAttr)
file2.close()
#
import csv
FCA_to_DI = {}
ID_to_FCA = {}
file1 = open("TestDevices_BREAST IMPLANT_Processed.csv","r")
reader1 = csv.reader(file1)
for row1 in reader1:
    attributeList = []
    mainBody = row1[7].split("processedMainBody: ")[1]
    attributeList.append(("MainBody",mainBody))
    attributeList.append(("CompanyName",row1[2]))
    separateAttr = []
    for singleItem in row1[8:]:
        if "('single use'," in singleItem:
            value = singleItem.split("', '")[1].split("')")[0]
            attributeList.append(("single use",value))
            separateAttr.append(("single use",value))
        elif "('sterile'," in singleItem:
            value = singleItem.split("', '")[1].split("')")[0]
            attributeList.append(("sterile",value))  
            separateAttr.append(("sterile",value)) 
        else:
            attributeList.append(("lexAttr",singleItem))
    FCAtuple = tuple(sorted(attributeList))
    ID_to_FCA[row1[0]] = (row1[6],row1[2])+tuple(separateAttr)
    if FCA_to_DI.get(FCAtuple,"Default") == "Default":
        FCA_to_DI[FCAtuple] = [row1[0]]
    else:
        FCA_to_DI[FCAtuple].append(row1[0])
file1.close()
#
counter2 = 20000
duplicateDescriptionkeyL = sorted(FCA_to_DI.keys())
for keyDup in duplicateDescriptionkeyL:
    if len(FCA_to_DI.get(keyDup)) >1:
        counter2 = counter2+1
        createdID = "NEW"+str(counter2)
        separateAttr = []
        attrList = []
        for item2 in keyDup:
            if item2[0] in ["single use","sterile"]:
                separateAttr.append((item2[0],item2[1]))
            else:
                attrList.append((item2[0],item2[1]))
        generatedName = "; ".join([x[1] for x in sorted(attrList)])
        newConceptDic[createdID] = (generatedName,)+tuple(separateAttr)
#
a = list(ID_to_FCA.keys())
b = list(newConceptDic .keys())
a.extend(b)
c = set(a)-set(topo_sortL)
root.extend(list(c))
#
from owlready2 import *
import types
onto = get_ontology("http://test.org/testOnto.owl")
onto.save(file = "testOnto_ccRemoved.owl",format = "rdfxml")
#
onto = get_ontology("testOnto_ccRemoved.owl").load()
a = list(onto.classes())
with onto:
    class device_id(AnnotationProperty):
        pass
    class company_name(AnnotationProperty):
        pass
    class single_use(AnnotationProperty):
        pass
    class package_sterile(AnnotationProperty):
        pass
#    
with onto:
    for concept1 in root:
        if concept1.startswith("NEW"):
            queryResult = newConceptDic.get(concept1)
            generatedName = queryResult[0]
            companyName = "N/A"
            if any(x[0] == "single use" for x in queryResult):
                singleUse = [x[1] for x in queryResult if x[0] == "single use"][0]
            else:
                singleUse = "N/A"
            if any(x[0] == "sterile" for x in queryResult):
                sterile = [x[1] for x in queryResult if x[0] == "sterile"][0]
            else:
                sterile = "N/A"   
        else:
            queryResult = ID_to_FCA.get(concept1)
            generatedName = queryResult[0]
            companyName = queryResult[1]
            if any(x[0] == "single use" for x in queryResult):
                singleUse = [x[1] for x in queryResult if x[0] == "single use"][0]
            else:
                singleUse = "N/A"
            if any(x[0] == "sterile" for x in queryResult):
                sterile = [x[1] for x in queryResult if x[0] == "sterile"][0]
            else:
                sterile = "N/A"   
        NewClass = types.new_class(concept1,(Thing,))
        NewClass.label.append(generatedName)
        NewClass.device_id = concept1
        NewClass.company_name = companyName
        NewClass.single_use = singleUse
        NewClass.package_sterile = sterile
        
with onto:
    for concept2 in topo_sortL:
        if concept2 not in root:
            if concept2.startswith("NEW"):
                queryResult = newConceptDic.get(concept2)
                generatedName = queryResult[0]
                companyName = "N/A"
                if any(x[0] == "single use" for x in queryResult):
                    singleUse = [x[1] for x in queryResult if x[0] == "single use"][0]
                else:
                    singleUse = "N/A"
                if any(x[0] == "sterile" for x in queryResult):
                    sterile = [x[1] for x in queryResult if x[0] == "sterile"][0]
                else:
                    sterile = "N/A"  
            else:
                queryResult = ID_to_FCA.get(concept2)
                generatedName = queryResult[0]
                companyName = queryResult[1]
                if any(x[0] == "single use" for x in queryResult):
                    singleUse = [x[1] for x in queryResult if x[0] == "single use"][0]
                else:
                    singleUse = "N/A"
                if any(x[0] == "sterile" for x in queryResult):
                    sterile = [x[1] for x in queryResult if x[0] == "sterile"][0]
                else:
                    sterile = "N/A"   
            ancestor = list(Dirgraph1.successors(concept2))
            ancestorObject = []
            a = list(onto.classes())
            for ancestor1 in ancestor:
                for class1 in a:
                    if class1.name == ancestor1:
                        ancestorObject.append(class1)
            NewClass = types.new_class(concept2,tuple(ancestorObject))
            NewClass.label.append(generatedName)
            NewClass.device_id = concept2
            NewClass.company_name = companyName
            NewClass.single_use = singleUse
            NewClass.package_sterile = sterile      
onto.save(file = "testOnto_ccRemoved.owl",format = "rdfxml")   
#   
#destroy_entity(Class) 
a = list(onto.classes())   
for testclass in a:
    destroy_entity(testclass)
b = list(onto.properties())
for c in b:
    destroy_entity(c)
d = list(onto.annotation_properties())
for e in d:
    destroy_entity(e)
###############################################################################
#Some Statistical Information
###############################################################################
import networkx as nx
#file2 = open("GeneratedHierarchy_LBL_1st&2nd&UpperLayer.txt","rb")
file2  = open("GeneratedHierarchy_ccRemoved.txt","rb")
Dirgraph1 = nx.read_edgelist(file2, create_using = nx.DiGraph(), nodetype = str)
file2.close()
Dirgraph1Reverse = Dirgraph1.reverse(copy=True) 
Longestpath = nx.dag_longest_path(Dirgraph1Reverse)# 
print(len(Longestpath))
for concept2 in Longestpath:
    if concept2.startswith("NEW"):
        generatedName = newConceptDic.get(concept2)
        print(generatedName)
    else:
        generatedName = ID_to_FCA.get(concept2)
        print(generatedName)
root = [n for n,d in Dirgraph1.out_degree() if d==0]
subhierarchyRoot = list(Dirgraph1Reverse.successors(root[0]))
for concept2 in subhierarchyRoot :
    if concept2.startswith("NEW"):
        generatedName = newConceptDic.get(concept2)
        print(generatedName)
    else:
        generatedName = ID_to_FCA.get(concept2)
        print(generatedName)
#        
def findSubhiearchy(node):
    Dic = dict(nx.bfs_successors(Dirgraph1Reverse,node))
    d = Dic.values()
    c = []
    for e in d:
        c.extend(e)
    return c  
for concept2 in subhierarchyRoot:
    print(len(findSubhiearchy(concept2)))
#
i = 0
for key in newConceptDic.keys():
    if key.startswith("NEW2"):
        i = i+1
print(i)
#
allNodes = list(Dirgraph1.nodes())
total = 0
LevelDic = {}
for concept in allNodes:
    if len(list(nx.all_simple_paths(Dirgraph1,concept,root)))>0:
        Level = len(max(nx.all_simple_paths(Dirgraph1,concept,root),key=lambda x: len(x)))
        total = total+ Level
        if LevelDic.get(Level,"empty") == "empty":
            LevelDic[Level] = 1
        else:
            LevelDic[Level] = LevelDic.get(Level)+1
print(float(total)/len(allNodes))
print(LevelDic)