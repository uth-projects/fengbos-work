\documentclass[conference]{IEEEtran}
\IEEEoverridecommandlockouts
% The preceding line is only needed to identify funding in the first footnote. If that is unneeded, please comment it out.
\usepackage{cite}
\usepackage{adjustbox}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{algorithmic}
%\usepackage{titlesec}
\usepackage{graphicx}
\usepackage{textcomp}
\usepackage{xcolor}
\def\BibTeX{{\rm B\kern-.05em{\sc i\kern-.025em b}\kern-.08em
    T\kern-.1667em\lower.7ex\hbox{E}\kern-.125emX}}
\usepackage{multirow}  
%\usepackage{float}
%\setlength{\intextsep}{1.0pt plus 1.0pt minus 2.0pt}
%\setlength{\parskip}{0em} 
%\setlength{\abovecaptionskip}{0cm}
%\setlength{\belowcaptionskip}{0cm}


\begin{document}
\title{A Lexical-based Formal Concept Analysis Method to Identify Missing Concepts in the NCI Thesaurus\\  
\thanks{This work was supported by the US National Science Foundation (NSF) under grant 1931134 and National Institutes of Health (NIH) under grant R01LM013335. The content is solely the responsibility of the authors and does not necessarily represent the official views of the NSF or NIH. Correspondence: \mbox{licong.cui@uth.tmc.edu}}
}



%\author{\IEEEauthorblockN{Fengbo Zheng}
%\IEEEauthorblockA{\textit{Department of Computer Science} \\
%\textit{University of Kentucky}\\
%Lexington, Kentucky, USA \\
%fengbo.zheng@uky.edu}
%\and
%\IEEEauthorblockN{Licong Cui}
%\IEEEauthorblockA{\textit{School of Biomedical Informatics} \\
%\textit{University of Texas Health Science Center at Houston}\\
%Houston, Texas, USA \\
%licong.cui@uth.tmc.edu}
%}
\author{\IEEEauthorblockN{Fengbo Zheng\IEEEauthorrefmark{1}$^, $\IEEEauthorrefmark{2},
Licong Cui\IEEEauthorrefmark{2}}
\IEEEauthorblockA{
\IEEEauthorrefmark{1}Department of Computer Science, University of Kentucky, Lexington, Kentucky, USA\\
\IEEEauthorrefmark{2}School of Biomedical Informatics, University of Texas Health Science Center at Houston, Houston, Texas, USA}}

\maketitle

\begin{abstract}
Biomedical terminologies have been increasingly used in modern biomedical research and applications to facilitate data management and ensure semantic interoperability. As part of the evolution process, new concepts are regularly added to biomedical terminologies in response to the evolving domain knowledge and emerging applications. Most existing concept enrichment methods suggest new concepts via directly importing knowledge from external sources. %and to some extent ignores the intrinsic knowledge contained in a terminology.
In this paper, we introduced a lexical method based on formal concept analysis (FCA) to identify potentially missing concepts in a given terminology by leveraging its intrinsic knowledge -- concept names. We first construct the FCA formal context based on the lexical features of concepts. Then we perform multistage intersection to formalize new concepts and detect potentially missing concepts. 
We applied our method to the {\em Disease or Disorder} sub-hierarchy in the National Cancer Institute (NCI) Thesaurus (19.08d version) and identified a total of 8,983 potentially missing concepts. As a preliminary evaluation of our method to validate the potentially missing concepts, we further checked whether they were included in any external source terminology in the Unified Medical Language System (UMLS). The result showed that 592 out of 8,937 potentially missing concepts were found in the UMLS.\end{abstract}

\begin{IEEEkeywords}
Biomedical Terminologies, Concept Enrichment, Formal Concept Analysis
\end{IEEEkeywords}

\section{Introduction}
A terminology or ontology provides formalized representation of knowledge in a domain, including a set of concepts and the describable relationships among them. In biomedicine, terminologies have played important roles in biomedical research and applications to ensure data consistency and interoperability~\cite{bodenreider2008biomedical}. For instance, the National Cancer Institute (NCI) Thesaurus,
covering knowledge of cancers, genes and therapies~\cite{de2004nci,fragoso2004overview,sioutos2007nci}, has been widely used as a standard for biomedical coding, knowledge reference, and public reporting for many NCI and other systems~\cite{haendel2018census}.

Biomedical terminologies are often incomplete and constantly evolving due to the growing knowledge in biomedicine and new requirements from emerging biomedical applications~\cite{cui2016biomedical}. During the terminology evolution process, new concepts are regularly added to the newer versions. For instance, the NCI Thesaurus is updated every month with averaging roughly 700 new concepts in each release~\cite{NCItOverview}.

There are two types of approaches to identify new or missing concepts for the concept enrichment of biomedical terminologies. One type mainly leverages extrinsic knowledge (or external sources). For instance, Chandar et al. developed a similarity-based method that suggests extracted phrases from text corpus as new concepts for the SNOMED~CT~\cite{chandar2015similarity}. Peng et al. analyzed connected matrices from the Gene Ontology (GO) and biological network to identified new terms for the GO~\cite{peng2016extending}. He et al. leveraged alignment between different terminologies to suggest new concepts for the SNOMED CT~\cite{he2015comparative} and NCI Thesaurus~\cite{he2016topological}. 
%In these works, the new concepts were identified by mainly using extrinsic knowledge (or external sources). 

The other type mainly utilizes the intrinsic knowledge within the terminology itself. For example, Jiang and Chute performed Formal Concept Analysis (FCA) based on logical definitions of concepts in the SNOMED~CT to search for possible missing concepts~\cite{jiang2009auditing}. Zhu et al. developed a scalable multistage algorithm called Spark-MCA~\cite{wei2017spark} to deal with the computational challenge of performing large-scale FCA for evaluating concept completeness of the SNOMED~CT. A limitation of these two FCA-based approaches is that the potentially missing concepts identified only involve logical definitions and no concept names were provided. Therefore, it is difficult to validate those missing concepts. 
In previous work~\cite{cui2017mining}, we discovered a lexical pattern in non-lattice subgraphs that can reveal missing concepts in the SNOMED CT; and we explored deep learning-based methods to properly name a concept given its lexical components (or a bag of words)~\cite{zheng2018exploring}. However, our previous work is limited to a specific type of lexical patterns and sub-structures of terminologies, which only revealed a small portion of missing concepts.

In this paper, we introduce a lexical- and FCA-based method to identify potentially missing concepts in the NCI Thesaurus. Lexical features (i.e., words appeared in the concept names) are considered as FCA attributes while generating formal context. Applying multistage intersection of FCA attributes identifies newly formalized bags of words (i.e., FCA formal concepts) that represent missing concepts, which may be further validated through external knowledge.
We applied our method to the \textit{Disease or Disorder} (C2991) sub-hierarchy in 19.08d version of the NCI Thesaurus and identified 8,983 potentially missing concepts. We performed a preliminary evaluation and validated that 592 out of 8,983 potentially missing concepts were included in external terminologies in the Unified Medical Language System (UMLS). 

\section{Background}~\label{background}
%\subsection{Formal Concept Analysis}
FCA is a mathematical theory concerned with the formalization of concepts and conceptual thinking\cite{ignatov2014introduction}. With FCA, we can generate a concept hierarchy from a collection of object and attributes.
The input of FCA is \textit{formal context} $K = (O, A, R)$, where $O$ is a set of objects, $A$ is a set of attributes, and $R$ is a binary relation between $O$ and $A$. The notation ($o,a$) $\in R$ means that object $o$ has attribute $a$.

Each formal context $K$ induces two operators: derivation operators $\uparrow: 2^O \rightarrow 2^A$ and concept-forming operators $\downarrow: 2^A \rightarrow 2^O$. 
The operators are defined, for each $X \subseteq O$ and $Y \subseteq A$,  as follows:
$$X^\uparrow = \{a\in A|\forall o \in X\!\!:(o,a) \in R\},$$
$$Y^\downarrow = \{o\in O|\forall a \in Y\!\!:(o,a) \in R\},$$ 
where $X^\uparrow$ is the set of all attributes shared by all objects in $X$, and $Y^\downarrow$ is the set of all objects sharing all attributes in $Y$. 

A formal concept of $K$ is a pair $(X, Y)$ with $X \subseteq O$ and $Y \subseteq A$ such that $X^\uparrow = Y$ and $Y^\downarrow = X$.  The subconcept-superconcept relation between formal concepts is given by $(X_1,Y_1)\leq (X_2,Y_2)$ iff $X_1 \subseteq X_2$ ($Y_2 \subseteq Y_1$). All formal concepts derived from the formal context $K$ together with the subconcept-superconcept relation form a complete lattice~\cite{ganter2012formal}. Note that lattice is a desired property for well-structured terminologies. 

\section{Method}~\label{method}
Our method mainly consists of two steps: (1) pre-processing concept names and constructing FCA formal context; and (2) performing FCA via a multistage intersection algorithm to identify potentially missing (or new) concepts in the NCI Thesaurus.

\subsection{Constructing Formal Context}
Given a collection of concepts in the terminology, we consider all the concepts as FCA objects $O$ and words appears in the concept names (i.e., lexical features) as FCA attributes $A$, respectively. With the binary relation $R \subseteq O \times A$ specifying whether concept $o\in O$ contains word $a\in A$, we can construct the FCA formal context $K = (O, A, R)$. 

Since words appearing in concept names may have variations (e.g., plural vs. singular forms) or synonyms, we perform attribute/word normalization to create a more robust FCA formal context. For word variations, we normalize words appearing in concept names using LuiNorm~\cite{luinorm}, a lexical tool provided by the UMLS. For example, ``bones'' can be normalized to ``bone''. Regarding word synonyms, we leverage concepts in the NCI Thesaurus with single-word preferred names and single-word synonyms. More specifically, if a word $w$ itself is the preferred name of an NCI Thesaurus concept and has a synonym $s$ that is also a single word, then we maintain a mapping between the synonym $s$ and the preferred name $w$. This way words with the same meanings can be normalized to their preferred names thus the same attribute.

%After pre-processing concepts names and word normalization, we construct the FCA formal context based on the given concepts and their corresponding lexical features. 

\subsection{Identifying Potentially Missing Concepts}
To derive FCA formal concepts, we leverage the idea of the faster concept analysis introduced in~\cite{troy2007faster}, which is to perform multistage intersection on each pair of formal concepts from the initial formal concept set consisting of all objects, until no more new formal concept is generated. The pseudocode of the algorithm is shown in Fig.~\ref{pseudocode}.

\begin{figure}[h!]
\centering
\includegraphics[scale = 0.38]{Algorithm.png}
%\vspace{-3mm}
\caption{Pseudocode of identifying potentially missing concepts by multistage intersection.}
\label{pseudocode}
\end{figure}

In practice, for computation convenience, we perform operations on the lexical feature sets (i.e., using FCA attribute sets to represent FCA formal concepts). The initial set of FCA formal concepts is a set of FCA attribute sets, that is, the lexical feature sets of all the original concepts (i.e., \{$o^\uparrow\mid o\in O$\}). In the first iteration, we compute the intersection of each pair of FCA attribute sets in the initial set; and if the result is not included in the initial set, we add it into the initial set. We repeat this process until no new FCA attribute set can be derived.
Each newly generated FCA attribute set is taken as the lexical feature set of a potentially missing concept among the given concepts. An advantage of using lexical features (or words) as FCA attribute sets is that these words can be further leverage to name the newly discovered concepts.

\subsection{Illustrative Example}
Fig.~\ref{formalcontext} shows a simple example of FCA formal context in a tabular format generated from the concept \textit{Breast Fibroepithelial Neoplasm} (C40405) and its descendants in the NCI Thesaurus. The cells with check marks represent the binary relation between the concepts and their lexical features. Note that word ``Tumor'' is normalized to ``neoplasm'', since it is a synonym of \textit{Neoplasm} (C3262) in the NCI Thesaurus.

\begin{figure*}[t]
\includegraphics[scale = 0.4]{FormalContext.png}
%\vspace{-3mm}
\caption{An example of FCA formal context generated by the concept \textit{Breast Fibroepithelial Neoplasm} (C40405) in the NCI Thesaurus and its descendants in company with their lexical features. Word ``Tumor'' is normalized to ``neoplasm'' and word ``Phyllodes'' is normalized to ``phyllode''. An FCA formal concept (marked by blue cells) with FCA attribute set \{breast, neoplasm\} is considered as a potentially missing concept among the given concepts.}
\label{formalcontext}
\end{figure*}

Given the FCA formal context, the FCA formal concept with attribute set \{breast, neoplasm\} (see blue cells in Fig.~\ref{formalcontext}) can be derived by intersecting the attribute sets of \textit{Borderline Breast Phyllodes Tumor} (C5316) and \textit{Breast Fibroepithelial Neoplasm} (C40405). Therefore, a concept with lexical feature set \{breast, neoplasm\} is considered as a potentially missing concept for the given FCA formal context. This example only intends to illustrate how our method works, and one may have noticed that \textit{Breast Neoplasm} (C2910) is an existing concept in the NCI Thesaurus although it is not among the given concepts. For the actual implementation of our method, we further check if the newly generated concepts are existing in the NCI Thesaurus and ensure the removal of such cases from the list of potentially missing concepts.

\section{Results}~\label{result}
\vspace{-5mm}
\subsection{Summary Result}
We applied our method to the sub-hierarchies under \textit{Disease or Disorder} (C2991) in the NCI Thesaurus (19.08d version). Table~\ref{statisInfo} shows the numbers of existing concepts, newly generated concepts, and potentially missing concepts respectively for each sub-hierarchy. For example, there are 10,996 existing concepts in the \textit{Neoplasm} (C3262) sub-hierarchy; and FCA generated a total of 8,511 new concepts, among which 7,737 were potentially missing concepts in the NCI Thesaurus. 

Note that potentially missing concepts are detected in terms of the given FCA formal context (or the given collection of the input concepts). Therefore, the missing concepts detected in a sub-hierarchy may overlap with those detected in another sub-hierarchy. In total, 8,983 unique potentially missing concepts were identified among these sub-hierarchies. 

\subsection{Preliminary Evaluation}
%\subsection{Validation Using External Knowledge in the UMLS}
We performed a preliminary evaluation to validate the potentially missing concepts identified using the external knowledge in the UMLS. 
The UMLS integrates millions of biomedical concepts from more than 200 source terminologies, including the GO, SNOMED CT and Medical Subject Headings (MSH), to enable interoperability between biomedical information systems~\cite{bodenreider2004unified}. 

\begin{table}[h!]

\centering

\renewcommand{\arraystretch}{1.35}

\caption{The numbers of existing concepts, newly generated concepts, potentially missing concepts, and missing concepts validated via UMLS for each sub-hierarchy of \textit{Disease or Disorder} (C2991).}

\label{statisInfo}

\scalebox{0.72}{

\begin{tabular}{|l|r|r|r|r|}
\hline

\multicolumn{1}{|c|}{\multirow{2}{*}{\textbf{Sub-hierarchy}}} & \multirow{2}{*}{\textbf{\# of Concepts}} & \multicolumn{3}{c|}{\textbf{\# of Newly Generated Concepts}}                                                                                                                        \\ \cline{3-5}

\multicolumn{1}{|c|}{}                               &                                 & \textbf{Total} & \begin{tabular}[c]{@{}c@{}}\textbf{\# of Potentially} \\\textbf{Missing} \end{tabular} & \begin{tabular}[c]{@{}c@{}}\textbf{\# of Validated} \\\textbf{via UMLS}\end{tabular} \\ \hline

C27551: Disorder by Site                             & 13,595                           & 9,114  & 7,864                                                                                    & 451                                                                      \\ \hline

C3262: Neoplasm                                      & 10,996                           & 8,511  & 7,737                                                                                     & 289                                                                      \\ \hline

C53529: Non-Neoplastic Disorder                      & 4,198                            & 1,279  & 813                                                                                     & 227                                                                      \\ \hline

C8278: Cancer-Related Condition                      & 578                             & 491   & 374                                                                                     & 28                                                                       \\ \hline

C4873: Rare Disorder                                 & 915                             & 283   & 196                                                                                      & 44                                                                       \\ \hline

C89328: Pediatric Disorder                           & 528                             & 280   & 218                                                                                      & 20                                                                       \\ \hline

C28193: Syndrome                                     & 907                             & 266   & 204                                                                                      & 31                                                                       \\ \hline

C3101: Genetic Disorder                              & 159                             & 52    & 30                                                                                      & 6                                                                        \\ \hline

C2893: Psychiatric Disorder                          & 231                             & 45    & 29                                                                                      & 11                                                                       \\ \hline

C3113: Hyperplasia                                   & 81                              & 24    & 17                                                                                       & 8                                                                        \\ \hline

C3340: Polyp                                         & 110                             & 24    & 7                                                                                      & 2                                                                        \\ \hline

C35470: Behavioral Disorder                          & 49                              & 19    & 9                                                                                      & 0                                                                        \\ \hline

C3075: Hamartoma                                     & 63                              & 15    & 6                                                                                       & 0                                                                        \\ \hline

C26684: Radiation-Induced Disorder                   & 25                              & 5     & 3                                                                                       & 0                                                                        \\ \hline

\end{tabular}

}

\end{table}

For each potentially missing concept identified, we checked whether its lexical feature set can be matched to any concept name from the external terminologies in the UMLS. 
%This assures that the formalized concepts are meaningful. 
We found 592 out of 8,983 potentially missing concepts are included in the external terminologies in UMLS (see Table~\ref{statisInfo} for the number of missing concepts validated via UMLS for each sub-hierarchy). Table~\ref{validationExample} lists 10 examples of validated missing concepts (in the form of lexical feature sets) and matched concept names in the UMLS terminologies. 
%For example, formal concept whose lexical feature set is \{in, breast, carcinoma, situ\} is matched with \textit{Breast Carcinoma In Situ} in MSH.

Since a matching concept may be from multiple UMLS terminologies, we further looked into the terminologies that contributed most to the validation of the 592 identified potentially missing concepts. The top 10 in terms of the number of matched concepts (in parentheses) are listed as follows: Consumer Health Vocabulary - CHV (328), SNOMED CT US Edition - SNOMEDCT\_US (245), Read Codes - RCD (135), MedDRA - MDR (124), ICPC2-ICD10 Thesaurus - ICPC2ICD10ENG (101), MSH (97), Metathesaurus Names - MTH (79), MEDCIN (78), Online Mendelian Inheritance in Man - OMIM (55), and Logical Observation Identifiers Names and Codes - LNC (52).

\begin{table}[h!]
\centering
\renewcommand{\arraystretch}{1.4}
\caption{Ten examples of validated missing concepts and their matched concepts in the UMLS terminologies.}
\label{validationExample}
\scalebox{0.74}{
\begin{tabular}{|l|l|}
\hline
\textbf{Lexical Feature Set of Missing Concept}         & \textbf{Matched Concept (External Terminology)} \\ \hline
\{carcinoma, papillary, urothelial\}           & Papillary urothelial carcinoma (SNOMEDCT\_US)     \\ \hline
\{borderline, serous, tumor\}                  & Serous borderline tumor (SNOMEDCT\_US)            \\ \hline
\{intestinal, lymphoma\}                       & Intestinal lymphoma (SNOMEDCT\_US)                \\ \hline
\{adrenal, carcinoma\}                         & Adrenal carcinoma (OMIM)                          \\ \hline
\{in, breast, carcinoma, situ\}                & breast carcinoma in situ (CHV)                    \\ \hline
\{fossa, piriform\}                            & Piriform Fossa (MSH)                              \\ \hline
\{cellular, pigmentation\}                     & cellular pigmentation (GO)                        \\ \hline
\{b-cell, cutaneous, lymphoma, primary\}       & Primary cutaneous B-cell lymphoma (MEDCIN)        \\ \hline
\{gastric, sarcoma\}                           & gastric sarcoma (MEDCIN)                          \\ \hline
\{adenocarcinoma, breast, metaplasia,   with\} & breast adenocarcinoma with metaplasia (MEDCIN)    \\ \hline
\end{tabular}
}
\end{table}


\section{Discussion}
In this work, we leveraged words in concept names and FCA to detect potentially missing concepts in the NCI Thesaurus. The preliminary evaluation via UMLS-based validation indicates that our method has the potential to identify missing concepts for concept enrichment of the NCI Thesaurus.

However, this work has several limitations that need further improvement. 
First, the potentially missing concepts detected by our method may not be directly imported into a terminology.  This is because different terminologies are developed for disparate purposes and have varying target applications, and a concept that is essential for a terminology may not be necessary for another. Further reviews and evaluations by the terminology curators are still required to decide whether a concept is meaningful and should be added according to the scope of the terminology and its potential applications.

Although we have found supporting evidence (i.e., matching concept names) in the UMLS for a certain portion of potentially missing concepts, further work is still needed to name the remaining concepts according to their lexical feature sets. We plan to experiment with two ideas. One is to maintain the order or sequence of words in concept names while performing the multistage intersection in FCA. The other is to leverage our previous work~\cite{zheng2018exploring} on predicting concept names using deep learning approaches given bags of words.

%Given a new concept derived from the formal context of a terminology, theoretically, we could locate where it should be inserted based on its subconcept-superconcept relations with other existing concepts. However, in this work, we used words in concept names as the FCA attributes which may lead the ``subconcept-superconcept" relation derived be different from the hierarchical IS-A relation in the original terminology. 
A limitation of using words in concept names as the FCA attributes is that the ``subconcept-superconcept" relation derived may be different from the hierarchical IS-A relation in the original terminology. 
For instance,  \textit{Breast Neoplasm} and \textit{Breast} are two new concepts generated based on the FCA formal context in Fig.~\ref{formalcontext}. Although the two concepts have a ``subconcept-superconcept" relation in terms of the FCA word attributes, they do not form a valid IS-A relation. In fact, \textit{Breast} locates in a different sub-hierarchy \textit{Organ}. A potential solution to avoid such cases is to use enriched lexical features for a concept, which includes its ancestor's lexical features. This way, the original hierarchical relation will be captured in the initial FCA formal context, and thus the new concepts generated by attribute set intersection will locate within the same sub-hierarchy with the root concept. However, the enriched lexical features may make it more difficult to decide which words to use for naming a concept. To deal with this, we plan to leverage both logical definitions and lexical features to identify and name missing concepts.

In addition, we only performed a preliminary evaluation to automatically validate potentially missing concepts using UMLS. In future work, we will invite domain experts to perform manual evaluation to validate potentially missing concepts identified by our FCA-based method.

%\begin{figure}[h!]
%\centering
%\includegraphics[scale = 0.15]{FCA_lattice.png}
%%\vspace{-3mm}
%\caption{The complete lattice generated from formal context in Figure~\ref{formalcontext}. The original root is marked by yellow and newly formalized concepts are marked by blue.}
%\label{fcalattice}
%\end{figure}


\section{Conclusion}~\label{conclusion}
In this paper, we introduced a lexical- and FCA-based method that utilizes intrinsic knowledge of a terminology to detect potentially missing concepts. 
%Words in the concepts names, after normalization and mapping, were used as attributes to construct the formal context. After applying multistage intersection, we could identify missing concepts with their lexical features. 
We applied our method to the NCI Thesaurus \textit{Disease or Disorder} sub-hierarchy and identified 8,983 potentially missing concepts.  The preliminary evaluation via external validation using UMLS showed encouraging evidence for the effectiveness of our method.

%\section*{Acknowledgment}
%This work was supported by the National Science Foundation (NSF) through grants, as well as the National Institutes of Health (NIH) through grants. The content is solely the responsibility of the authors and does not necessarily represent the official views of the NSF or NIH.
\bibliographystyle{IEEEtran}
\bibliography{IEEEexample}
\iffalse %sample in the original file
\begin{thebibliography}{00}
\bibitem{b1} G. Eason, B. Noble, and I. N. Sneddon, ``On certain integrals of Lipschitz-Hankel type involving products of Bessel functions,'' Phil. Trans. Roy. Soc. London, vol. A247, pp. 529--551, April 1955.
\end{thebibliography}
\fi

\end{document}
