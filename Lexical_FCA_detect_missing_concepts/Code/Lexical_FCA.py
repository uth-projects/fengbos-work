###############################################################################
#Pre-processing: Normalization and 1-word synonym mapping
###############################################################################
IDToSynonyms = {}
file1 = open("conceptSynonym_1908_normalized.txt","r",encoding="ISO-8859-1")
for lines1 in file1:
    line1 = lines1.split("\n")[0].split("\t")
    SynonymTuple = line1[2].lower().split("()()")
    if IDToSynonyms.get(line1[0],"default") == "default":
        IDToSynonyms[line1[0]] = [SynonymTuple]
    elif SynonymTuple not in IDToSynonyms.get(line1[0]):
        IDToSynonyms[line1[0]].append(SynonymTuple)
file1.close()
#
IDToLabel = {}
oriTokenToNormalized = {}
file2 = open("conceptInformation_1908_normalized.txt","r",encoding="ISO-8859-1")
for lines2 in file2:
    line2 = lines2.split("\n")[0].split("\t")
    LabelTuple = line2[2].lower().split("()()")
    if line2[3][-1] == " ":
        oriName = line2[3].lower()[:-1].split(" ")
        print(lines2)
    else:
        oriName = line2[3].lower().split(" ")
    IDToLabel[line2[0]] = LabelTuple
    for x in range(0,len(LabelTuple)):
        if LabelTuple[x] != oriName[x]:
            if oriTokenToNormalized.get(LabelTuple[x],"DEFAULT") == "DEFAULT":
                oriTokenToNormalized[LabelTuple[x]] = [oriName[x]]
            elif oriName[x] not in oriTokenToNormalized.get(LabelTuple[x]):
                oriTokenToNormalized[LabelTuple[x]].append(oriName[x])
    #if len(oriName) != len(LabelTuple):
    #    print(oriName,LabelTuple)
    #    print([lines2])
file2.close()
#
import csv
output1 = open("NormalizationReplacementMapping.csv","w")
writer1 = csv.writer(output1)
for key1 in oriTokenToNormalized.keys():
    queryResult = oriTokenToNormalized.get(key1)
    writer1.writerow((key1,)+tuple(queryResult))
output1.close()
#1-word synonym mapping
replaceMap = {} #key will be replaced by value while computing formal context -- bag-of-word
notReplace = set()
#
for concept1 in IDToLabel.keys():
    concept1Label = IDToLabel.get(concept1)
    if len(concept1Label) == 1:
        concept1Synonyms = IDToSynonyms.get(concept1)
        for synonym1 in concept1Synonyms:
            if len(synonym1) == 1 and synonym1 != concept1Label:
                if replaceMap.get(synonym1[0],"Default") == "Default":
                    replaceMap[synonym1[0]] = concept1Label[0]
                else:
                    if replaceMap.get(synonym1[0]) != concept1Label[0]:
                        notReplace.add(synonym1[0])
#                
output1 = open("SynonymReplacementMapping.txt","w")
for key1 in replaceMap.keys():
    if key1 not in notReplace:
        output1.write(key1+"\t"+replaceMap.get(key1)+"\n")
output1.close()
#
###############################################################################
#Step1
###############################################################################
import csv
replaceMap = {} #key will be replaced by value while performing FCA on sequences
replaceMapReverse = {} #key could be changed to values while validating sequences
#e.g., while peforming FCA, tumor will be mapped to neoplasm
#e.g., while validating, neoplasm will be replaced by 
#[tumor, other synonym, tumor before noromalization and other synonym before normalization]
file1 = open("SynonymReplacementMapping.txt","r")
for lines1 in file1:
    line1 = lines1.split("\n")[0].split("\t")
    if replaceMap.get(line1[0],"Default") == "Default":
        replaceMap[line1[0]] = line1[1]
    else:
        print("error")
    if replaceMapReverse.get(line1[1],"Default") == "Default":
        replaceMapReverse[line1[1]] = [line1[0]]
    else:
        replaceMapReverse[line1[1]].append(line1[0])
file1.close()
#
file2 = open("NormalizationReplacementMapping.csv","r")
reader2 = csv.reader(file2)
for row2 in reader2:
    if replaceMapReverse.get(row2[0],"DEFAULT") == "DEFAULT":
        replaceMapReverse[row2[0]] = row2[1:]
    else:
        for items in row2[1:]:
            if items not in replaceMapReverse.get(row2[0]):
                replaceMapReverse[row2[0]].append(items)
file2.close()
############################################################################### 
#Step2 
############################################################################### 
import csv
import string
allowed = string.ascii_letters + string.digits
def check_naive(myString):
    return any(c in allowed for c in myString)
conceptLexical = {}
file1 = open("conceptInformation_1908_normalized.txt","r",encoding="ISO-8859-1")
for lines1 in file1:
    line1 = lines1.split("\n")[0].split("\t")
    lexicalItems = line1[2].lower().split("()()")
    qualified = []
    for item1 in lexicalItems:
        if check_naive(item1) != False:
            qualified.append(item1)
    #qualifiedIter = qualified[:]
    for i in range(len(qualified)):
        if replaceMap.get(qualified[i],"DEFAULT") != "DEFAULT":
            qualified[i] = replaceMap.get(qualified[i])
    conceptLexical[line1[0]] = qualified
file1.close()
#
import networkx as nx  
file22 = open("hierarchicalRelation(ParentChild)_1908.txt","rb")
Dirgraph2 = nx.read_edgelist(file22, create_using = nx.DiGraph(),nodetype = str)
def findSubhierarchy(node):
    Dic = dict(nx.bfs_successors(Dirgraph2,node))
    d = Dic.values()
    c = []
    for e in d:
        c.extend(e)
    return c 
file22.close()
###############################################################################
def lcs(S,T):
    m = len(S)
    n = len(T)
    counter = [[0]*(n+1) for x in range(m+1)]
    longest = 0
    lcs_set = set()
    for i in range(m):
        for j in range(n):
            if S[i] == T[j]:
                c = counter[i][j] + 1
                counter[i+1][j+1] = c
                if c > longest:
                    lcs_set = set()
                    longest = c
                    lcs_set.add(tuple(S[i-c+1:i+1]))
                elif c == longest:
                    lcs_set.add(tuple(S[i-c+1:i+1]))
    return lcs_set
def seq_intersect(S,T):
    resultSet = set()
    interSeq = []
    for i in range(len(S)):
        if S[i] in T:
            interSeq.append(S[i])
    interSeq2 = []
    for j in range(len(T)):
        if T[j] in S:
            interSeq2.append(T[j])
    if len(interSeq)>0:
        resultSet.add(tuple(interSeq))
    if len(interSeq2)>0:
        resultSet.add(tuple(interSeq2))
    return resultSet
def subList(a,b): #check if a in b
    j = 0
    for i in range(len(b)):
        if b[i:i+len(a)] == a:
            j = j+1
    if j ==0:
        return False
    else:
        return True    
#########################
import csv
import string
allowed = string.ascii_letters + string.digits
def check_naive(myString):
    return any(c in allowed for c in myString)
#
allNameInNCIt = {}
NCItLabel = {}
file2 = open("conceptInformation_1908_normalized.txt","r",encoding="ISO-8859-1")
for lines2 in file2:
    line2 = lines2.split("\n")[0].split("\t")
    BagOfWord = line2[2].lower().split("()()")
    checkedBOW1 = []
    for item1 in BagOfWord:
        if check_naive(item1) != False:
            checkedBOW1.append(item1)
    for i in range(len(checkedBOW1)):
        if replaceMap.get(checkedBOW1[i],"DEFAULT") != "DEFAULT":
            checkedBOW1[i] = replaceMap.get(checkedBOW1[i])
    checkedBOW = tuple(checkedBOW1)
    if allNameInNCIt.get(checkedBOW,"Default") == "Default":
        allNameInNCIt[checkedBOW] = [line2[0]]
    else:
        allNameInNCIt[checkedBOW].append(line2[0])
    NCItLabel[line2[0]] = line2[3]
file2.close()
#
import networkx as nx
file2 = open("hierarchicalRelation(ChildParent)_1908.txt","rb")
Dirgraph1 = nx.read_edgelist(file2, create_using = nx.DiGraph(), nodetype = str)
file2.close()
#
def findAncestors(node):
    Dic = dict(nx.bfs_successors(Dirgraph1,node))
    d = Dic.values()
    c = []
    for e in d:
        c.extend(e)
    return c 
allNodesInGraph = set(Dirgraph1.nodes)
#
associatedRoles = {}
file1 = open("conceptRelationInferred_1908State.txt","r")
for lines1 in file1:
    line1 = lines1.split("\n")[0].split("\t")
    if line1[1]!= "ISA2019FZ":
        if associatedRoles.get(line1[0],"DEFAULT") == "DEFAULT":
            associatedRoles[line1[0]] = [(line1[1],line1[2])]
        elif (line1[1],line1[2]) not in associatedRoles.get(line1[0]):
            associatedRoles[line1[0]].append((line1[1],line1[2]))     
file1.close()
#
def checkSingleGeneral(attributeB, attributeA):  #check if B is more general than A
    if attributeB[0] in allNodesInGraph and attributeA[0] in allNodesInGraph:
        if (attributeB[0] == attributeA[0]) or (attributeB[0] in findAncestors(attributeA[0])):
            if (attributeB[1] in findAncestors(attributeA[1])) or (attributeB[1] == attributeA[1]):
                return True
            else:
                return False
        else:
            return False
    else:
        if (attributeB[0] == attributeA[0]):
            if (attributeB[1] in findAncestors(attributeA[1])) or (attributeB[1] == attributeA[1]):
                return True
            else:
                return False
        else:
            return False
#       
def roleIntersection(attributeLB, attributeLA):
    intersection = []
    for singleAttributeB in attributeLB:
        if any(checkSingleGeneral(singleAttributeB,x) for x in attributeLA):
            intersection.append(singleAttributeB)
    for singleAttributeA in attributeLA:
        if any(checkSingleGeneral(singleAttributeA,y) for y in attributeLB):
            intersection.append(singleAttributeB)
    return(list(set(intersection)))
###############################################################################
roots = list(Dirgraph2.successors("C2991")) 
for root in roots:
    print(root)
    allConcept = findSubhierarchy(root)
    allConcept.append(root)
    originalSet = set() #all sequences 
    #allBOW = set() #all bag of words
    for eachConcept in allConcept:
        itsSeq = conceptLexical.get(eachConcept)
        #itsBOW = tuple(sorted(set(itsSeq)))
        originalSet.add(tuple(itsSeq))
        #allBOW.add(itsBOW)       
    #
    initialSet = originalSet.copy()  # used to store all formalzied concepts
    oldIteration= set()
    newlyAdded = originalSet.copy()  # used to store newly added concepts for each iteration
    flag = 0
    while flag ==0:
        #currentIteration = initialSet.copy()
        lastIteration = initialSet.copy()     #concept names before an iteration
        oldIterationL = list(oldIteration)    #keep old concept names (no need to do intersection for them)
        newlyAddedL = list(newlyAdded)
        #last iteration new vs last iteration new
        for i in range(0,len(newlyAddedL)-1):
            for j in range(i+1,len(newlyAddedL)):
                a = lcs(newlyAddedL[i],newlyAddedL[j])
                for newlyFormed in a:
                    initialSet.add(newlyFormed)
        #last iteration old vs last iteration new        
        for k in range(0,len(newlyAddedL)):
            for l in range(0,len(oldIterationL)):
                b = lcs(newlyAddedL[k],oldIterationL[l])
                for newlyFormed in b:
                    initialSet.add(newlyFormed)
        #update variables
        newlyAdded = initialSet - lastIteration
        oldIteration = lastIteration.copy()     #keep old concept names
        if len(newlyAdded) == 0:
            flag = 1
    #
    allNewlyAdded = initialSet - originalSet
    #
    filename = NCItLabel.get(root)+"_NewConcepts_LCS(>0).csv"
    output5 = open(filename,"w")
    writer5 = csv.writer(output5)
    for eachNewOne in allNewlyAdded:
        directSubtypes = []
        directSuperTypes = []
        for existConcept in allConcept:
            existConceptLex = conceptLexical.get(existConcept)
            #if set(existConceptLex).issuperset(set(eachNewOne)):
            if subList(list(eachNewOne),existConceptLex):
                directSubtypes.append(existConcept)
            if subList(existConceptLex,list(eachNewOne)):
                directSuperTypes.append(existConcept)
        allSubtypes = "\t".join([x+": "+NCItLabel.get(x) for x in directSubtypes])
        lowerBound = directSubtypes[:]
        for child1 in directSubtypes:
            if any(x in findAncestors(child1) for x in directSubtypes):
                lowerBound.remove(child1)
        lowerBoundL = "\t".join([y+": "+NCItLabel.get(y) for y in lowerBound])
        lowerBoundRole = [associatedRoles.get(x,[]) for x in lowerBound]
        intersectionFlag = 0
        initialIntersect = lowerBoundRole[0]
        for i in range(1,len(lowerBoundRole)):
            initialIntersect = roleIntersection(initialIntersect,lowerBoundRole[i])
        if len(initialIntersect)>0:
            intersectionFlag = 1
        #
        upperBound = directSuperTypes[:]
        for parent1 in directSuperTypes:
            if any(parent1 in findAncestors(x) for x in directSuperTypes):
                upperBound.remove(parent1)
        upperBoundL = "\t".join([y+": "+NCItLabel.get(y) for y in upperBound])        
        writer5.writerow((lowerBoundL,upperBoundL,len(lowerBound),len(upperBound),intersectionFlag,allSubtypes)+eachNewOne)
    output5.close()   
#
############################################################################### 
#Step3 Validation
############################################################################### 
import csv
import string
allowed = string.ascii_letters + string.digits
def check_naive(myString):
    return any(c in allowed for c in myString)
#
allNameInNCIt = {}
NCItLabel = {}
file2 = open("conceptInformation_1908_normalized.txt","r",encoding="ISO-8859-1")
for lines2 in file2:
    line2 = lines2.split("\n")[0].split("\t")
    BagOfWord = line2[2].lower().split("()()")
    checkedBOW1 = []
    for item1 in BagOfWord:
        if check_naive(item1) != False:
            checkedBOW1.append(item1)
    for i in range(len(checkedBOW1)):
        if replaceMap.get(checkedBOW1[i],"DEFAULT") != "DEFAULT":
            checkedBOW1[i] = replaceMap.get(checkedBOW1[i])
    checkedBOW = tuple(checkedBOW1)
    if allNameInNCIt.get(checkedBOW,"Default") == "Default":
        allNameInNCIt[checkedBOW] = [line2[0]]
    else:
        allNameInNCIt[checkedBOW].append(line2[0])
    NCItLabel[line2[0]] = line2[3]
file2.close()
#
file2Supl = open("conceptSynonym_1908_normalized.txt","r",encoding="ISO-8859-1")
for lines4 in file2Supl:
    line4 = lines4.split("\n")[0].split("\t")
    BagOfWord = line4[2].lower().split("()()")
    checkedBOW1 = []
    for item1 in BagOfWord:
        if check_naive(item1) != False:
            checkedBOW1.append(item1)
    for i in range(len(checkedBOW1)):
        if replaceMap.get(checkedBOW1[i],"DEFAULT") != "DEFAULT":
            checkedBOW1[i] = replaceMap.get(checkedBOW1[i])
    checkedBOW = tuple(checkedBOW1)
    if allNameInNCIt.get(checkedBOW,"Default") == "Default":
        allNameInNCIt[checkedBOW] = [line4[0]]
    else:
        if line4[0] not in allNameInNCIt.get(checkedBOW):
            allNameInNCIt[checkedBOW].append(line4[0])  
file2Supl.close()
#    
#131 i macroaggregated()()albumen|(131)I-Macroaggregated Albumin|C0000005|MSH|D012711|Y|A26634265
allNameInUMLS = {}
UMLSLabel = {}
toCUI = {}
file3 = open("UMLS_conceptName2019AB_normalized.txt","r",encoding="ISO-8859-1")
for lines3 in file3:
    line3 = lines3.split("\n")[0].split("|")
    BagOfWord = line3[0].lower().split("()()")
    checkedBOW2 = []
    for item2 in BagOfWord:
        if check_naive(item2) != False:
            checkedBOW2.append(item2)    
    for i in range(len(checkedBOW2)):
        if replaceMap.get(checkedBOW2[i],"DEFAULT") != "DEFAULT":
            checkedBOW2[i] = replaceMap.get(checkedBOW2[i])
    checkedBOW = tuple(checkedBOW2)
    if allNameInUMLS.get(checkedBOW,"Default") == "Default":
        allNameInUMLS[checkedBOW] = [(line3[2],line3[3],line3[4],line3[6])]#
    else:
        allNameInUMLS[checkedBOW].append((line3[2],line3[3],line3[4],line3[6]))
    UMLSLabel[line3[6]] = line3[1]
    toCUI[(line3[3],checkedBOW)] = line3[2]
file3.close()
###############################################################################
CUI_to_ST = {}
file1 = open("conceptSemanticType2019AB.csv","r") 
reader1 = csv.reader(file1)
for row1 in reader1:
    if CUI_to_ST.get(row1[0],"DEFAULT")== "DEFAULT":
        CUI_to_ST[row1[0]] = [row1[2]]
    else:
        CUI_to_ST[row1[0]].append(row1[2])
file1.close()
#
import networkx as nx
file3 = open("UMLS_CUISA(childParent).txt","rb")
CUIDAG = nx.read_edgelist(file3, create_using = nx.DiGraph(), nodetype = str)
allNodesInCUIDAG = set(CUIDAG.nodes()) 
file3.close()
#
roots = list(Dirgraph2.successors("C2991")) 
import csv
import sys
csv.field_size_limit(sys.maxsize)
statisFile = open("newConcept_StatisInfo.csv","w")
statisWriter = csv.writer(statisFile)
for root in roots:
    TotalNewlyFormalized = 0
    TotalNotQualified = 0
    NCItTP = 0
    NCItFP = 0
    NCItTN = 0
    NCItFN = 0
    UMLSTP = 0
    UMLSFP = 0
    UMLSTN = 0
    UMLSFN = 0
    locationSupportUMLS = 0
    print(root)
    allConcept = findSubhierarchy(root)
    allConcept.append(root)
    #iterationSuffix = ["_NewConcepts_LCS","_NewConcepts_seqIntersection"]
    suffix1= "_NewConcepts_LCS(>0)"
    #for suffix1 in iterationSuffix:
    filename = NCItLabel.get(root)+suffix1+".csv"
    file1 = open(filename,"r")  
    reader1 = csv.reader(file1)
    filename2 = NCItLabel.get(root)+suffix1+"_NotVerified.csv"
    file2 = open(filename2,"w")
    writer3 = csv.writer(file2)
    filename3 = NCItLabel.get(root)+suffix1+"_InNCIt.csv"
    filename4 = NCItLabel.get(root)+suffix1+"_InUMLS.csv"
    output1 = open(filename3,"w")
    output2 = open(filename4,"w")
    writer1 = csv.writer(output1)
    writer2 = csv.writer(output2)
    for row1 in reader1:
        TotalNewlyFormalized = TotalNewlyFormalized +1
        if row1[3]=="0":
            TotalNotQualified = TotalNotQualified+1
        flag = 0
        nameSeq = tuple(row1[6:])
        if allNameInNCIt.get(nameSeq,"Default") != "Default":
            flag=1
            IDs = allNameInNCIt.get(nameSeq)
            written = []
            mappedID = IDs[:]
            for ID in IDs:
                written.append(ID+": "+NCItLabel.get(ID))
            if any(x in allConcept for x in mappedID) == False:
                writer1.writerow(tuple(row1[0:6])+("outside hierarchy",written)+nameSeq)
                if row1[3]=="0":
                    NCItTN = NCItTN+1
                else:
                    NCItFP = NCItFP+1
            else:
                writer1.writerow(tuple(row1[0:6])+("inside sysnonym",written)+nameSeq)
                if row1[3]=="0":
                    NCItFN = NCItFN+1
                else:
                    NCItTP = NCItTP+1                    
        #                            
        elif allNameInUMLS.get(nameSeq,"Default") != "Default":
            flag=1
            IDs = allNameInUMLS.get(nameSeq)
            written = []
            mappedCUI = [x[0] for x in IDs]
            for ID in IDs:
                written.append("("+ID[3]+", "+ID[1]+")--"+ID[0]+": "+UMLSLabel.get(ID[3]))
            childConceptID = [x.split(": ")[0] for x in row1[0].split("\t")] 
            childQuery = [("NCI",tuple(conceptLexical.get(y))) for y in childConceptID]
            childCUI = [toCUI.get(z,"") for z in childQuery]
            mappedST = []
            for x in mappedCUI:
                mappedST.extend(CUI_to_ST.get(x))
            childST = []
            for y in childCUI:
                childST.extend(CUI_to_ST.get(y,[]))
            hasPath = 0
            onePath = ""
            if len(set(mappedST).intersection(set(childST))) == 0:
                if row1[3]=="0":
                    UMLSTN = UMLSTN+1
                else:
                    for CUI1 in mappedCUI:
                        if CUI1 in allNodesInCUIDAG:
                            for CUI2 in childCUI:
                                if CUI2 in allNodesInCUIDAG:
                                    if nx.has_path(CUIDAG,CUI2,CUI1):
                                        a = list(nx.shortest_simple_paths(CUIDAG,CUI2,CUI1))
                                        hasPath = 1
                                        onePath = str(a[0])
                                        break
                    if hasPath ==1:
                        locationSupportUMLS = locationSupportUMLS+1
                    UMLSFP = UMLSFP+1
                writer2.writerow(tuple(row1[0:6])+(hasPath,onePath)+("semantic type not consistent","* ".join(list(set(childST))),"* ".join(list(set(mappedST))),written)+nameSeq)
            else:
                if row1[3]=="0":
                    UMLSFN = UMLSFN+1
                else:
                    for CUI1 in mappedCUI:
                        if CUI1 in allNodesInCUIDAG:
                            for CUI2 in childCUI:
                                if CUI2 in allNodesInCUIDAG:
                                    if nx.has_path(CUIDAG,CUI2,CUI1):
                                        a = list(nx.shortest_simple_paths(CUIDAG,CUI2,CUI1))
                                        hasPath = 1
                                        onePath = str(a[0])
                                        break
                    if hasPath ==1:
                        locationSupportUMLS = locationSupportUMLS+1
                    UMLSTP = UMLSTP+1
                writer2.writerow(tuple(row1[0:6])+(hasPath,onePath)+("semantic type ok","* ".join(list(set(childST))),"* ".join(list(set(mappedST))),written)+nameSeq)
        if flag ==0:
            writer3.writerow(tuple(row1))
    statisWriter.writerow((NCItLabel.get(root),TotalNewlyFormalized,TotalNotQualified,NCItTP,NCItFP,NCItTN,NCItFN,UMLSTP,UMLSFP,UMLSTN,UMLSFN,locationSupportUMLS))
    file2.close()
    file1.close()  
    output1.close()
    output2.close()
statisFile.close()
###############################################################################
#For the missing concepts that are validated, generated all possible layout
#and then validate by biomedical literature (Done by Rashmie)
###############################################################################
roots = list(Dirgraph2.successors("C2991")) 
for root in roots:
    iterationSuffix = ["_NewConcepts_LCS(>0)"]
    for suffix1 in iterationSuffix:
        filename2 = NCItLabel.get(root)+suffix1+"_NotVerified.csv"
        file1 = open(filename2,"r")
        reader1 = csv.reader(file1)
        filename3 = NCItLabel.get(root)+suffix1+"_NotVerified_Replace.csv"
        output = open(filename3,"w")
        writer1 = csv.writer(output)
        for row1 in reader1:
            replaceList = []
            for item in row1[6:]:
                result = [item]
                candidate = replaceMapReverse.get(item,[])
                candidate.append(item)
                for candidate1 in candidate:
                    result.extend(replaceMapReverse.get(candidate1,[]))
                replaceList.append(list(set(result)))
            replacement = ["()()".join(x) for x in replaceList]
            writer1.writerow((row1[5],row1[3],"()()".join(row1[6:]))+tuple(replacement))
            #print("ok2")
        file1.close()
        output.close()