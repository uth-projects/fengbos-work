#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  1 15:56:05 2021

@author: fengbozheng
"""
#file1 = open("/spark_files/yzhou9/Ruoxing/hf_d_diagnosis.txt","r") 
#file2 = open("/home/fzheng1/QualityImpactAnalysis/TestSCP.txt","r")
diagnosis_hasMapping = {}
file1 = open("procedure_AthenaMap_Strict_noName.txt","r")
file1.readline()
for lines1 in file1:
	line1 = lines1.split("\n")[0].split("\t")
	if line1[2] != "":
		diagnosis_hasMapping[(line1[0],line1[1])] = line1[2]
file1.close()
print(len(diagnosis_hasMapping))
#
file2 = open("/spark_files/yzhou9/Ruoxing/hf_d_procedure.txt","r")
output2= open("ProcedureID_SNOMED_mapping.txt","w")
#ProcedureID, SNOMEDID
file2.readline()
for lines2 in file2:
	line2 = lines2.split("\n")[0].split("\t")
	if diagnosis_hasMapping.get((line2[1],line2[2]),"DEFAULT") != "DEFAULT":
		output2.write(line2[0]+"\t"+diagnosis_hasMapping.get((line2[1],line2[2]))+"\n")
file2.close()
output2.close()
#
diagnosis_to_SNOMED = {}   #5964
file3 = open("ProcedureID_SNOMED_mapping.txt","r")
for lines3 in file3:
	line3 = lines3.split("\n")[0].split("\t")
	diagnosis_to_SNOMED[line3[0]] = line3[1]
file3.close()
print(len(diagnosis_to_SNOMED))
#
SNOMED_to_encounters = {}  #5394
file4 = open("/spark_files/yzhou9/Ruoxing/ruoxing_proc.txt","r")
#output4 = open("ruoxing_diag_Encounter-to-SNOMED.txt","w")
#EncounterID, SNOMEDID
file4.readline()
for lines4 in file4:
	line4 = lines4.split("\n")[0].split("\t")
	SNOMEDID = diagnosis_to_SNOMED.get(line4[2],"DEFAULT")
	if SNOMEDID != "DEFAULT":
		if SNOMED_to_encounters.get(SNOMEDID,"DEFAULT") == "DEFAULT":
			SNOMED_to_encounters[SNOMEDID] = set()
			SNOMED_to_encounters[SNOMEDID].add(line4[0])
		else:
			SNOMED_to_encounters[SNOMEDID].add(line4[0])
		#output4.write(line4[1]+"\t"+diagnosis_to_SNOMED.get(line4[2])+"\n")
file4.close()
#output4.close()
#
print(len(SNOMED_to_encounters))
output = open("SNOMEDProc_to_patientsk.txt","w")
output2 = open("SNOMEDProc_to_patientsk_dic.txt","w")
i = 0
for SNOMEDID in SNOMED_to_encounters.keys():
	i = i+1
	#queryResult = list(SNOMED_Diag_to_encounters.get(SNOMEDID))
	output.write(SNOMEDID+"\t"+";".join(list(SNOMED_to_encounters.get(SNOMEDID)))+"\n")
	output2.write(SNOMEDID+"\t"+str(i)+"\n")
output.close()
output2.close()