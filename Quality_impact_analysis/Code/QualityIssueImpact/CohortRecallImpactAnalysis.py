#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  1 15:56:05 2021

@author: fengbozheng
"""
#
'''
print(len(SNOMED_to_encounters))
output = open("SNOMEDDiag_to_patientsk.txt","w")
output2 = open("SNOMEDDiag_to_patientsk_dic.txt","w")
i = 0
for SNOMEDID in SNOMED_to_encounters.keys():
	i = i+1
	#queryResult = list(SNOMED_Diag_to_encounters.get(SNOMEDID))
	output.write(SNOMEDID+"\t"+";".join(list(SNOMED_to_encounters.get(SNOMEDID)))+"\n")
	output2.write(SNOMEDID+"\t"+str(i)+"\n")
output.close()
output2.close()
'''
#
#round(a / b * 100, 2)
SNOMEDID_to_patientsks = {}
file1 = open("SNOMEDDiag_to_patientsk.txt","r")
for lines1 in file1:
	line1 = lines1.split("\n")[0].split("\t")
	SNOMEDID_to_patientsks[line1[0]] = tuple(line1[1].split(";"))
file1.close()
file2 = open("SNOMEDProc_to_patientsk.txt","r")
for lines2 in file2:
	line2 = lines2.split("\n")[0].split("\t")
	if SNOMEDID_to_patientsks.get(line2[0],"DEFAULT") != "DEFAULT":
		if set(line2[1].split(";"))!= set(SNOMEDID_to_patientsks.get(line2[0])):
			SNOMEDID_to_patientsks[line2[0]] = tuple()
	else:
		SNOMEDID_to_patientsks[line2[0]] = tuple(line2[1].split(";"))
file2.close()
#appears in both Procedure and Diagnosis  (do not include these concept mappings)
'''
68254000
34955007
58533008
83607001
65200003
118677009
116859006
45125008
118666003
34043003
250707004
26958001
25676003
118449008
43362002
363680008
398171003
70082004
200849007
85476000
271422003
25431006
21054003
171126009
11429006
171183004
122444009
40505001
12895002
40675008
416608005
171201007
36331006
'''
#
Covered_SNOMEDID = set(SNOMEDID_to_patientsks.keys())
output2 = open("Covered_SNOMEDID.txt","w")
for conceptID in Covered_SNOMEDID:
	output2.write(conceptID+"\n")
output2.close()
import csv
file1 = open("ConfirmedMissingIS-A_DescendantsAdded(ThreeWorkTogether).csv","r")
reader1 = csv.reader(file1)
output = open("RecallAnalysis.csv","w")
writer1 = csv.writer(output)
for row1 in reader1:
	if row1[0] in Covered_SNOMEDID:
		childCovered = "child covered"
	else:
		childCovered = "child not covered"
	if row1[2] in Covered_SNOMEDID:
		parentCovered = "parent covered"
	else:
		parentCovered = "parent not covered"
	childAndDesc = [row1[0]]	
	if row1[1] != "":
		childAndDesc.extend(row1[1].split(","))
	parentAndDesc = [row1[2]]
	if row1[3] != "":
		parentAndDesc.extend(row1[3].split(","))
	childCompute = list(set(childAndDesc) - set(parentAndDesc))
	childPatientSet = set()
	parentPatientSet = set()
	for childDesc in childCompute:
		childPatientSet.update(set(SNOMEDID_to_patientsks.get(childDesc,[])))
	for parentDesc in parentAndDesc:
		parentPatientSet.update(set(SNOMEDID_to_patientsks.get(parentDesc,[])))
	a = childPatientSet - parentPatientSet
	b = childPatientSet.union(parentPatientSet)
	if len(childPatientSet)!=0 and len(parentPatientSet)!=0:
		writer1.writerow((row1[0],row1[4],row1[2],row1[5])+(childCovered,parentCovered,len(childPatientSet),len(parentPatientSet),len(a),round(len(parentPatientSet)/len(b)*100,2)))
	else:
		writer1.writerow((row1[0],row1[4],row1[2],row1[5])+(childCovered,parentCovered,len(childPatientSet),len(parentPatientSet),"",""))
file1.close()
file2.close()